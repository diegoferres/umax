<?php

use App\Covenants;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::domain('ventajasmedicina.umax.edu.py')->group(function () {
      Route::get('/', function () {
          return view('landing.index');
      })->name('ventajas.medicina.index');

      Route::get('/index/portugues', function () {
          return view('landing.index_portugues');
      })->name('ventajas.medicina.portugues');
});

Route::group(['prefix'=>'admin','as'=>'admin.'], function(){
    Route::get('/', function () {
        return view('administration.index');
    })->name('dashboard')->middleware('auth');

    Route::resource('news', 'Admin\NewsController')->middleware('auth');
    Route::resource('banners', 'Admin\BannersController')->middleware('auth');
    Route::resource('covenants', 'Admin\CovenantsController')->middleware('auth');
});

Route::get('/', 'HomeController@index')->name('index');

Route::get('/noticias', 'HomeController@news')->name('noticias');

Route::get('/noticias/{id}/detail', 'HomeController@new')->name('noticias.detail');

Route::get('/divulgacion-cientifica', function (){
    $ids = [6,11,26,16,18,20,21,22,23,24,25,27,30,32,34,36,37,39,40,38,41,44,43,45];
    $news = \App\News::whereHas('categories', function ($q) {
        $q->where('id', 2);
    })->orderBy('datetime', 'desc')
        ->get();

    $news = $news->sortBy(function($model) use ($ids) {
        return array_search($model->getKey(), $ids);
    });

    return view('news.divulgacion', compact('news'));

})->name('divulgacion');


Route::get('/nosotros', function () {
    return view('about.index');
})->name('about');

Route::get('/medicina', function () {
    return view('about.medicina');
})->name('medicina');

Route::get('/enfermeria', function () {
    return view('about.enfermeria');
})->name('enfermeria');

Route::get('/virtual', function () {
    return view('about.virtual');
})->name('virtual');

/*
 * Convenios
 */
Route::get('/convenios', function () {
    $covenants = Covenants::orderBy('created_at', 'desc')->get();
    return view('about.convenios', compact('covenants'));
})->name('convenios');

Route::get('/convenios/{id}', function ($id) {
    $covenant = Covenants::find($id);
    return view('about.convenio_interna', compact('covenant'));
})->name('convenio.interna');

Route::get('/convenios_bk', function () {
    return view('about.convenios_bk');
});

Route::get('/convenios_bk/1', function () {
    return view('about.detail-convenio');
})->name('convenio-1');

Route::get('/convenios_bk/2', function () {
    return view('about.detail-convenio2');
})->name('convenio-2');

Route::get('/convenios_bk/3', function () {
    return view('about.detail-convenio3');
})->name('convenio-3');

Route::get('/convenios_bk/4', function () {
    return view('about.detail-convenio4');
})->name('convenio-4');

Route::get('/convenios_bk/5', function () {
    return view('about.detail-convenio5');
})->name('convenio-5');

Route::get('/convenios_bk/6', function () {
    return view('about.detail-convenio6');
})->name('convenio-6');



/*
 * Carreras
 */
Route::get('/postgrado', function () {
    return view('carreras.postgrado');
})->name('postgrado');

Route::get('/pregrado/tecnico-superior-farmacia', function () {
    return view('carreras.pregrado.tecnico_superior_farmacia');
})->name('pregrado-1');

Route::get('/pregrado/tecnico-superior-radiologia', function () {
    return view('carreras.pregrado.tecnico_superior_radiologia');
})->name('pregrado-2');

Route::get('/pregrado/tecnico-superior-laboratorio-clinico', function () {
    return view('carreras.pregrado.tecnico_superior_laboratorio');
})->name('pregrado-3');

Route::get('/pregrado/tecnico-superior-masaje-terapeutico', function () {
    return view('carreras.pregrado.tecnico_superior_masaje');
})->name('pregrado-4');

Route::get('/pregrado/tecnico-superior-enfermeria', function () {
    return view('carreras.pregrado.tecnico_superior_enfermeria');
})->name('pregrado-5');



Route::get('/contacto', function () {
    return view('about.contacto');
})->name('contacto');


Route::get('/maestria', function () {
    return view('postgrado.maestria');
})->name('maestria');

Route::get('/especializacion', function () {
    return view('postgrado.especializacion');
})->name('especializacion');

Route::get('/diplomado', function () {
    return view('postgrado.diplomado');
})->name('diplomado');

Route::get('/extension', function () {
    return view('extension.index');
})->name('extension');

Route::get('/investigacion', function () {
    return view('extension.investigacion');
})->name('investigacion');

Route::get('/politicas-investigacion', function () {
    return view('extension.politicas-investigacion');
})->name('politicas-investigacion');

Route::get('/lineas-investigacion', function () {
    return view('extension.lineas-investigacion');
})->name('lineas-investigacion');

Route::get('/actividades-investigacion', function () {
    return view('extension.actividades-investigacion');
})->name('actividades-investigacion');

Route::get('/investigadores-asociados', function () {
    return view('extension.investigadores-asociados');
})->name('investigadores-asociados');

Route::get('/revista-cientifica', function () {
    return view('extension.revista-cientifica');
})->name('revista-cientifica');

Route::get('/declaracion-privacidad', function () {
    return view('extension.declaracion-privacidad');
})->name('declaracion-privacidad');

Route::get('/equipo-editorial', function () {
    return view('extension.equipo-editorial');
})->name('equipo-editorial');

Route::get('/archivos', function () {
    return view('extension.archivos');
})->name('archivos');

Route::get('/envios', function () {
    return view('extension.envios');
})->name('envios');

Route::get('/revista-cientifica', function () {
    return view('extension.revista-cientifica');
})->name('revista-cientifica');

Route::get('/pastoral', function () {
    return view('extension.pastoral');
})->name('pastoral');

Route::get('/bienestar-estudiantil', function () {
    return view('bienestar.bienestar');
})->name('bienestar');

Route::get('/egresados', function () {
    return view('about.egresados');
})->name('egresados');

Route::get('/noticias/2', function () {
    return view('news.news-detail-2');
})->name('news-detail-2');

Route::get('/noticias/3', function () {
    return view('news.news-detail-3');
})->name('news-detail-3');

Route::get('/noticias/4', function () {
    return view('news.news-detail-4');
})->name('news-detail-4');

Route::get('/noticias/5', function () {
    return view('news.news-detail-5');
})->name('news-detail-5');

Route::get('/biblioteca-virtual', function () {
    return view('about.biblioteca');
})->name('biblioteca');

/*
 * Investigación -> Núcleos de Investigación
 */
Route::get('/innovacion-medica', function () {
    return view('nucleos.innovacion-medica');
})->name('innovacion-medica');

Route::get('/enfermedades-infecciosas', function () {
    return view('nucleos.enfermedades-infecciosas');
})->name('enfermedades-infecciosas');

Route::get('/nutricion-geriatria', function () {
    return view('nucleos.nutricion-geriatria');
})->name('nutricion-geriatria');

Route::get('/bioestadistica-aplicada-salud', function () {
    return view('nucleos.bioestadistica-aplicada-salud');
})->name('bioestadistica-aplicada-salud');

Route::get('/donacion-transplante-organos', function () {
    return view('nucleos.donacion-transplante-organos');
})->name('donacion-transplante-organos');

Route::get('/psiquiatria-salud-mental', function () {
    return view('nucleos.salud-mental');
})->name('salud-mental');

Route::get('/tecnologia-informacion-comunicacion-salud', function () {
    return view('nucleos.tecnologia-informacion-comunicacion-salud');
})->name('tecnologia-informacion-comunicacion-salud');


/*
 * MAILING
 */
Route::get('/mail/send/future', 'EmailsController@future')->name('mail.send.future');
Route::get('/mail/send/contact', 'EmailsController@contact')->name('mail.send.contact');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
//http://ventajasmedicina.umax.edu.py/

/*Route::domain('{account}.example.com')->group(function () {
    Route::get('user/{id}', function ($account, $id) {
        //
    });
});*/

Route::get('ventajas/es', function (){
    return view('landing.index');
});
Route::get('ventajas/pt', function (){
    return view('landing.index_portugues');
});
