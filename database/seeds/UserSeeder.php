<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'admin@admin.com',
            'password' => Hash::make('umaxadmin2021'),
        ]);

        DB::table('users')->insert([
            'name' => 'Federico',
            'email' => 'federico@umax.com',
            'password' => Hash::make('123'),
        ]);
    }
}
