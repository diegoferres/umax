<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class NewCategory extends Model
{
    protected $table = 'new_categories';
}
