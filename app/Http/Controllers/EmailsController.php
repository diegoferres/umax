<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\contactMail;
use App\Mail\futureMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailsController extends Controller
{
    public function future(Request $request)
    {
        Mail::to('landing@umax.edu.py')->send(new futureMail($request->all()));

        return redirect()->back();
    }

    public function contact(Request $request)
    {
        Mail::to('info@umax.edu.py')->send(new contactMail($request->all()));

        return redirect()->back();
    }

    /*
     * Egresados
     */
    public function question(Request $request)
    {
        Mail::to('diana.caballero@umax.edu.py')->send(new contactMail($request->all()));

        return redirect()->back();
    }
}
