<?php

namespace App\Http\Controllers;

use App\Banners;
use App\News;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $news = News::orderBy('datetime', 'DESC')->limit(5)->get();
        $banners = Banners::where('status', true)->orderBy('order', 'ASC')->get();

        return view('home.index', compact('news', 'banners'));
    }

    public function news()
    {
        $news = News::orderBy('datetime', 'DESC')->get();

        return view('news.index', compact('news'));
    }

    public function new($id)
    {
        $new = News::find($id);

        return view('news.news-detail', compact('new'));
    }
}

