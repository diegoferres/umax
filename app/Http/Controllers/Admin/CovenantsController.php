<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Covenants;
use App\Http\Controllers\Controller;
use App\Mail\contactMail;
use App\Mail\futureMail;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class CovenantsController extends Controller
{
    public function index(Request $request)
    {
        $covenants = Covenants::orderBy('created_at', 'DESC')->paginate(20);

        return view('administration.covenants.index', compact('covenants'));
    }

    public function create()
    {
        return view('administration.covenants.form');
    }

    public function store(Request $request)
    {
        try {
            \DB::beginTransaction();

            $covenants = new Covenants();
            $covenants->title         = $request->title;
            $covenants->content       = $request->content_text;
            $covenants->external_link = $request->external_link;
            $covenants->save();

            if ($request->image_path) {
                $filename = \Str::uuid().'_'.$covenants->id.'.'.$request->image_path->getClientOriginalExtension();
                $request->image_path->storeAs('public/covenants',$filename);
                $covenants->image_path    = 'covenants/'.$filename;
            }

            if ($request->internal_image_path) {
                $filename = \Str::uuid().'_internal_'.$covenants->id.'.'.$request->internal_image_path->getClientOriginalExtension();
                $request->internal_image_path->storeAs('public/covenants',$filename);
                $covenants->internal_image_path    = 'covenants/'.$filename;
            }

            $covenants->save();

            \DB::commit();

            //dd($covenants);

            return redirect()->route('admin.covenants.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Datos guardados']));
        }catch (\Exception $e){
            \DB::rollBack();
            dd($e->getMessage());
            return redirect()->back()->withInput()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));
        }
    }

    public function edit($id)
    {
        $covenants = Covenants::find($id);
        //dd($covenants);

        return view('administration.covenants.form', compact('covenants'));
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        try {
            \DB::beginTransaction();

            $covenants = Covenants::find($id);

            $covenants->title         = $request->title;
            $covenants->content       = $request->content_text;
            $covenants->external_link = $request->external_link;
            $covenants->save();

            if ($request->image_path) {
                Storage::delete('public/'.$covenants->image_path);

                $filename = \Str::uuid().'_'.$covenants->id.'.'.$request->image_path->getClientOriginalExtension();
                $request->image_path->storeAs('public/covenants',$filename);
                $covenants->image_path    = 'covenants/'.$filename;
            }

            if ($request->internal_image_path) {
                Storage::delete('public/'.$covenants->internal_image_path);

                $filename = \Str::uuid().'_internal_'.$covenants->id.'.'.$request->internal_image_path->getClientOriginalExtension();
                $request->internal_image_path->storeAs('public/covenants',$filename);
                $covenants->internal_image_path    = 'covenants/'.$filename;
            }

            $covenants->save();

            \DB::commit();

            return redirect()->route('admin.covenants.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Datos guardados']));
        }catch (\Exception $e){
            \DB::rollBack();
            dd($e->getMessage());
            return redirect()->back()->withInput()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));;
        }
    }

    public function destroy($id)
    {
        try {
            \DB::beginTransaction();

            $covenant = Covenants::find($id);
            \Storage::delete('public/'.$covenant->image_path);
            \Storage::delete('public/'.$covenant  ->internal_image_path);
            $covenant->delete();

            \DB::commit();

            return redirect()->route('admin.covenants.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Convenio eliminado']));
        }catch (\Exception $e){

            \DB::rollBack();

            dd($e->getMessage());
            return redirect()->back()->withInput()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));;
        }
    }
}
