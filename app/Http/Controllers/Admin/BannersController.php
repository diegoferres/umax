<?php

namespace App\Http\Controllers\Admin;

use App\Banners;
use App\Http\Controllers\Controller;
use App\Mail\contactMail;
use App\Mail\futureMail;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BannersController extends Controller
{
    public function index(Request $request)
    {
        $banners = Banners::orderBy('created_at', 'DESC')->get();

        return view('administration.banners.index', compact('banners'));
    }

    public function create()
    {
        return view('administration.news.form');
    }

    public function store(Request $request)
    {
        //return redirect()->back()->withInput()->with('notification', json_encode(['type' => 'error', 'message' => 'Correcto']));
        try {
            \DB::beginTransaction();

            $new = new News();
            $new->title         = $request->title;
            $new->datetime      = date('Y-m-d H:i:s', strtotime($request->datetime));
            $new->content       = $request->content_text;
            $new->save();

            $filename = \Str::uuid().'_'.$new->id.'.'.$request->image->getClientOriginalExtension();
            $request->image->storeAs('public/news',$filename);

            $new->image_path    = 'storage/news/';
            $new->image_name    = $filename;
            $new->save();

            \DB::commit();

            return redirect()->route('admin.news.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Datos guardados']));
        }catch (\Exception $e){
            dd($e->getMessage());

            \DB::rollBack();
            return redirect()->back()->withInput()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));;
        }
    }

    public function edit($id)
    {
        $new = News::find($id);

        return view('administration.news.form', compact('new'));
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        try {
            \DB::beginTransaction();

            $new = News::find($id);
            $new->title         = $request->title;
            $new->content       = $request->content_text;
            $new->datetime      = date('Y-m-d H:i:s', strtotime($request->datetime));

            if ($request->image) {

                \Storage::delete('public/news/'.$new->image_name);
                $filename = \Str::uuid().'_'.$new->id.'.'.$request->image->getClientOriginalExtension();
                $request->image->storeAs('public/news',$filename);

                $new->image_path    = 'storage/news/';
                $new->image_name    = $filename;
            }

            $new->save();

            \DB::commit();

            return redirect()->route('admin.news.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Datos guardados']));
        }catch (\Exception $e){
            dd($e->getMessage());
            \DB::rollBack();
            return redirect()->back()->withInput()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));;
        }
    }

    public function destroy($id)
    {
        try {
            \DB::beginTransaction();

            $new = News::find($id);
            \Storage::delete('public/news/'.$new->image_name);
            $new->delete();

            \DB::commit();

            return redirect()->route('admin.news.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Noticia eliminada']));
        }catch (\Exception $e){

            \DB::rollBack();
            return redirect()->back()->withInput()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));;
        }
    }
}
