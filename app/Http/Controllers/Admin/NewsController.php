<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Mail\contactMail;
use App\Mail\futureMail;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $news = News::orderBy('datetime', 'DESC')->paginate(20);

        return view('administration.news.index', compact('news'));
    }

    public function create()
    {
        $categories = Category::pluck('name', 'id');
        return view('administration.news.form', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'content_text' => 'required',
            'categories' => 'required',
            'datetime' => 'required'
        ],[],[
            'content_text' => 'contenido',
        ]);
        //return redirect()->back()->withInput()->with('notification', json_encode(['type' => 'error', 'message' => 'Correcto']));
        try {
            \DB::beginTransaction();

            $new = new News();
            $new->title         = $request->title;
            $new->datetime      = date('Y-m-d H:i:s', strtotime($request->datetime));
            $new->content       = $request->content_text;
            $new->autor         = $request->autor;
            $new->save();

            $new->categories()->sync($request->categories);

            $filename = \Str::uuid().'_'.$new->id.'.'.$request->image->getClientOriginalExtension();
            $request->image->storeAs('public/news',$filename);

            $new->image_path    = 'storage/news/';
            $new->image_name    = $filename;
            $new->save();

            \DB::commit();

            return redirect()->route('admin.news.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Datos guardados']));
        }catch (\Exception $e){
            dd($e->getMessage());

            \DB::rollBack();
            return redirect()->back()->withInput()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));
        }
    }

    public function edit($id)
    {
        $new = News::find($id);
        $categories = Category::pluck('name', 'id');

        return view('administration.news.form', compact('new', 'categories'));
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        try {
            \DB::beginTransaction();

            $new = News::find($id);
            $new->title         = $request->title;
            $new->content       = $request->content_text;
            $new->datetime      = date('Y-m-d H:i:s', strtotime($request->datetime));
            $new->autor         = $request->autor;

            if ($request->image) {

                \Storage::delete('public/news/'.$new->image_name);
                $filename = \Str::uuid().'_'.$new->id.'.'.$request->image->getClientOriginalExtension();
                $request->image->storeAs('public/news',$filename);

                $new->image_path    = 'storage/news/';
                $new->image_name    = $filename;
            }

            $new->save();

            $new->categories()->sync($request->categories);

            \DB::commit();

            return redirect()->route('admin.news.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Datos guardados']));
        }catch (\Exception $e){
            dd($e->getMessage());
            \DB::rollBack();
            return redirect()->back()->withInput()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));;
        }
    }

    public function destroy($id)
    {
        try {
            \DB::beginTransaction();

            $new = News::find($id);
            \Storage::delete('public/news/'.$new->image_name);
            $new->delete();

            \DB::commit();

            return redirect()->route('admin.news.index')->with('notification', json_encode(['type' => 'success', 'message' => 'Noticia eliminada']));
        }catch (\Exception $e){

            \DB::rollBack();
            return redirect()->back()->withInput()->with('notification', json_encode(['type' => 'error', 'message' => 'Ocurrió un error']));;
        }
    }
}
