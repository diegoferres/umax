<?php

namespace App\Http\Livewire;

use App\Banners;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class DraggableBanners extends Component
{
    use WithFileUploads;

    public $banners, $banner_name, $banner_image, $order, $status;

    public function sortTable($banners)
    {
        foreach ($banners as $banner) {

            $b = Banners::find($banner['value']);
            $b->order = $banner['order'];
            $b->save();

            //dd($banner);
        }

        $this->banners = Banners::orderBy('order', 'asc')->get();/*this->items = collect($items)->map(function ($id){
            return collect()
        })*/
        //dd($items);

    }

    public function storeImage()
    {
        try {
            DB::beginTransaction();

            if ($banner = Banners::create([
                'name' => $this->banner_name,
                'order' => Banners::max('order') + 1,
                'status' => true
            ])) {
                $img = basename($this->banner_image->store('/public/banners'));
                $banner->image_path = 'banners/'.$img;
                $banner->save();
            }

            DB::commit();

        }catch (\Exception $e){
            dd($e->getMessage());
            DB::rollBack();
        }

        $this->banners = Banners::all();

        request()->session()->flash(
            'notification',
            json_encode(['type' => 'success', 'message' => 'Banner agregado exitósamente'])
        );

        return redirect()->route('admin.banners.index');
        //Session::flash());
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $banner = Banners::find($id);

            if (Storage::exists('public/'.$banner->image_path)) {
                Storage::delete('public/'.$banner->image_path);
            }
            $banner->delete();

            DB::commit();

        }catch (\Exception $e){
            dd($e->getMessage());
            DB::rollBack();
        }

        request()->session()->flash(
            'notification',
            json_encode(['type' => 'success', 'message' => 'Banner eliminado exitósamente'])
        );

        return redirect()->route('admin.banners.index');
    }

    public function switchStatus($id)
    {
        $banner = Banners::find($id);
        $banner->status = $banner->status ? false : true;
        $banner->save();
    }

    public function render()
    {
        return view('livewire.draggable-banners');
    }
}
