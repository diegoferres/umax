<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AddBanners extends Component
{
    public function render()
    {
        return view('livewire.add-banners');
    }
}
