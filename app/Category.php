<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function news()
    {
        return $this->belongsToMany(News::class, NewCategory::class,
            'category_id', 'new_id');
    }
}
