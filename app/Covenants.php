<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Covenants extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'image_path',
        'internal_image_path',
        'external_link',
    ];
}
