<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'image_path',
        'image_name',
        'datetime',
        'published',
        'autor',
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, NewCategory::class,
            'new_id', 'category_id');
    }
}
