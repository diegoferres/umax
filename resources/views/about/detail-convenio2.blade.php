@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Convenio con el MSP y BS e IPS</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>

    <section class="container">
        <div class="row">
            <div class="col-12">
                <nav class="mb-4" aria-label="breadcrumb">
                    <ol class="breadcrumb bg-light p-4">
                        <li class="breadcrumb-item"><a href="{{ route('convenios') }}">Convenios</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Convenio con el MSP y BS e IPS</li>
                    </ol>
                </nav>
                <img src="{{ asset('images/convenio-mspbs.png') }}" alt="" class="img-fluid mb-5" data-aos="fade-up">
                <h3 class="mt-5 mb-4" data-aos="fade-up">Convenio con el MSP y BS e IPS</h3>
                <p data-aos="fade-up">En su carácter de institución formadora de profesionales de la salud, la UMAX
                    establece alianzas estratégicas con las instituciones más importantes a nivel nacional, para ampliar
                    los conocimientos de sus estudiantes ya en el campo real.
                    <br>
                    <br>
                    En la actualidad, la Universidad María Auxiliadora lleva adelante los convenios de cooperación con
                    el Ministerio de Salud Público y Bienestar Social y el Instituto de Previsión Social, para la
                    realización de las prácticas hospitalarias que son parte del plan académico tanto de Medicina como
                    de Licenciatura en Enfermería.
                    <br>
                    <br>
                    De esta manera tanto el Ministerio como el IPS pone a disposición sus instalaciones en diversos
                    hospitales para llevar adelante este proceso formativo distribuidos de la siguiente manera:
                </p>
                <ul data-aos="fade-up">
                    <li>Hospital Central del IPS</li>
                    <li>Hospital Distrital de Lambaré</li>
                    <li>Hospital Distrital de Mariano Roque Alonso</li>
                    <li>Hospital Distrital de Limpio</li>
                    <li>Hospital General Barrio Obrero</li>
                    <li>Hospital General San Lorenzo</li>
                    <li>Hospital Materno Infantil Santísima Trinidad</li>
                    <li>Hospital Materno Infantil de Loma Pytá</li>
                    <li>Hospital Regional de Caacupé</li>
                </ul>
            </div>

        </div>

    </section>
@endsection
