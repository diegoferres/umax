<div class="malla">
    <div class="container">
        <div class="d-flex justify-content-between">
            <h3 class="mb-5" data-aos="fade-left">Malla Curricular</h3>
            <ul class="nav nav-pills nav-date date mb-3" id="pills-tab" role="tablist">
                {{--<li class="nav-item">
                    <a class="nav-link active" id="pills-2015-tab" data-toggle="pill" href="#pills-2015" role="tab"
                       aria-controls="pills-2015" aria-selected="true">2015</a>
                </li>--}}
                <li class="nav-item">
                    <a class="nav-link active" id="pills-2018-tab" data-toggle="pill" href="#pills-2018" role="tab"
                       aria-controls="pills-2018" aria-selected="false">2018</a>
                </li>
            </ul>
        </div>
        <div class="tab-content" id="pills-tabContent">
            {{--<div class="tab-pane fade show active" id="pills-2015" role="tabpanel" aria-labelledby="pills-2015-tab">
                <div class="malla-tab" data-aos="fade-up">
                    <nav class="nav nav-pills flex-column flex-sm-row">
                        <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#primer"
                           role="tab" aria-controls="primer" aria-selected="true">PRIMER AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo" role="tab"
                           aria-controls="segundo" aria-selected="false">SEGUNDO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero" role="tab"
                           aria-controls="tercero" aria-selected="false">TERCER AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto" role="tab"
                           aria-controls="cuarto" aria-selected="false">CUARTO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto" role="tab"
                           aria-controls="quinto" aria-selected="false">QUINTO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#sexto" role="tab"
                           aria-controls="sexto" aria-selected="false">SEXTO AÑO</a>
                    </nav>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="primer" role="tabpanel" aria-labelledby="primer-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p>
                                    <p>Histología Humana I</p>
                                    <p>Matemática Aplicada a la Medicina</p>
                                    <p>Medicina de la Comunidad</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Biología Celular</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Anatomía Humana II</p>
                                    <p>Histología Humana II</p>
                                    <p>Embriología y Genética</p>
                                    <p>Epidemiología y Ecología</p>
                                    <p>Biofísica Médica</p>
                                    <p>Bioestadística y Demografía</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="segundo" role="tabpanel" aria-labelledby="segundo-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>3º SEMESTRE</h5>
                                    <p>Fisiología Humana I</p>
                                    <p>Microbiología, Parasitología e Inmunología I</p>
                                    <p>Bioquímica Médica I</p>
                                    <p>Metodología de la Investigación</p>
                                    <p>Guaraní</p>
                                    <p>Inglés</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>4º SEMESTRE</h5>
                                    <p>Fisiología Humana II</p>
                                    <p>Microbiología, Parasitología e Inmunología II</p>
                                    <p>Bioquímica Médica II</p>
                                    <p>Psicología Médica</p>
                                    <p>Socio-antropología</p>
                                    <p>Primeros Auxilios</p>
                                    <p>Principios y Valores Humanos y Espirituales</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tercero" role="tabpanel" aria-labelledby="tercero-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>5º SEMESTRE</h5>
                                    <p>Fisiopatología I</p>
                                    <p>Anatomía Patológica I</p>
                                    <p>Medicina Preventiva y Salud Pública</p>
                                    <p>Medicina Familiar</p>
                                    <p>Informática Aplicada a la Medicina</p>
                                    <p>Patología Médica I</p>
                                    <p>Patología Quirúrgica I</p>
                                    <p>Optativa I</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>6º SEMESTRE</h5>
                                    <p>Fisiopatología II</p>
                                    <p>Anatomía Patológica II</p>
                                    <p>Economía</p>
                                    <p>Ética y Bioética Médica</p>
                                    <p>Medicina Legal</p>
                                    <p>Patología Médica II</p>
                                    <p>Patología Quirúrgica II</p>
                                    <p>Psicopatología</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="cuarto" role="tabpanel" aria-labelledby="cuarto-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>7º SEMESTRE</h5>
                                    <p>Semiología Médica I</p>
                                    <p>Semiología Quirúrgica I</p>
                                    <p>Farmacología I</p>
                                    <p>Otorrinolaringología</p>
                                    <p>Nutrición Clínica</p>
                                    <p>Medicina de Imágenes</p>
                                    <p>Psiquiatría</p>
                                    <p>Administración de Servicios de Salud</p>
                                    <p>Optativa II</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>8º SEMESTRE</h5>
                                    <p>Semiología Médica II</p>
                                    <p>Semiología Quirúrgica II</p>
                                    <p>Farmacología II</p>
                                    <p>Medicina Tropical</p>
                                    <p>Dermatología</p>
                                    <p>Oftalmología</p>
                                    <p>Urología</p>
                                    <p>Anestesiología</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="quinto" role="tabpanel" aria-labelledby="quinto-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>9º SEMESTRE</h5>
                                    <p>Cirugía I</p>
                                    <p>Ginecología y Obstetricia I</p>
                                    <p>Medicina Interna I</p>
                                    <p>Pediatría I</p>
                                    <p>Toxicología</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>10º SEMESTRE</h5>
                                    <p>Cirugía II</p>
                                    <p>Ginecología y Obstetricia II</p>
                                    <p>Medicina Interna II</p>
                                    <p>Pediatría II</p>
                                    <p>Traumatología, Ortopedia y Rehabilitación</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="sexto" role="tabpanel" aria-labelledby="sexto-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>11º y 12º SEMESTRE</h5>
                                    <p>Internado en Medicina Interna</p>
                                    <p>Internado en Cirugía</p>
                                    <p>Internado en Pediatría</p>
                                    <p>Internado en Ginecología y Obstetricia</p>
                                    <p>Internado en Emergentología</p>
                                    <p>Atención Primaria en Salud en la comunidad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-center mt-5 justify-content-md-between flex-column flex-md-row">
                    <div data-aos="fade-right" class="mb-3 mb-md-0">
                        <h4>Título a obtener: MEDICO</h4>
                        <h4>TOTAL HORAS: 8.340 horas reloj</h4>
                        <h4>EXTENSION UNIVERSITARIA: 60 horas reloj</h4>
                    </div>
                    <a href="{{ asset('records/brochure-medicina.pdf') }}" class="btn btn-primary" data-aos="fade-left"
                       target="_blank ">DESCARGAR BROCHURE</a>
                </div>
            </div>--}}
            <div class="tab-pane fade show active" id="pills-2018" role="tabpanel" aria-labelledby="pills-2018-tab">
                <div class="malla-tab" data-aos="fade-up">
                    <nav class="nav nav-pills flex-column flex-sm-row">
                        <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#primer-2018"
                           role="tab" aria-controls="primer-2018" aria-selected="true">PRIMER AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo-2018"
                           role="tab" aria-controls="segundo-2018" aria-selected="false">SEGUNDO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero-2018"
                           role="tab" aria-controls="tercero-2018" aria-selected="false">TERCER AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto-2018" role="tab"
                           aria-controls="cuarto-2018" aria-selected="false">CUARTO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto-2018" role="tab"
                           aria-controls="quinto-2018" aria-selected="false">QUINTO AÑO</a>
                        <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#sexto-2018" role="tab"
                           aria-controls="sexto-2018" aria-selected="false">SEXTO AÑO</a>
                    </nav>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="primer-2018" role="tabpanel"
                             aria-labelledby="primer-2018-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>1º SEMESTRE</h5>
                                    <p>Anatomía Humana I</p>
                                    <p>Biología Celular</p>
                                    <p>Histología Humana I</p>
                                    <p>Comunicación Oral y Escrita</p>
                                    <p>Medicina de la Comunidad</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>2º SEMESTRE</h5>
                                    <p>Anatomía Humana II</p>
                                    <p>Histología Humana II</p>
                                    <p>Embriología</p>
                                    <p>Bioquímica Médica I</p>
                                    <p>Biofísica Médica</p>
                                    <p>Guaraní Conversacional</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="segundo-2018" role="tabpanel" aria-labelledby="segundo-2018-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>3º SEMESTRE</h5>
                                    <p>Fisiología Humana I</p>
                                    <p>Microbiología I</p>
                                    <p>Inmunología General</p>
                                    <p>Bioquímica Médica II</p>
                                    <p>Atención primaria de salud</p>
                                    <p>Metodología de la Investigación</p>
                                    <p>Socioantropología</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>4º SEMESTRE</h5>
                                    <p>Fisiología Humana II</p>
                                    <p>Microbiología II</p>
                                    <p>Inglés</p>
                                    <p>Bioestadística e Informática</p>
                                    <p>Psicología Médica</p>
                                    <p>Optativa I</p>
                                    <p>Módulo Integrador I</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tercero-2018" role="tabpanel" aria-labelledby="tercero-2018-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>5º SEMESTRE</h5>
                                    <p>Anatomía Patológica I</p>
                                    <p>Fisiopatología I</p>
                                    <p>Patología Médica I</p>
                                    <p>Patología Quirúrgica I</p>
                                    <p>Epidemiología</p>
                                    <p>Metodología de la Investigación II</p>
                                    <p>Primeros Auxilios</p>
                                    <p>Principios y Valores I</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>6º SEMESTRE</h5>
                                    <p>Anatomía Patológica II</p>
                                    <p>Fisiopatología II</p>
                                    <p>Patología Médica II</p>
                                    <p>Patología Quirúrgica II</p>
                                    <p>Ética y Bioética</p>
                                    <p>Medicina Familiar</p>
                                    <p>Principios y Valores II</p>
                                    <p>Optativa II</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="cuarto-2018" role="tabpanel" aria-labelledby="cuarto-2018-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>7º SEMESTRE</h5>
                                    <p>Semiología Médica I</p>
                                    <p>Semiología Quirúrgica I</p>
                                    <p>Salud Pública, Administración y Gestión en Salud</p>
                                    <p>Imagenología</p>
                                    <p>Farmacología I</p>
                                    <p>Medicina Legal</p>
                                    <p>Principios y Valores III</p>
                                    <p>Módulo Integrador II</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>8º SEMESTRE</h5>
                                    <p>Semiología Médica II</p>
                                    <p>Semiología Quirúrgica II</p>
                                    <p>Psiquiatría</p>
                                    <p>Toxicología</p>
                                    <p>Farmacología II</p>
                                    <p>Otorrinolaringología</p>
                                    <p>Medicina Tropical</p>
                                    <p>Principios y Valores IV</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="quinto-2018" role="tabpanel" aria-labelledby="quinto-2018-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>9º SEMESTRE</h5>
                                    <p>Cirugía I</p>
                                    <p>Ginecología y Obstetricia I</p>
                                    <p>Medicina Interna I</p>
                                    <p>Pediatría I</p>
                                    <p>Traumatología, Ortopedia y Rehabilitación</p>
                                    <p>Dermatología</p>
                                </div>
                                <div class="col-md-6">
                                    <h5>10º SEMESTRE</h5>
                                    <p>Cirugía II</p>
                                    <p>Ginecología y Obstetricia II</p>
                                    <p>Medicina Interna II</p>
                                    <p>Urología</p>
                                    <p>Oftalmología</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="sexto-2018" role="tabpanel" aria-labelledby="sexto-2018-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>11º y 12º SEMESTRE</h5>
                                    <p>Internado en Medicina Interna</p>
                                    <p>Internado en Ginecología y Obstetricia</p>
                                    <p>Internado en Cirugía</p>
                                    <p>Internado en Pediatría</p>
                                    <p>Internado en Emergentología</p>
                                    <p>Internado A. P. en Salud en la comunidad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-center mt-5 justify-content-md-between flex-column flex-md-row">
                    <div data-aos="fade-right" class="mb-3 mb-md-0">
                        <h4>Título a obtener: MEDICO</h4>
                        <h4>TOTAL HORAS: 7.789 horas reloj.</h4>
                        <h4>EXTENSION UNIVERSITARIA: 60 horas reloj</h4>
                    </div>
                    <a href="{{ asset('records/brochure-medicina_nuevo.pdf') }}" class="btn btn-primary" data-aos="fade-left"
                       target="_blank ">DESCARGAR BROCHURE</a>
                </div>
            </div>
        </div>
    </div>
</div>
