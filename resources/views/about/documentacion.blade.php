<div class="requisitos">
    <div class="container">
        <h3 class="mb-5" data-aos="fade-up">Requisitos de inscripción</h3>
        <div class="row">
            <div class="col-md-6" data-aos="fade-right" data-aos-duration="800">
                <h4 class="bg-primary p-3 mb-4">Nacionales</h4>
                <p><ion-icon name="document-text-outline" class="text-primary"></ion-icon> Copia de Cédula de Identidad</p> 
                <p><ion-icon name="document-text-outline" class="text-primary"></ion-icon> Copia de Certificado de Estudio</p>
                <h6 class="text-primary mb-1"><strong>OBS.</strong></h6><small>Los alumnos tendrán un tiempo límite de 60 días paran presentar todos sus documentos originales <b>(Radicación o Escolar y Certificado de Nacimiento o de Matrimonio)</b> a partir del día de inicio de clases.</small>
                <h4 class="mt-5 text-primary">Matriculación</h4>
                <p><ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Certificado de Estudios de la Educación Media original.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Certificado de Nacimiento original.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> 1 fotocopia autenticada por Escribanía del Certificado de Estudios de la Educación Media.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> 1 fotocopia autenticada por Escribanía del Certificado de Nacimiento.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> 2 fotocopias autenticadas por Escribanía de la cédula de identidad.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> 3 fotos tipo carnet con fondo blanco o rojo (medidas 3x4 cm).</p>

                <h4 class="mt-5 text-primary">Convalidación de Asignaturas</h4>
                <p class="mb-1" style="opacity: 1"><b>Proveniente de Universidades paraguayas</b></p>
                <p><ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Certificado de Estudios de la carrera de grado original con Legalización del MEC.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Certificado de Programas de Estudios original con Legalización del MEC.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Declaración jurada de autenticidad de los documentos entregados (proveído por UMAX).</p>
                
                <a href="{{ asset('records/2021_medicina.doc') }}" class="btn btn-primary mb-5 mb-md-0" data-aos="fade-left" target="blank">Descargar Formulario</a>
            </div>
            <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                <h4 class="bg-primary p-3 mb-4">Extranjeros</h4>
                <p><ion-icon name="document-text-outline" class="text-primary"></ion-icon> Copia de Cédula de Identidad</p> 
                <p><ion-icon name="document-text-outline" class="text-primary"></ion-icon> Copia de Certificado de Estudio</p>
                <h6 class="text-primary mb-1"><strong>OBS.</strong></h6><small>Los alumnos tendrán un tiempo límite de 60 días paran presentar todos sus documentos originales <b>(Radicación o Escolar y Certificado de Nacimiento o de Matrimonio)</b> a partir del día de inicio de clases.</small>
                <h4 class="mt-5 text-primary">Matriculación</h4>
                <p><ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Certificado de Estudios de la Educación Media, fotocopia con Apostilla original.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Certificado de Nacimiento, fotocopia con Apostilla original.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> 1 fotocopia autenticada por Escribanía del Certificado de Estudios de la Educación Media.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> 1 fotocopia autenticada por Escribanía del Certificado de Nacimiento.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> 2 fotocopias autenticadas por Escribanía del documento de identidad (RG, DNI, Pasaporte).<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> 2 fotocopias autenticadas por Escribanía del carnet de Radicación Permanente o Temporaria fotos tipo carnet con fondo blanco o rojo (medidas 3cm X 4cm).</p>

                <h4 class="mt-5 text-primary">Convalidación de Asignaturas</h4>
                <p class="mb-1" style="opacity: 1"><b>Proveniente de Universidades extranjeras</b></p>
                <p><ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Certificado de Estudios de la carrera de grado original con Apostilla.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Certificado de Programas de Estudios original con Apostilla.<br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Declaración jurada de autenticidad de los documentos entregados (proveído por UMAX).</p>

                <a href="{{ asset('records/2021_medicina.doc') }}" class="btn btn-primary" data-aos="fade-left" target="blank">Descargar Formulario</a>
            </div>
        </div>
    </div>
</div>