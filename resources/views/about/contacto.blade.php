@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Contacto</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university pb-5">
        <div class="asesores">
            <div class="container">
                <style>
                    header {
                        border: 3px solid;
                        min-height: 230px;
                        position: relative;
                    }

                    header > h1 {
                        position: absolute;
                        top: -35px;
                        left: 50px;
                        background: #fff;
                        padding: 0 20px;
                    }

                    h6 {
                        font-size: 0.8em;
                    }
                </style>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-auto">
                            <div class="row">
                                <div class="col-auto mt-4">
                                    <header style="border-color: red"><h1
                                            style="font-size: 20px; background-color: red; color: white" class="mt-3">
                                            Asesores de Ventas </h1>
                                        <div class="col-md-12 mb-2">
                                            <hr>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="card aos-init aos-animate   " data-aos="flip-left">
                                                    <div class="avatar  ">
                                                        <img src="/images/asesor_JOHNI.jpg" alt=""
                                                             class="img-fluid">
                                                    </div>
                                                    <div class="caption  ">
                                                        <h6> Johnatan Baudelet <br>
                                                            <span style="color:red"><a target="_blank"
                                                                                       href="https://wa.me/595986723806">+595 986 723 806</a></span>
                                                        </h6>

                                                    </div>
                                                </div>
                                                <div class="card aos-init aos-animate  " data-aos="flip-left">
                                                    <div class="avatar  ">
                                                        <img src="/images/asesor_JOSE.jpg" alt=""
                                                             class="img-fluid">
                                                    </div>
                                                    <div class="caption  ">
                                                        <h6> José Recalde <br>
                                                            <span style="color:red"><a target="_blank"
                                                                                       href="https://wa.me/595986723806">+595 985 803 148</a></span>
                                                        </h6>

                                                    </div>
                                                </div>
                                                <div class="card aos-init aos-animate  " data-aos="flip-left">
                                                    <div class="avatar  ">
                                                        <img src="/images/asesor_OSMAR.jpg" alt=""
                                                             class="img-fluid">
                                                    </div>
                                                    <div class="caption  ">
                                                        <h6> Osmar Martínez <br>
                                                            <span style="color:red"><a target="_blank"
                                                                                       href="https://wa.me/595986723806">+595 982 336 034</a></span>
                                                        </h6>

                                                    </div>
                                                </div>
                                                <div class="card aos-init aos-animate  " data-aos="flip-left">
                                                    <div class="avatar  ">
                                                        <img src="/images/asesor_PAOLO.jpg" alt=""
                                                             class="img-fluid">
                                                    </div>
                                                    <div class="caption  ">
                                                        <h6> Paolo Tifi <br>
                                                            <span style="color:red"><a target="_blank"
                                                                                       href="https://wa.me/595986723806">+595 981 404 287</a></span>
                                                        </h6>

                                                    </div>
                                                </div>
                                                <div class="card aos-init aos-animate  " data-aos="flip-left">
                                                    <div class="avatar  ">
                                                        <img src="/images/asesor_ROLANDO.jpg" alt=""
                                                             class="img-fluid">
                                                    </div>
                                                    <div class="caption  ">
                                                        <h6> Rolando Oviedo <br>
                                                            <span style="color:red"><a target="_blank"
                                                                                       href="https://wa.me/595986723806">+595 984 558 222</a></span>
                                                        </h6>

                                                    </div>
                                                </div>
                                                <div class="card aos-init aos-animate  " data-aos="flip-left">
                                                    <div class="avatar  ">
                                                        <img src="/images/asesor_GABRIELA.jpg" alt=""
                                                             class="img-fluid">
                                                    </div>
                                                    <div class="caption  ">
                                                        <h6> Gabriela Rojas <br>
                                                            <span style="color:red"><a target="_blank"
                                                                                       href="https://wa.me/595985803158">+595 985 803 158</a></span>
                                                        </h6>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </header>
                                </div>

                                <div class="col-auto mt-4">
                                    <header style="border-color: red"><h1
                                            style="font-size: 20px; background-color: red; color: white" class="mt-3">
                                            Promotores </h1>
                                        <div class="col-md-12 mb-2">

                                            <hr>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">


                                                <div class="card aos-init aos-animate  " data-aos="flip-left">
                                                    <div class="avatar  ">
                                                        <img src="/images/promotor_Magno.jpg" alt=""
                                                             class="img-fluid">
                                                    </div>
                                                    <div class="caption  ">
                                                        <h6> Magno Brito <br>
                                                            <span style="color:red"><a target="_blank"
                                                                                       href="https://wa.me/595986723806">+595 994 908 200</a></span>
                                                        </h6>

                                                    </div>
                                                </div>

                                                <div class="card aos-init aos-animate  " data-aos="flip-left">
                                                    <div class="avatar  ">
                                                        <img src="/images/promotor_Renata.jpg" alt=""
                                                             class="img-fluid">
                                                    </div>
                                                    <div class="caption  ">
                                                        <h6> Renata Gomes <br>
                                                            <span style="color:red"><a target="_blank"
                                                                                       href="https://wa.me/595986723806">+595 994 690 621</a></span>
                                                        </h6>

                                                    </div>
                                                </div>
                                                <div class="card aos-init aos-animate  " data-aos="flip-left">
                                                    <div class="avatar  ">
                                                        <img src="/images/promotor_Leticia.jpg" alt=""
                                                             class="img-fluid">
                                                    </div>
                                                    <div class="caption  ">
                                                        <h6> Leticia Batista <br>
                                                            <span style="color:red"><a target="_blank"
                                                                                       href="https://wa.me/595986723806">+595 995 689 998</a></span>
                                                        </h6>

                                                    </div>
                                                </div>
                                                <div class="card aos-init aos-animate  " data-aos="flip-left">
                                                    <div class="avatar  ">
                                                        <img src="/images/promotor_MAX.jpg" alt=""
                                                             class="img-fluid">
                                                    </div>
                                                    <div class="caption  ">
                                                        <h6> Max Waldemar <br>
                                                            <span style="color:red"><a target="_blank"
                                                                                       href="https://wa.me/595986723806">+595 986 518 959</a></span>
                                                        </h6>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </header>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="mt-2">
        <div class="contact">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-6" data-aos="fade-right">
                        <h3 class="mb-4">Dejanos un mensaje</h3>
                        <form action="{{ route('mail.send.contact') }}" method="get">
                            <div class="form-group mb-4" data-aos="fade-up">
                                <input type="text" class="form-control" id="inputName" name="name"
                                       placeholder="Nombre y Apellido" required>
                            </div>
                            <div class="form-group" data-aos="fade-up">
                                <input type="email" class="form-control" id="inputEmail" name="email"
                                       placeholder="Corrreo electrónico" required>
                            </div>
                            <div class="form-group" data-aos="fade-up">
                                <input type="text" class="form-control" id="inputCelular" name="cellphone"
                                       placeholder="Celular" required>
                            </div>
                            <div class="form-group mb-4" data-aos="fade-up">
                            <textarea class="form-control" id="mensaje" name="message" placeholder="Mensaje"
                                      rows="3"></textarea>
                            </div>
                            <div class="d-flex justify-content-md-end mt-5" data-aos="fade-up">
                                <button class="btn btn-primary mb-5 mb-md-0" type="submit">Enviar mensaje</button>
                                {{--<a href="#" class="btn btn-primary mb-5 mb-md-0">Enviar mensaje</a>--}}
                            </div>
                        </form>
                    </div>
                    <div class="col-md-5 offset-md-1">
                        <h3 class="mb-4" data-aos="fade-left">Información de contacto</h3>
                        <h4 data-aos="fade-up">Mario Halley Mora c/ Palo Santo. Mariano Roque Alonso - Paraguay.</h4>
                        <h4 data-aos="fade-up">Lun - Vie 08.00 - 21.00; Sab 08.00 - 12.00</h4>
                        <a href="tel:+59521296900"><h4 class="my-5" data-aos="fade-up">+595 21296900</h4></a>
                        <a href="mailto:info@umax.edu.py"><h4 data-aos="fade-up">info@umax.edu.py</h4></a>
                    </div>
                    <div class="col-md-12">
                        <h4 class="mb-4 mt-5" data-aos="fade-left">Contactos y Horarios de Atención</h4>
                        <div class="row">
                            <div class="col-md-3">
                                <hr>
                                <b>Atención al Estudiante:</b>
                                <br>
                                Lunes a Viernes: 7:30 a 19:00 hs.
                                <br>
                                Sábados: 8:00 a 12:00 hs.
                                <br>
                                <br>
                                <b>Contacto:</b>
                                <br>
                                atencion.estudiantes@umax.edu.py
                                <br>
                                +595 986 623 807
                            </div>
                            <div class="col-md-3">
                                <hr>
                                <b>Soporte Académico:</b>
                                <br>
                                Lunes a Viernes: 8:00 a 18:00 hs.
                                <br>
                                <br>
                                <b>Contacto:</b>
                                <br>
                                soporte.academico@umax.edu.py
                                <br>
                                +595 974 570 763
                            </div>
                            <div class="col-md-3">
                                <hr>
                                <b>Cajas:</b>
                                <br>
                                Lunes a Viernes: 7:30 a 20:00  hs.
                                <br>
                                Sábados: 8:00 a 12:00 hs.
                                <br>
                                <br>
                                <b>Contactos:</b>
                                <br>
                                +595 985 606 042
                                <br>
                                +595 974 570 762
                                <br>
                                +595 981 249 847
                                <br>
                                +595 974 570 765

                            </div>
                            <div class="col-md-3">
                                <hr>
                                <b>Secretaría de Decanato de Medicina:</b>
                                <br>
                                Lunes a Viernes: 8:00 a 18:00 hs.
                                <br>
                                <br>
                                <b>Contacto:</b>
                                <br>
                                decanato.medicina@umax.edu.py
                                <br>
                                +595 986 756 443
                            </div>
                            <div class="col-md-3">
                                <hr>
                                <b>Secretaría de Decanato de Licenciatura en Enfermería:</b>
                                <br>
                                Lunes a Viernes: 10:30 a 20:00 hs
                                <br>
                                <br>
                                <b>Contacto:</b>
                                <br>
                                decanato.enfermeria@umax.edu.py
                                <br>
                                +595 961 780 781
                            </div>
                            <div class="col-md-3">
                                <hr>
                                <b>Aulas Virtuales:</b>
                                <br>
                                Lunes a viernes: 8:00 a 18:30 hs.
                                <br>
                                <br>
                                <b>Contacto:</b>
                                <br>
                                aulas.virtuales@umax.edu.py
                                <br>
                                +595 974 570 772
                            </div>
                            <div class="col-md-3">
                                <hr>
                                <b>Biblioteca:</b>
                                <br>
                                Lunes a Viernes: 08:00 a 18:30 hs.
                                <br>
                                <br>
                                <b>Contacto:</b>
                                <br>
                                biblioteca@umax.edu.py
                            </div>
                            <div class="col-md-3">
                                <hr>
                                <b>Bienestar Estudiantil:</b>
                                <br>
                                Lunes a Viernes: 07:30 a 18:00 hs.
                                <br>
                                <br>
                                <b>Contacto:</b>
                                <br>
                                bienestar_estudiantil@umax.edu.py
                                <br>
                                +595 974 698 522
                            </div>
                            <div class="col-md-3">
                                <hr>
                                <b>Extensión y Vinculación con el Medio:</b>
                                <br>
                                Lunes: 13:00 a 19:00 hs.
                                <br>
                                Martes: 8:00 a 20:00 hs.
                                <br>
                                Miércoles: 08:00 a 13:00 hs.
                                <br>
                                Jueves: 08:00 a 20:00 hs.
                                <br>
                                Viernes: 08: a 13:00 hs.
                                <br>
                                <br>
                                <b>Contacto:</b>
                                <br>
                                extension.vinculacion@umax.edu.py
                            </div>
                            <div class="col-md-3">
                                <hr>
                                <b>Pastoral:</b>
                                <br>
                                Lunes a Viernes: 7:30 a 17:30 hs.
                                <br>
                                <br>
                                <b>Contacto:</b>
                                <br>
                                pastoral@umax.edu.py
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-2 py-5">
                <div class="container">
                    <h4 class="mt-5" data-aos="fade-up">Visitanos</h4>
                    <div class="mapa" data-aos="fade-up">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d115458.11603389579!2d-57.6626722!3d-25.2683609!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x945da5cc419fbb4f%3A0x99bf9e1fa69b6033!2sUniversidad%20Maria%20Auxiliadora%20(SEDE%20NUEVA)!5e0!3m2!1ses!2spy!4v1599128101352!5m2!1ses!2spy"
                            width="100%" height="550" frameborder="0" style="border:0;" allowfullscreen=""
                            aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
