@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Convenios</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>

    <section class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="mb-4" data-aos="fade-up">Convenios vigentes</h3>
            </div>
            @foreach ($covenants as $covenant)
                @if ($covenant->image_path)
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 px-0">
                                <img src="{{ asset('storage/'.$covenant->image_path) }}" class="card-img-top"
                                     alt="Logo">
                            </div>
                            <div class="col-md-12 bg-light">
                                <div class="mt-2 p-4">
                                    <h3 class="card-title mt-2">{{ $covenant->title }}</h3>
                                    <a href="{{ route('convenio.interna', $covenant->id) }}" class="">Más
                                        Información</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
            {{--<div class="col-md-6">
                <div class="row">
                    <div class="col-md-12 px-0" style="max-height: 165px">
                        <img src="{{ asset('images/convenio2.jpg') }}" class="card-img-top" alt="Logo">
                    </div>
                    <div class="col-md-12 bg-light">
                        <div class="mt-2 p-4">
                            <h3 class="card-title mt-2">Convenio con el MSP y BS e IPS</h3>
                            <a href="{{ route('convenio-2') }}" class="">Más Información</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12 px-0" style="max-height: 165px">
                        <img src="{{ asset('images/convenio3.jpg') }}" class="card-img-top" alt="Logo">
                    </div>
                    <div class="col-md-12 bg-light">
                        <div class="mt-2 p-4">
                            <h3 class="card-title mt-2">Universidad Nacional del Nordeste  (UNNE)</h3>
                            <a href="{{ route('convenio-3') }}" class="">Más Información</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12 px-0" style="max-height: 165px">
                        <img src="{{ asset('images/convenio4.jpg') }}" class="card-img-top" alt="Logo">
                    </div>
                    <div class="col-md-12 bg-light">
                        <div class="mt-2 p-4">
                            <h3 class="card-title mt-2">Convenio entre la UMAX y el Instituto Nacional de Educación Superior
                                “Dr. Peña”</h3>
                            <a href="{{ route('convenio-4') }}" class="">Más Información</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12 px-0" style="max-height: 165px">
                        <img src="{{ asset('images/convenio5.jpg') }}" class="card-img-top" alt="Logo">
                    </div>
                    <div class="col-md-12 bg-light">
                        <div class="mt-2 p-4">
                            <h3 class="card-title mt-2">Convenio entre la UMAX y la Universidad Regional Integrada del Alto
                                Uruguay y Misiones de Brasil (URI)</h3>
                            <a href="{{ route('convenio-5') }}" class="">Más Información</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12 px-0" style="max-height: 165px">
                        <img src="{{ asset('images/convenio6.jpg') }}" class="card-img-top" alt="Logo">
                    </div>
                    <div class="col-md-12 bg-light">
                        <div class="mt-2 p-4">
                            <h3 class="card-title mt-2">Convenio entre la UMAX y el Hospital Reina Sofía de la Cruz Roja
                                Paraguaya</h3>
                            <a href="{{ route('convenio-6') }}" class="">Más Información</a>
                        </div>
                    </div>
                </div>
            </div>--}}
            <div class="col-md-4 offset-1 mt-4">
                <h4>Otros Convenios</h4>
                <span>
                @foreach ($covenants->where('image_path', null) as $covenant)
                    {{--@if (!is_null($covenant->content))--}}
                       {{-- <a href="{{ route('convenio.interna', $covenant->id) }}">• {{ $covenant->title }}</a>--}}
                    {{--@else--}}
                        • {{ $covenant->title }}<br>
                    {{--@endif--}}
                @endforeach
                </span>
                {{--<span>
                    • Cooperativa San Cristóbal Ltda. <br>
                    • Cooperativa Nazareth. <br>
                    • Odonto Excellence. <br>
                    • MG Gimnasio. <br>
                    • La Josefina (Bar & Restaurante). <br>
                    • El Comedor de La Rural.
                </span>--}}
            </div>
        </div>

    </section>


@section('scripts')
    {{--  <script>
         new Glider(document.querySelector('.glider'), {
             slidesToShow: 3.5,
             dots: '#scrollLockDelay',
             draggable: true,
             arrows: {
                 next: '.glider-next',
                 prev: '.glider-prev'
             }
         });
     </script> --}}
@endsection

@endsection
