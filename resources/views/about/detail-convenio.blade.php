@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Convenio Universidad Salamanca</h2>
        </div>
        <img src="{{ asset('images/convenio1.jpg') }}" alt="" class="img-fluid">
    </div>

   <section class="container">
       <div class="row">
           <div class="col-12">
                <nav class="mb-4" aria-label="breadcrumb">
                    <ol class="breadcrumb bg-light p-4">
                        <li class="breadcrumb-item"><a href="{{ route('convenios') }}">Convenios</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Convenio Universidad Salamanca</li>
                    </ol>
                </nav>
                <img src="{{ asset('images/salamanca-2.jpg') }}" alt="" class="img-fluid mb-5" data-aos="fade-up">
                <h3 class="mt-5 mb-4" data-aos="fade-up">CONVENIO INTERUNIVERSITARIO ENTRE LA UNIVERSIDAD DE SALAMANCA (REINO DE ESPAÑA) y UNIVERSIDAD MARÍA AUXILIADORA</h3>
                <p data-aos="fade-up">En el marco del proceso de la internacionalización de las universidades se ha firmado: </p>
                <h4 class="my-4" data-aos="fade-up">CONVENIO BÀSICO DE COLABORACIÒN UNIVERSDITARIA INTERNACIONAL ENTRE LA UNIVERSIDAD DE SALAMANCA (REINO DE ESPAÑA) Y LA UNIVERSIDAD MARÌA AUXILIADORA (PARAGUAY)</h4>
                <p data-aos="fade-up">Dicho convenio estipula lo siguiente:</p>
                <ol data-aos="fade-up">
                    <li>Elaboración de programas de movilidad de investigadores, personal docente y estudiantes.</li>
                    <li>Realización de ediciones conjuntas en áreas de interés común de las instituciones vinculadas</li>
                    <li>Realización de proyectos de investigación</li>
                    <li>Creación y organización de actividades docentes coordinadas</li>
                    <li>Organización de Coloquios internacionales</li>
                </ol>
                <h4 class="my-4" data-aos="fade-up">II. CONVENIO PARA LA MOVILIDAD DE ESTUDIANTES </h4>
                <p data-aos="fade-up">Su fin es establecer un programa de movilidad de estudiantes de titulaciones oficiales de grado, máster y doctorado entre las partes. <br><br>
                    Lo cual permite que los estudiantes de origen pueden recibir formación académica en la Institución de destino (Estudiantes de la UMAX en Salamanca y Estudiantes de Salamanca en UMAX)
                </p>
                <ol data-aos="fade-up">
                    <li>La movilidad de estudiantes puede tener una duración mínima de un semestre y máxima de un curso completo</li>
                    <li>Los candidatos de la movilidad deben manejar muy bien el idioma de la Universidad (El español)</li>
                    <li>Los estudiantes de la movilidad deben tener el seguro de cobertura de la movilidad</li>
                    <li>Las candidaturas para la movilidad se presentan en las oficinas responsables de cada institución </li>
                    <li>Se debe evaluar el rendimiento académico de los estudiantes</li>
                    <li>Se expedirá certificado de estos cursos</li>
                    <li>El reconocimiento de crédito queda a cargo de la institución de origen</li>
                </ol>
                <p data-aos="fade-up">Este convenio es de trascendental importancia para los estudiantes de la UMAX y también para los de Salamanca. Pues los estudiantes tienen la brillante  ocasión de expandir y enriquecer su cultura y su conocimiento en el área que quisiera dentro de las líneas del CONVENIO. <br><br>
                    La universidad María Auxiliadora apunta con este tipo de convenio a enriquecer con idoneidad y cultura general a sus estudiantes de cualquiera de las carreras. <br><br>
                    Cabe destacar que la Universidad de Salamanca tiene 800 años de servicio académico en el mundo. Es la universidad más prestigiosa en el mundo ibérico y ha contribuido enormemente en la defensa de los Derechos Humanos en la conquista y colonización de América.
                </p>
           </div>

       </div>

   </section>
@endsection
