@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Medicina</h2>
        </div>
        <img src="{{ asset('images/banner-medicina.png') }}" alt="" class="img-fluid">
    </div>

    <section class="university">
        <div class="container">
            <div class="habilitacion" data-aos="fade-up">
                <div class="row align-items-center">
                    <div class="col-md-3">
                        <h3>Habilitación y acreditación de la carrera</h3>
                    </div>
                    <div class="col-md-3">
                        <div class="d-flex">
                            <img src="{{ asset('images/aneaes.png') }}" alt="" class="w-50">
                            <img src="{{ asset('images/cones.png') }}" alt="" class="w-50">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <p>Reconocimiento de habilitación de la carrera de Medicina por el Consejo Nacional de Educación
                            Superior (CONES)</p>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ asset('records/cones-medicina.pdf') }}" class="btn btn-primary btn-block mb-2"
                           target="blank">Ver Habilitación</a>
                        <a href="{{ asset('records/acreditacion-medicina.pdf') }}" class="btn btn-primary btn-block"
                           target="blank">Ver Acreditación</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="mensaje">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="pr-5">
                            <h5 data-aos="fade-up">MENSAJE DEL DECANO DE LA</h5>
                            <h3 data-aos="fade-up" data-aos-duration="200">Carrera de Medicina</h3>
                            <p data-aos="fade-up" data-aos-duration="800">La Medicina nos elige. Nosotros obedecemos a
                                un llamado que nos empuja a realizarnos a través de todas las dificultades, obstáculos,
                                esfuerzo y cansancio que implica seguir a esa voz interior
                                <br><br>
                                La recompensa es grande, por supuesto, pero no es algo tan visible como las cosas
                                materiales que señalan el éxito para la Sociedad. Es algo intenso y discreto que se da
                                cuando somos conscientes que hemos ayudado a la Naturaleza a paliar el sufrimiento de
                                alguien o cuando percibimos que somos un instrumento divino al participar del milagro de
                                un nacimiento o al acompañar a alguien que se está yendo, sabiendo que hemos hecho todo
                                lo posible por él.
                                <br><br>
                                Estudiar Medicina tiene un comienzo, pero no tiene un final. Es algo imposible de
                                acabar, porque como un ser vivo, crece, cambia y siempre tiene sorpresas y giros que nos
                                obligan a estar pendiente de él. Es una pasión que nos consume, porque queremos saber
                                siempre más y no nos conformamos nunca.
                                <br><br>
                                Es para personas especiales, que sienten el sufrimiento de los demás como propio y
                                buscan a través del conocimiento del Arte de Curar, una herramienta para aliviar al
                                prójimo. Como dijo Samuel Taylor Coleridge, <i><b>“El mejor médico es el que mejor
                                        inspira la esperanza”.</b></i>
                                <br><br>
                                Si eso es lo que te mueve a estudiar Medicina, adelante! Y que se cumpla contigo lo
                                dicho por Hipócrates: <i><b>“Algunos pacientes, aunque conscientes de que su condición
                                        es peligrosa, recuperan su salud simplemente por su satisfacción con la bondad
                                        del médico.”</b></i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="avatar">
                            <img src="{{ asset('images/dr-gustavo.png') }}" alt="" class="img-fluid"
                                 data-aos="fade-left" data-aos-duration="500">
                            <p class="pr-5 mt-4" data-aos="fade-up" data-aos-duration="800">Dr. Gustavo Guillermo
                                Calvo</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="text-nosotros">
                <h4 class="mt-5" data-aos="fade-up">Misión</h4>
                <p data-aos="fade-up" data-aos-duration="800">La carrera de Medicina tiene como misión formar
                    profesionales con un alto nivel científico, competitivo, con pensamiento crítico y reflexivo, con
                    capacidad de integrarse al equipo interdisciplinario de salud, con sólidos principios éticos y
                    compromiso con las personas, familia, comunidad, y un elevado nivel de inteligencia emocional que le
                    permita adecuarse equilibradamente a su realidad local, nacional e internacional, en el ámbito de su
                    competencia y en los distintos niveles de atención al sistema de salud, así como en el campo de la
                    investigación, docencia y extensión social.</p>

                <h4 class="mt-5" data-aos="fade-up">Visión</h4>
                <p data-aos="fade-up" data-aos-duration="800">Ser una carrera con excelencia académica y alto sentido de
                    responsabilidad social, líder en la formación de profesionales Médicos, capaces de confrontar,
                    modificar y plantear soluciones a los problemas de salud de su entorno, donde la docencia, la
                    investigación y la extensión hagan parte integral del proceso formativo del estudiante, generando
                    conocimientos que conlleven al mejoramiento de la calidad de vida de la sociedad paraguaya.</p>

                <h4 class="mt-5" data-aos="fade-up">Perfil del Egresado</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    La formación del profesional médico en la Carrera de Medicina de la
                    Universidad Maria Auxiliadora, se orienta a desarrollar en el egresado
                    competencias genéricas y profesionales requeridas para desenvolverse con
                    eficiencia y eficacia en los ámbitos disciplinares pertinentes a la profesión y que
                    en su conjunto lo perfilan como una persona idónea para realizar prevención,
                    diagnóstico, tratamiento y  rehabilitación de los problemas de salud basados en
                    la investigación, que afectan a la población, evidenciando además un
                    comportamiento ético, con su entorno social, capacidad para trabajar en
                    equipo, liderazgo y protagonismo en trabajos de extensión comunitaria e
                    investigación social.
                </p>
                <h5 class="mt-3" data-aos="fade-up">El/la egresado/a de la carrera de Medicina debe poseer las
                    competencias
                    necesarias para:</h5>
                <p data-aos="fade-up" data-aos-duration="800">
                    <b>1.</b> Ser un profesional de reconocida excelencia, que se desempeñe con
                    responsabilidad y en forma competente en la red de salud tanto de gestión
                    pública o privada, atendiendo integralmente las crecientes demandas de salud
                    de las personas y de la población, reconociendo los derechos de los pacientes,
                    el de la confidencialidad del secreto profesional y el del consentimiento
                    informado.
                    <br>
                    <br>
                    <b>2.</b> Participar en los ámbitos de la promoción, prevención, recuperación y
                    rehabilitación a lo largo del ciclo vital, incorporando una visión humanista con
                    respecto a la familia, la comunidad y su entorno sociocultural.
                    <br>
                    <br>
                    <b>3.</b> Tener razonamiento crítico y reflexivo acerca de su rol social, así como de su
                    labor y profesión, la que ejercerá sobre la base del conocimiento actualizado,
                    considerando el avance de las ciencias, la tecnología y los cambios de los
                    determinantes de la salud, incorporando aquellas innovaciones relevantes para
                    la práctica clínica.
                    <br>
                    <br>
                    <b>4.</b> Ser capaz de trabajar efectivamente en los diferentes equipos de salud, en
                    escenarios diversos en cuanto a complejidad y contexto, en coherencia con los
                    principios y valores éticos, espirituales y morales.
                    <br>
                    <br>
                    <b>5.</b> Desarrollar una relación de comunicación empática, utilizando los idiomas
                    ofíciales, generando acciones orientadas a resolver las necesidades y
                    expectativas de salud del país, optimizando los procesos de gestión desde una
                    perspectiva estratégico-operativa, con el fin de mejorar la salud de la población.
                    <br>
                    <br>
                    <b>6.</b> Desarrollar habilidades investigativas en las diferentes áreas del saber que
                    generen aportes innovadores a las ciencias médicas. 
                    <br>
                    <br>
                    <b>7.</b> Ejercer el liderazgo y positivismo en sus ámbitos de desempeño, consciente
                    de su propia necesidad de crecimiento y formación profesional, realizando
                    actividades de perfeccionamiento continuo y así contribuir a la generación de
                    conocimiento, proyectándose como un referente de excelencia, ya sea como
                    Médico General o Especialista, Administrador o Académico y/o Investigador en
                    el ámbito de la salud.
                </p>
            </div>
        </div>
        @include('about.malla')
        @include('about.documentacion')
        <div class="campos">
            <div class="container">
                <h3 class="mb-4">Campos de prácticas</h3>
                <div class="row">
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital-ips.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title my-auto">Hospital IPS</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital-mariano.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title my-auto">Hospital Distrital Mariano Roque Alonso</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital-lambare.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title my-auto">Hospital Distrital de Lambaré</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital-limpio.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title my-auto">Hospital Distrital de Limpio</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital-sanlorenzo.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title my-auto">Hospital General San Lorenzo</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital-trinidad.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title my-auto">Hospital Materno Infantil Santísima Trinidad</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital-loma.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title my-auto">Hospital Materno Infantil Loma Pyta</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/hospital-caacupe.png') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title my-auto">Hospital Regional de Caacupé</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-left" data-aos-duration="800">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-5">
                                    <img src="{{ asset('images/fachada.jpg') }}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body">
                                        <h6 class="card-title my-auto">USF, 6 de Mayo, Remansito</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="autoridades">
            <div class="container">
                <h5 data-aos="fade-up">Nomina de</h5>
                <h3 class="mb-5" data-aos="fade-up">Autoridades de Medicina</h3>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-gustavo-2.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dr. Gustavo Guillermo Calvo.</h4>
                                <h6>Decano de la Facultad de Medicina.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dra-silva.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dra. Silvia Brizuela.</h4>
                                <h6>Directora Académica.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-ivan.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dr. Iván Javier Díaz Rolón.</h4>
                                <h6>Coordinador de Ciencias Básicas y Pre-Clínicas</h6>
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dra-eileen-mosqueira.jpg') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                --}}{{--<h4>Dra. Jessica Nilse Candia Lezcano.</h4>--}}{{--
                                <h4>Dra. Eileen Mosqueira.</h4>
                                <h6>Coordinadora de Pre-Clínicas.</h6>
                            </div>
                        </div>
                    </div>--}}
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-waldo-mieres.jpg') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                {{--<h4>Dr. Cristian Pestana Sierra.</h4>--}}
                                <h4>Dr. Waldo Ricardo Mieres Romero.</h4>
                                <h6>Coordinador de Clínicas e Internado.</h6>
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-mabel.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Lic. Mabel Cañete.</h4>
                                <h6>Coordinadora Pedagógica.</h6>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>

        @include('partials.form-inscripcion')

    </section>

@section('scripts')
    {{--  <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script> --}}
@endsection

@endsection
