@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Egresados</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="container">
        <div class="container mb-2">
            <div class="text-nosotros">
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                    El seguimiento a egresados de la UMAX forma parte de la política institucional, pues el conocimiento
                    del desempeño profesional de los egresados y el grado de satisfacción de sus empleadores, así como
                    la opinión del egresado sobre su proceso de formación, permiten la identificación de indicadores
                    claves respecto a la evaluación de la calidad del proyecto formativo. El conocimiento de las
                    fortalezas y debilidades de los programas de estudio de las carreras resulta esencial para la
                    gestión y el aseguramiento de la calidad.
                    <br>
                    <br>
                    A la vez, conocer las necesidades de formación continua de los egresados permite a la universidad
                    brindar ofertas académicas acordes a esas necesidades, de manera a seguir contribuyendo
                    significativamente en la formación de sus graduados en los diferentes niveles de postgrado.
                </p>
            </div>
        </div>
        <div class="fotos">
            <div class="container">
                <h3 class="mt-2 py-5" data-aos="fade-up">Galería de fotos</h3>
            </div>
            <!-- Glider-->
            <div class="glider-contain mb-5">
                <div class="glider">
                    <div class="ml-0" data-aos="fade-left">
                        <img src="{{ asset('images/egresados/egresado_1.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/egresados/egresado_2.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/egresados/egresado_3.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/egresados/egresado_4.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-angle-left"></i>
                    </button>
                    <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
            <!-- End Glider-->
        </div>
        <div class="testimonios my-5">
            <div class="container">
                <div id="carouselTestimonios" class="carousel slide" data-ride="carousel">
                    <h3 data-aos="fade-right">Novedades</h3>
                    <a class="carousel-control-prev" href="#carouselTestimonios" role="button" data-slide="prev"
                       data-aos="fade-left">
                        <ion-icon name="chevron-back-outline"></ion-icon>
                    </a>
                    <a class="carousel-control-next" href="#carouselTestimonios" role="button" data-slide="next"
                       data-aos="fade-left">
                        <ion-icon name="chevron-forward-outline"></ion-icon>
                    </a>
                    <ol class="carousel-indicators">
                        <li data-target="#carouselTestimonios" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselTestimonios" data-slide-to="1"></li>
                    </ol>
                    <div class="carousel-inner" data-aos="fade-up">
                        <div class="carousel-item active">
                            <div class="profile">
                                <div class="d-flex align-items-center">
                                    <div class="picture">
                                        <img src="{{ asset('images/dr-juan.png') }}" alt="" class="img-fluid">
                                    </div>
                                    <div class="date">
                                        <h4 class="mb-0"><b>Egresado de la primera Promoción 2014</b></h4>
                                        {{--<small>Tercer Semestre Chile</small>--}}
                                    </div>
                                </div>
                                <div class="caption">
                                    <p>Doctor Juan Aníbal Recalde Rivas, egresado de la primera Promoción 2014.
                                        Destacado en su labor como líder de una compleja cirugía contra el cáncer de
                                        cuello uterino en el Instituto de Previsión Social de Concepción. Una
                                        intervención considerada como inédita en esa región del país y con resultado
                                        satisfactorio para el paciente.
                                        <br>
                                        <br>
                                        El valor de este logro fortalece la imagen de la UMAX como formadora de
                                        profesionales capaces con vocación de servicio, respeto a la ética y a la
                                        ciencia de la salud.</p>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="profile">
                                <div class="d-flex align-items-center">
                                    <div class="picture">
                                        <img src="{{asset('/images/dra-lilian-paiva.png')}}" alt="" class="img-fluid">
                                    </div>
                                    <div class="date">
                                        <h4 class="mb-0"><b>Historias que inspiran</b></h4>
                                        {{--<small>6to Semestre Grupo A Brasil</small>--}}
                                    </div>
                                </div>
                                <div class="caption">
                                    <p>Después de varias décadas la humanidad nuevamente se ve amenazada por una
                                        pandemia.
                                        <br>
                                        El COVID-19 ha transformado todos los aspectos de la vida actual y representa un
                                        enorme desafío para los profesionales de la salud.
                                        <br>
                                        A nivel local, los esfuerzos están concentrados en la prevención y el
                                        tratamiento en una batalla sin descanso.
                                        <br>
                                        En la primera línea del deber médico tenemos el agrado de destacar a la Doctora
                                        Lilian Fabiola Paiva Moreno, egresada de la primera promoción de médicos
                                        formados por la Universidad María Auxiliadora en el año 2014.
                                        <br>
                                        La profesional ejerce el segundo periodo de Residencia en la Unidad de Cuidados
                                        Intensivos del Hospital Nacional de Itauguá.
                                        <br>
                                        Esto significa verse cara a cara con la fase más compleja de la enfermedad,
                                        donde brinda su capacidad y compromiso profesional al servicio de los pacientes.
                                        <br>
                                        En palabras de la Doctora Paiva, la capacidad profesional debe combinarse
                                        armónicamente con el aspecto humano para sobrellevar la compleja misión que
                                        enfrentan los médicos, especialmente cuando de cuidados intensivos se trata.
                                        <br>
                                        Por otro lado expresó que todo lo que hoy es como profesional, tiene origen en
                                        lo aprendido en la Universidad María Auxiliadora.
                                        <br>
                                        “Me ayudó a cumplir mi sueño, mis compañeros y yo confiamos, si bien humo
                                        algunos momentos de dudas, perseveramos y logramos nuestros objetivos”.
                                        <br>
                                        Mencionó además que la mayor parte de los egresados de la promoción 2014 son
                                        especialistas o están en proceso, algunos incluso ya ejercen como jefes de
                                        servicios médicos.
                                        <br>
                                        Cada logro profesional de nuestros egresados nos enorgullece y nos impulsa a
                                        seguir formando profesionales, capaces, innovadores y comprometidos con la ética
                                        y la ciencia.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="row">
            <div class="col-md-4" data-aos="fade-up">
                <img src="{{ asset('images/dr-juan.png') }}" alt="" class="w-100">
                <p class="bg-light p-4 h5">Doctor Juan Aníbal Recalde Rivas</p>
            </div>
            <div class="col-md-8 pl-md-5">
                <h3 class="my-4" data-aos="fade-up">Egresado de la primera Promoción 2014</h3>
                <p data-aos="fade-right">Doctor Juan Aníbal Recalde Rivas, egresado de la primera Promoción 2014.
                    Destacado en su labor como líder de una compleja cirugía contra el cáncer de cuello uterino en el
                    Instituto de Previsión Social de Concepción. Una intervención considerada como inédita en esa región
                    del país y con resultado satisfactorio para el paciente.
                    <br><br>
                    El valor de este logro fortalece la imagen de la UMAX como formadora de profesionales capaces con
                    vocación de servicio, respeto a la ética y a la ciencia de la salud.</p>
            </div>
        </div>
        <div class="row pt-5 mt-5 flex-column-reverse flex-md-row">
            <div class="col-md-8 pr-5">
                <h3 class="my-4" data-aos="fade-up">Historias que inspiran</h3>
                <p data-aos="fade-left">Después de varias décadas la humanidad nuevamente se ve amenazada por una
                    pandemia. <br><br> El COVID-19 ha transformado todos los aspectos de la vida actual y representa un
                    enorme desafío para los profesionales de la salud. <br><br> A nivel local, los esfuerzos están
                    concentrados en la prevención y el tratamiento en una batalla sin descanso. <br><br>
                    En la primera línea del deber médico tenemos el agrado de destacar a la Doctora Lilian Fabiola Paiva
                    Moreno, egresada de la primera promoción de médicos formados por la Universidad María Auxiliadora en
                    el año 2014. <br><br>
                    La profesional ejerce el segundo periodo de Residencia en la Unidad de Cuidados Intensivos del
                    Hospital Nacional de Itauguá. <br><br> Esto significa verse cara a cara con la fase más compleja de
                    la enfermedad, donde brinda su capacidad y compromiso profesional al servicio de los pacientes. <br><br>
                    En palabras de la Doctora Paiva, la capacidad profesional debe combinarse armónicamente con el
                    aspecto humano para sobrellevar la compleja misión que enfrentan los médicos, especialmente cuando
                    de cuidados intensivos se trata. <br><br>
                    Por otro lado expresó que todo lo que hoy es como profesional, tiene origen en lo aprendido en la
                    Universidad María Auxiliadora. <br><br> “Me ayudó a cumplir mi sueño, mis compañeros y yo confiamos,
                    si bien humo algunos momentos de dudas, perseveramos y logramos nuestros objetivos”. <br><br>
                    Mencionó además que la mayor parte de los egresados de la promoción 2014 son especialistas o están
                    en proceso, algunos incluso ya ejercen como jefes de servicios médicos. <br><br>
                    Cada logro profesional de nuestros egresados nos enorgullece y nos impulsa a seguir formando
                    profesionales, capaces, innovadores y comprometidos con la ética y la ciencia. </p>
            </div>
            <div class="col-md-4" data-aos="fade-up">
                <img src="{{ asset('images/dra-lilian-paiva.png') }}" alt="" class="w-100">
                <p class="bg-light p-4 h5">Doctora Lilian Fabiola Paiva Moreno</p>
            </div>
        </div>--}}
        <h4 class="mt-3" data-aos="fade-up">Actualizá tus datos aquí</h4>
        <div class="d-flex justify-content-md-start mt-5 aos-init aos-animate" data-aos="fade-up">
            <a target="_blank" href="https://forms.gle/ERrkmYWAHyhxv6jo7" class="btn btn-primary mb-5 mb-md-0">Ingrese
                aquí</a>
        </div>
        <hr>
        <div class="row justify-content-md-center">
            <div class="col-md-6 aos-init aos-animate" data-aos="fade-right">
                <h4 class="mb-4">Dejanos tu consulta</h4>
                <form action="http://umax-site.auth/mail/send/contact" method="get">
                    <div class="form-group mb-4 aos-init aos-animate" data-aos="fade-up">
                        <input type="text" class="form-control" id="inputName" name="name"
                               placeholder="Nombre y Apellido" required="">
                    </div>
                    <div class="form-group aos-init aos-animate" data-aos="fade-up">
                        <input type="email" class="form-control" id="inputEmail" name="email"
                               placeholder="Corrreo electrónico" required="">
                    </div>
                    <div class="form-group aos-init" data-aos="fade-up">
                        <input type="text" class="form-control" id="inputCelular" name="cellphone" placeholder="Celular"
                               required="">
                    </div>
                    <div class="form-group mb-4 aos-init" data-aos="fade-up">
                        <textarea class="form-control" id="mensaje" name="message" placeholder="Mensaje"
                                  rows="3"></textarea>
                    </div>
                    <div class="d-flex justify-content-md-end mt-5 aos-init" data-aos="fade-up">
                        <button class="btn btn-primary mb-5 mb-md-0" type="submit">Enviar mensaje</button>
                    </div>
                </form>
            </div>
            <div class="col-md-6"></div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script>
@endsection
