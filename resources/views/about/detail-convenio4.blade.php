@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Convenio entre la UMAX y el Instituto
                Nacional de Educación Superior “Dr. Peña”</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>

    <section class="container">
        <div class="row">
            <div class="col-12">
                <nav class="mb-4" aria-label="breadcrumb">
                    <ol class="breadcrumb bg-light p-4">
                        <li class="breadcrumb-item"><a href="{{ route('convenios') }}">Convenios</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Convenio entre la UMAX y el Instituto
                            Nacional de Educación Superior “Dr. Peña”
                        </li>
                    </ol>
                </nav>
                {{--<img src="{{ asset('images/convenio-unne.png') }}" alt="" class="img-fluid mb-5" data-aos="fade-up">--}}
                <h3 class="mt-5 mb-4" data-aos="fade-up">Convenio entre la UMAX y el Instituto Nacional de Educación
                    Superior “Dr. Peña”</h3>
                <p data-aos="fade-up">
                    Por medio de la alianza estratégica con el Instituto Nacional de Educación Superior "Dr. Raúl Peña",
                    avanzamos hacia el fortalecimiento institucional incorporando más opciones para la formación y
                    perfeccionamiento de nuestros estudiantes.
                    <br>
                    <br>
                    El convenio establece las siguientes cláusulas:
                </p>
                <ul data-aos="fade-up">
                    <li>Implementar la realización de práctica profesional y pasantías de los estudiantes matriculados
                        en ofertas académicas de posgrado de la UMAX en el INAES conforme a la capacidad disponible
                    </li>
                    <li>Traslados a la práctica los conocimientos teóricos adquiridos a lo largo del desarrollo
                        curricular en las clases.
                    </li>
                    <li>Adquirir competencias mediante experiencias laborales reales.</li>
                    <li>Desarrollar destrezas en el proceso de adaptación del estudiante para la gestión profesional
                    </li>
                    <li>Incorporar habilidades requeridas para el ejercicio de la profesión</li>
                    <li>Añadir habilidades de relacionamiento e interacción en equipos de trabajo en diversos entornos
                    </li>
                    <li>Fortalecer el vínculo Institucional de la Universidad con la comunidad productiva.</li>
                </ul>
                <div class="d-flex justify-content-md-start mt-5 aos-init aos-animate" data-aos="fade-up">
                    <a target="_blank"
                       href="https://www.inaes.edu.py/application/files/2016/1834/0111/Convenio_especfico_Umax.pdf"
                       class="btn btn-primary mb-5 mb-md-0" type="submit">Texto Completo</a>
                </div>
            </div>

        </div>

    </section>
@endsection
