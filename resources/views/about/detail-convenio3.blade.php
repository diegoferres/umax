@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Convenio con la Universidad Nacional del Nordeste (UNNE)</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>

    <section class="container">
        <div class="row">
            <div class="col-12">
                <nav class="mb-4" aria-label="breadcrumb">
                    <ol class="breadcrumb bg-light p-4">
                        <li class="breadcrumb-item"><a href="{{ route('convenios') }}">Convenios</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Convenio con la Universidad Nacional del
                            Nordeste (UNNE)
                        </li>
                    </ol>
                </nav>
                <img src="{{ asset('images/convenio-unne.png') }}" alt="" class="img-fluid mb-5" data-aos="fade-up">
                <h3 class="mt-5 mb-4" data-aos="fade-up">Convenio con Universidad Nacional del Nordeste (UNNE)</h3>
                <p data-aos="fade-up">
                    La Universidad María Auxiliadora rubricó el Convenio de Cooperación con la Universidad Nacional del
                    Nordeste (Argentina), dando así un paso de importancia en la integración regional entre
                    instituciones de Educación Superior.
                    <br>
                    <br>
                    Entre los principales aspectos del convenio se destacan los siguientes:
                </p>
                <ul data-aos="fade-up">
                    <li>Movilidad Estudiantil y Docente</li>
                    <li>Capacitaciones Docentes con Certificación Conjunta</li>
                    <li>Realización de Seminarios y Conferencias</li>
                    <li>Realización de Actividades de Posgrado e Investigación.</li>
                </ul>
                <p data-aos="fade-up">
                    Ambas instituciones asumen el desafío con el propósito de direccionar de forma integradora los
                    objetivos de interés mutuo, con la finalidad de promover y mejorar estrategias viables e integrales
                    que permitan responder a las constantes exigencias en pro de la
                </p>
            </div>

        </div>

    </section>
@endsection
