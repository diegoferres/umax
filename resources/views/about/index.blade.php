@extends('layout.app')

@section('head')
    <style>
        #more {
            display: none;
        }
    </style>
@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Sobre nosotros</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>

    <section class="university">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 pr-lg-0" data-aos="fade-right">
                    <img src="{{ asset('images/fachada2.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-lg-6 px-0" data-aos="fade-left" style="background-color: #F6F6FA;">
                    <div class="text">
                        <h3 class="mb-4">La Universidad</h3>
                        <p>
                            La Universidad María Auxiliadora es una institución educativa privada, de estudios
                            superiores, de investigación, de formación profesional y de servicios con 13 años de vida
                            institucional, creada por Ley N.º 3501/08 y que se rige actualmente por la Ley N° 1264/98
                            “General de Educación”, por la Ley N.º 4995/13 “De Educación Superior”, por la Ley N.º
                            2072/03 “De creación de la Agencia inicios Nacional de Evaluación y Acreditación de la
                            Educación Superior”, y demás normas jurídicas que regulan la materia.<span
                                id="dots">...</span>

                            <br>
                            <br>
                            En setiembre de 2008 se habilita la carrera de Medicina, conforme a la Ley de Universidades.
                            En 2017 recibe la habilitación del CONES y en 2018 la acreditación de la ANEAES. Ambas
                            instancias, validan la calidad de nuestra opción académica siendo ANEAES la máxima
                            certificación de calidad a nivel nacional.
                            <br>
                            <br>

                            La carrera Licenciatura en Enfermería depende de la Facultad de Ciencias de la Salud y fue
                            creada por el Consejo Superior Universitario de la UMAX según Resolución Nº 2 de fecha 16 de
                            julio de 2008 y se imparte desde agosto de ese mismo año.
                            <span id="more">
                            <br>
                            <br>
                            Respecto a las carreras de pregrado actualmente se imparten las siguientes: Técnico Superior
en Enfermería, Técnico Superior en Laboratorio Clínico, Técnico Superior en Radiología,
Técnico Superior en Farmacia y Técnico Superior en Masaje Terapéutico.
                            <br>
                            <br>
                            Dentro de las proyecciones de ampliación de la oferta educativa, se prevé la creación de la
                            Facultad de Ciencias Empresariales, con la apertura de las carreras de Licenciatura en
                            Administración de Empresas (en las modalidades presencial y virtual), Ingeniería Comercial y
                            Licenciatura en Marketing (en la modalidad virtual), cuyos Proyectos Académicos ya fueron
                            presentados ante el CONES.
                            <br>
                            <br>
                            En lo referente a las carreras de Posgrado, también fueron presentados al CONES los
                            Proyectos Académicos del área de Humanidades, específicamente de Educación, como el de la
                            Especialización en Didáctica Superior Universitaria, el de la Maestría en Educación
                            Superior, el de la Maestría en Educación Médica, entre otros proyectos de formación
                            continua.
                                </span>
                            <a id="myBtn" href="javascript:myFunction()" class="">Ver más (+)</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="text-nosotros">

                {{--<h4 data-aos="fade-up">Habilitación de Carreras</h4>--}}
                <p data-aos="fade-up" data-aos-duration="800">
                    Con 13 años de recorrido en el ámbito de la Educación Superior, la Universidad María
                    Auxiliadora se erige como una institución de referencia en la enseñanza de carreras de salud.
                    <br>
                    <br>
                    La Universidad María Auxiliadora es una institución educativa privada, de estudios superiores,
                    de investigación, de formación profesional y de servicios, creada por Ley N.º 3501/08 y que se
                    rige actualmente por la Ley N° 1264/98 “General de Educación”, por la Ley N.º 4995/13 “De
                    Educación Superior”, por la Ley N.º 2072/03 “De creación de la Agencia inicios Nacional de
                    Evaluación y Acreditación de la Educación Superior”, y demás normas jurídicas que regulan la
                    materia.
                    {{--<br>
                    <br>
                    Hoy día se emprende el plan de expansión hacia nuevas opciones académicas, manteniendo los
                    altos estándares de excelencia y compromiso con la sociedad a través de la formación de
                    profesionales capaces, íntegros y responsables.--}}
                </p>

                <h4 data-aos="fade-up">Habilitación de Carreras</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    En fecha 16 de julio de 2.008 la Universidad María Auxiliadora ha habilitado la carrera
                    Ingeniería en Ciencias de la Informática de conformidad a lo que establece el Artículo
                    10 de su Estatuto, el Artículo 79 de la Constitución Nacional y el Artículo 5 de la Ley Nº
                    2.529/06 “Que modifica parcialmente la Ley Nº 136/93 “DE UNIVERSIDADES”.
                    <br><br>
                    En fecha 05 de setiembre de 2.008 la Universidad María Auxiliadora ha habilitado la
                    carrera MEDICINA de conformidad a lo que establece el Artículo 10 de su Estatuto, el
                    Artículo 79 de la Constitución Nacional y el Artículo 5 de la Ley Nº 2.529/06 “Que
                    modifica parcialmente la Ley Nº 136/93 “DE UNIVERSIDADES”. En 2017 recibe la
                    habilitación del CONES y en 2018 la acreditación de la ANEAES. En fecha 17 de marzo
                    de 2017 El Consejo de Educación Superior (CONES) por Resolución 133/17 ha
                    habilitado la carrera MEDICINA de la Sede Central de Loma Pyta – Asunción de
                    conformidad a la Resolución 166/15 e insertada en el Registro Nacional de Ofertas
                    Académicas.
                    <br><br>
                    La carrera Licenciatura en Enfermería depende de la Facultad de Ciencias de la Salud y
                    fue creada por el Consejo Superior Universitario de la UMAX según Resolución Nº 2 de
                    fecha 16 de julio de 2008.
                </p>

                <h4 class="mt-5" data-aos="fade-up">Reconocimiento de Títulos</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    Los títulos de Licenciatura en Enfermería a ser emitidos por la Universidad María Auxiliadora
                    cuentan con el reconocimiento oficial del Ministerio de Educación y Ciencias según Resolución
                    Nº 908/08 de fecha 07 de Agosto de 2.008 expedida por la Dirección General de Educación
                    Superior.
                    <br><br>
                    Los títulos de Medicina a ser emitidos por la Universidad María Auxiliadora cuentan con el
                    reconocimiento oficial del Ministerio de Educación y Ciencias según Nota DGES Nº 786 de
                    fecha 17 de Marzo de 2.009 expedida por la Dirección General de Educación.
                </p>
                <div class="row my-5">
                    <div class="col-md-6 p-5 bg-light">
                        <h4 class="mt-5" data-aos="fade-up">Misión</h4>
                        <p data-aos="fade-up" data-aos-duration="800">Formar profesionales íntegros e idóneos, con
                            valores humanos y cristianos; pensamiento científico, crítico y reflexivo, principios éticos
                            y sólida preparación en el campo de la investigación y extensión social; comprometidos con
                            el mejoramiento de la calidad de vida de su entorno local, regional y global.</p>
                    </div>
                    <div class="col-md-6 p-5 bg-light">
                        <h4 class="mt-5" data-aos="fade-up">Visión</h4>
                        <p data-aos="fade-up" data-aos-duration="800">Ser una universidad reconocida nacional e
                            internacionalmente por la excelencia académica y perfil humanista de sus egresados, con
                            acreditación de calidad certificada y comprometida con la transformación de la sociedad.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="valores">
            <div class="container">
                <div class="row text-center justify-content-center">
                    <div class="col-md-10">
                        <h3 data-aos="fade-up">Principios y Valores</h3>
                        <p class="mb-5" data-aos="fade-up">Es por ello que la Institución se ha propuesto enfatizar su
                            actividad
                            <br>en dos áreas del saber como la SALUD y la EDUCACIÓN.</p>
                        <h6 class="item" data-aos="flip-down">Integridad</h6>
                        <h6 class="item" data-aos="flip-down">Compromiso con la Calidad</h6>
                        <h6 class="item" data-aos="flip-down">Cordialidad</h6>
                        <h6 class="item" data-aos="flip-down" data-aos-duration="600">Espiritualidad Cristiana</h6>
                        <h6 class="item" data-aos="flip-down" data-aos-duration="600">Responsabilidad Social</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="mensaje">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="pr-5">
                            <h5 data-aos="fade-up">Mensaje del Rector</h5>
                            <h3 data-aos="fade-up" data-aos-duration="200">Estimados Alumnos:</h3>
                            <p data-aos="fade-up" data-aos-duration="800">El espíritu de nuestra casa de estudios nos
                                motiva y nos empuja al crecimiento integral de la persona y hacemos de este nuestra
                                razón de ser, formando recursos humanos con alta idoneidad.
                                <br><br>
                                Los desafíos actuales del mercado laboral y de la sociedad demandan un alto
                                entrenamiento técnico con fino desarrollo interpersonal y combinando esto con una firme
                                disciplina, generan una fórmula exacta y precisa para lograr el éxito.
                                <br><br>
                                Sabemos y asumimos que la salud es un área sensible y muy delicada de nuestro país.
                                Partiendo de esto desarrollamos proyectos educativos minuciosos, serios, estrictos y con
                                proyección encuadradas en las exigencias actuales.
                                <br><br>
                                Nuestra Institución educativa desde 1993 está formando excelentes profesionales en salud
                                basados en la exigencia académica y el énfasis en los campos de práctica.
                                <br><br>
                                Formar a los “Mejores Profesionales en Salud” del Paraguay es posible gracias a que
                                tenemos un objetivo bien definido, un grupo humano calificado, trayectoria, tradición e
                                infraestructura a disposición de las personas que confían en nosotros.
                                <br><br>
                                Anímate a formar parte de la única Universidad enfocada exclusivamente en formar a los
                                <b>“Mejores Egresados en Salud”.</b></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="avatar">
                            <img src="{{ asset('images/rector.png') }}" alt="" class="img-fluid" data-aos="fade-left"
                                 data-aos-duration="600">
                            <p class="pr-5 mt-4" data-aos="fade-up" data-aos-duration="800">Prof. Dr. Hernando Javier
                                Quiñonez Sarabia</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="autoridades">
            <div class="container">
                <h2 class="mb-5" data-aos="fade-up">Autoridades</h2>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/rector-2.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Prof. Dr. Hernando Javier Quiñonez Sarabia</h4>
                                <h6>Rector</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-elvira.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>M. Sc. Elvira Peralta Ortigoza</h4>
                                <h6>Presidente del C.S.U.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-gloria.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dra. Gloria Concepción Martínez Pasmor</h4>
                                <h6>Vicerrectora Académica</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-steven.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>M. Sc. Rubén Steven Falcón Gamarra</h4>
                                <h6>Secretario General</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-viviana.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Lic. Viviana Carina Pompa Arias</h4>
                                <h6>Directora General Administrativa</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-gustavo.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dr. Gustavo Guillermo Calvo</h4>
                                <h6>Decano de la Facultad de Medicina</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-lilian-2.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Prof. M.Sc. María Lilian Portelli</h4>
                                <h6>Decana de la Facultad de Ciencias de la Salud</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dra-fanny.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dra. Fanny Graciela Ayala Ratti</h4>
                                <h6>Directora de Calidad Académica</h6>
                            </div>
                        </div>
                    </div>
                    {{-- NUEVOS --}}
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dra-ramona-bolivar.jpg') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dra. Ramona Bolívar</h4>
                                <h6>Directora de Posgrado</h6>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/ing-eduardo-almeida.jpg') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Ing. Eduardo Almeida</h4>
                                <h6>Director de Aulas Virtuales</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-clara-pardo.jpg') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Lic. Clara Benítez de Pardo</h4>
                                <h6>Directora de Tecnología de la Información</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/ing-cinthia.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Ing. Cinthia Maiz</h4>
                                <h6>Directora de Comunicación Institucional y Marketing</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-verena-bardella.jpg') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Lic. Verena Bardella</h4>
                                <h6>Directora de Bienestar Estudiantil</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-mabel-canete.jpg') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Lic. Mabel Cañete</h4>
                                <h6>Directora Pedagógica</h6>
                            </div>
                        </div>
                    </div>
                    {{-- NUEVOS --}}
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-ricardo.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dr. Ricardo Elvis Garay</h4>
                                <h6>Director de Extensión y Vinculación con el Medio</h6>
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dr-arsenio.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>M.Sc Arsenio Ramón Amaral Rodríguez.</h4>
                                <h6>Director de Investigación.</h6>
                            </div>
                        </div>
                    </div>--}}
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/dra-isabel.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Prof. Dra. Isabel Rodríguez</h4>
                                <h6>Directora de Investigación e Innovación</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card" data-aos="flip-left">
                            <div class="avatar">
                                <img src="{{ asset('images/lic-nayila.png') }}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Dra. Nayila García</h4>
                                <h6>Directora de Pastoral</h6>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">

            <div class="text-nosotros">
                <h4 class="mb-4" data-aos="fade-up">Calidad Académica</h4>
                <h5 data-aos="fade-up">Mecanismo de Aseguramiento de la Calidad </h5>
                <p data-aos="fade-up" data-aos-duration="800">
                    La Dirección de Calidad Académica, es la instancia institucional responsable de la planificación,
                    coordinación, ejecución y evaluación a nivel institucional y de grado, de los procesos de evaluación
                    interna y externa.
                    <br>
                    <br>
                    Para el cumplimiento de los objetivos del aseguramiento de la calidad educativa, se trabaja
                    conjuntamente con los Comités Permanente de Autoevaluación de la carrera, la Dirección Pedagógica,
                    los Comités de Revisión Curricular y los Comité de Investigación y Bioética de la carrera.
                </p>

                <h5 data-aos="fade-up">Los objetivos del sistema de aseguramiento de la calidad educativa son:</h5>
                <p data-aos="fade-up" data-aos-duration="800">
                <ul>
                    <li>Mejorar la calidad de los servicios educativos que presta la carrera de medicina, a la luz de
                        las políticas institucionales y de los criterios de calidad establecidos por la ANEAES para el
                        modelo nacional y ARCUSUR para el modelo regional.
                    </li>
                    <li>Propiciar un mayor grado de compromiso de la comunidad académica de la UMAX con el mejoramiento
                        de la calidad de los servicios educativos que presta a la sociedad en general, a su entorno
                        próximo y zona de influencia.
                    </li>
                    <li>Determinar los aspectos críticos de la implementación de las normativas, Planes y Proyectos,
                        para proponer las mejoras correspondientes.
                    </li>
                    <li>Propiciar la cultura de la evaluación participativa y la cultura de la calidad educativa al
                        interior de la comunidad académica.
                    </li>
                    <li>Contar con un instrumento de referencia para la revisión sistemática (retroalimentación) de los
                        procesos de aplicación de las normativas, Planes y Proyectos.
                    </li>
                    <li>Contar con datos fidedignos y sistemáticamente actualizados sobre el nivel de satisfacción de
                        los distintos estamentos de la comunidad educativa sobre los servicios que presta la
                        institución, sus carreras y programas
                    </li>
                    <li>Mantener una base de datos actualizados sobre Indicadores de eficiencia interna.</li>
                    <li>Contar con datos precisos y actualizados sobre el nivel de cumplimiento de la coherencia interna
                        (cumplimiento de planes, programas y proyectos) y los criterios de calidad de organismos
                        externos (cumplimiento de las dimensiones, componentes, criterios e indicadores) citados en el
                        Manual de Evaluación Institucional de Grado y Postgrado UMAX.
                    </li>
                </ul>

                La Dirección de Calidad Académica, desde su creación en el año 2016, ha acompañado a los procesos de
                gestión académica y los procesos de autoevaluación a lo que la carrera se fue enfrentando, prueba del
                trabajo efectivo y colaborativo es la Acreditación por el Modelo Nacional de las carreras de Medicina.
                <br>
                <br>
                Los informes de la implementación de los planes de mejoras de la misma y el proceso de autoevaluación
                son realizados en forma sistemáticamente para el cumplimiento de la coherencia interna entre lo que está
                escrito formalmente y lo que se aplica en la práctica, presentando recomendaciones de mejoras
                correspondientes a todas las áreas involucradas.
                </p>
                <h5 data-aos="fade-up">Directora de Calidad Académica: <span class="text-muted">Dra. Fanny Graciela Ayala Ratti</span>
                </h5>
                <p data-aos="fade-up" data-aos-duration="800">
                <ul>
                    <li>Educ. Universitaria Facultad de Odontología - UNA Asunción – Paraguay</li>
                    <li>Maestría en Didáctica Universitaria - Universidad del Pacifico Privada Asunción - Paraguay</li>
                    <li>Especialista en Cirugía y Traumatología Buco-maxilofacial Universidad Autónoma del Paraguay
                        Asunción - Paraguay
                    </li>
                    <li>Doctorado en Educación Universidad Iberoamericana Asunción - Paraguay</li>
                    <li>Trayectoria en Educación Universitaria: 31 años.</li>
                    <li>Antigüedad en la UMAX: desde 2 de mayo del 2016</li>
                </ul>
                </p>

            </div>
        </div>

        <div class="insfra">
            <div class="container">
                <h2 class="mb-4" data-aos="fade-up">Infraestructura</h2>
            </div>
            <!-- Glider-->
            <div class="glider-contain my-5">
                <div class="glider">
                    <div class="ml-0" data-aos="fade-left">
                        <img src="{{ asset('images/s1.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s2.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s3.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s4.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/s5.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>{{--
                    <div class="ml-1">
                        <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1">
                        <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                    </div> --}}
                </div>
                <div class="d-flex justify-content-center">
                    <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-angle-left"></i>
                    </button>
                    <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
            <!-- End Glider-->
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script>
    <script>
        function myFunction() {
            var dots = document.getElementById("dots");
            var moreText = document.getElementById("more");
            var btnText = document.getElementById("myBtn");

            if (dots.style.display === "none") {
                dots.style.display = "inline";
                btnText.innerHTML = "Ver mas (+)";
                moreText.style.display = "none";
            } else {
                dots.style.display = "none";
                btnText.innerHTML = "Ver menos (-)";
                moreText.style.display = "inline";
            }
        }
    </script>
@endsection


