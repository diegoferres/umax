@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Biblioteca</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Biblioteca “M.Sc. Alberta Sarabia”</h3>
                <h4 data-aos="fade-up">Un moderno y confortable espacio del saber </h4>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                    La biblioteca M.Sc. Alberta Sarabia de la Universidad María Auxiliadora, cuenta con todos los
                    materiales necesarios para acompañar el proceso formativo de los estudiantes y futuros profesionales
                    de la salud.
                    <br>
                    <br>
                    Su amplia colección comprende más de 1.100 volúmenes, entre materiales de consulta, tesis,
                    monografías, revistas científicas y otras publicaciones de interés en salud, ciencia, investigación
                    y tecnología.

                </p>
                <h4 class="mt-5" data-aos="fade-up">Los materiales abarcan las siguientes áreas de la salud</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Histología <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Obstetricia <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Pediatría <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Ginecología <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Nutrición <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Microbiología <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Fisiología <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Anatomía <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Cirugía. <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Administración Hospitalaria <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Farmacología, <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Patología y Semiología, <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Medicina Interna <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Biología <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Bioquímica <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Estadística <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Psicología <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Metodología de la Investigación Científica <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Redacción y Literatura. <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Matemática <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                    Obras de referencia como: diccionarios, enciclopedias, anuarios. <br><br>
                    Gran parte de los volúmenes disponibles vienen acompañados de materiales audiovisuales que
                    enriquecen la experiencia de lectura, complementando las letras con las imágenes y de esa manera
                    obtener un aprendizaje acabado en una materia determinada.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Infraestructura</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    Una sala de 200 metros cuadrados con amplios ventanales permite la entrada de la luz solar por las
                    mañanas, otorgando un ambiente de paz y tranquilidad, ideal para la concentración.
                    <br>
                    <br>
                    Los espacios de lectura individual están debidamente separados, para una lectura sin interrupciones
                    y con el distanciamiento requerido, atendiendo los protocolos vigentes en salud.
                    <br>
                    <br>
                    Igualmente están disponibles diez computadoras con acceso a internet, para quienes deseen ampliar su
                    base teórica con la información disponible en la web.
                    <br>
                    <br>
                    Siguiendo con el plan de mejoras dentro de la biblioteca, se está implementando el sistema de
                    gestión online, donde el estudiante podrá consultar por un título determinado y su disponibilidad,
                    como así también gestionar las reservas, entre otras funcionalidades.

                </p>
                <div class="mt-5 mb-5 col-md-3">
                    <a href="https://biblioteca.umax.edu.py" class="btn btn-primary btn-block aos-init aos-animate"
                       data-aos="fade-up">Biblioteca Virtual</a>
                </div>
                <p data-aos="fade-up" data-aos-duration="800">
                    Nuestra biblioteca forma parte de la Red de Bibliotecas Virtuales en Salud del Paraguay, como centro
                    colaborador.
                    <br>
                    <br>
                    Por medio de esta alianza estratégica, se pueden acceder a herramientas de gran utilidad como la
                    Base de Datos Nacional del Paraguay (BDNPAR), al Portal de Revistas Científicas del Paraguay,
                    Legislación en Salud (leyes, Normativas y Reglamentaciones de Salud en Paraguay), también
                    publicaciones del Ministerio de Salud Pública y Bienestar Social (colección histórica y documental
                    del Ministerio de Salud).
                    <br>
                    <br>
                    En dicha plataforma también están disponibles las siguientes fuentes de consulta:
                    <br>
                    <br>

                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon><b>Base de datos LILACS</b> https://lilacs.bvsalud.org/es/
                    <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon><b>BVS Regional</b> https://bvsalud.org/es/
                    <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon><b>Medline</b> https://medlineplus.gov/spanish/
                    <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon><b>Scielo.</b> https://scielo.org/es/
                    <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon><b>Paho Iris</b> https://iris.paho.org/
                    <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon><b>Who Iris.</b> https://apps.who.int/iris/
                    <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon><b>DECS Terminología en Salud.</b> https://decs.bvsalud.org/E/homepagee.htm
                    <br><br>
                </p>
                <h4 class="mt-5" data-aos="fade-up">Staff</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    Lic. Fernando Varela- Encargado de Biblioteca
                    <br>
                    Contacto: fernando.varela@umax.edu.py
                </p>
            </div>
        </div>
        <div class="fotos pt-2">
            <div class="container">
                <h3 class="mt-5 py-5" data-aos="fade-up">Fotos</h3>
            </div>
            <!-- Glider-->
            <div class="glider-contain mb-5">
                <div class="glider">
                    <div class="ml-0" data-aos="fade-left">
                        <img src="{{ asset('images/biblioteca/biblioteca_1.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/biblioteca/biblioteca_2.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/biblioteca/biblioteca_3.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/biblioteca/biblioteca_4.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-angle-left"></i>
                    </button>
                    <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
            <!-- End Glider-->
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="ModalExtension" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/wXR6NTgyeNk" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalMedicina" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/M711BrIu5xQ" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalProyecto" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/tNGs9vgjTtc" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>

@section('scripts')
    <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script>
@endsection

@endsection
