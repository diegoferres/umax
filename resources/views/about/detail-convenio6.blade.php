@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Convenio entre la UMAX y el Hospital Reina Sofía de la Cruz Roja Paraguaya</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>

    <section class="container">
        <div class="row">
            <div class="col-12">
                <nav class="mb-4" aria-label="breadcrumb">
                    <ol class="breadcrumb bg-light p-4">
                        <li class="breadcrumb-item"><a href="{{ route('convenios') }}">Convenios</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Convenio entre la UMAX y el Hospital
                            Reina Sofía de la Cruz Roja Paraguaya
                        </li>
                    </ol>
                </nav>
                {{--<img src="{{ asset('images/convenio-unne.png') }}" alt="" class="img-fluid mb-5" data-aos="fade-up">--}}
                <h3 class="mt-5 mb-4" data-aos="fade-up">Convenio entre la UMAX y el Hospital Reina Sofía de la Cruz
                    Roja Paraguaya</h3>
                <p data-aos="fade-up">
                    La Universidad María Auxiliadora y el Hospital Materno Infantil Reina Sofía de la Cruz Roja
                    Paraguaya, firmaron un convenio de cooperación interinstitucional de gran relevancia para la
                    formación de los futuros médicos de la UMAX.
                    <br>
                    <br>
                    Por medio de este convenio los estudiantes de Medicina podrán realizar sus Prácticas y pasantías profesionales en las siguientes especialidades:
                </p>
                <ul data-aos="fade-up">
                    <li>Pediatría</li>
                    <li>Ginecología</li>
                    <li>Obstetricia</li>
                </ul>
                <p data-aos="fade-up">
                    La Cruz Roja pondrá a disposición profesionales de amplia trayectoria y reconocimiento en el campo
                    de la Medicina y la docencia, para guiar a los estudiantes en dicho proceso.
                </p>
                <p data-aos="fade-up">
                    Nos enorgullece trabajar conjuntamente con una de las instituciones más importantes en materia de
                    salud, con la cual nos unen intereses comunes, principios y especialmente la vocación de servir a
                    quienes lo necesitan, desde nuestros ámbitos de gestión.
                </p>
            </div>
        </div>

    </section>
@endsection
