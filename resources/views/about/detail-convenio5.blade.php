@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Convenio entre la UMAX y la Universidad Regional Integrada del Alto Uruguay y
                Misiones de Brasil (URI)</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>

    <section class="container">
        <div class="row">
            <div class="col-12">
                <nav class="mb-4" aria-label="breadcrumb">
                    <ol class="breadcrumb bg-light p-4">
                        <li class="breadcrumb-item"><a href="{{ route('convenios') }}">Convenios</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Convenio entre la UMAX y la Universidad
                            Regional Integrada del Alto Uruguay y Misiones de Brasil (URI)
                        </li>
                    </ol>
                </nav>
                {{--<img src="{{ asset('images/convenio-unne.png') }}" alt="" class="img-fluid mb-5" data-aos="fade-up">--}}
                <h3 class="mt-5 mb-4" data-aos="fade-up">Convenio entre la UMAX y la Universidad Regional Integrada del
                    Alto Uruguay y Misiones de Brasil (URI)</h3>
                <p data-aos="fade-up">
                    La Universidad María Auxiliadora y la Universidad Regional Integrada del Alto Uruguay y Misiones de
                    Brasil, suscribieron un acuerdo de cooperación académica, científica y cultural, con una vigencia de
                    cinco años de acuerdo al documento oficial.
                    <br>
                    <br>
                    Entre los aspectos más resaltantes del acuerdo figuran los siguientes:
                </p>
                <ul data-aos="fade-up">
                    <li>El intercambio de profesores y estudiantes con miras a su titulación académica y profesional.</li>
                    <li>La promoción, ejecución y difusión de estudios, investigaciones, como así también la participación en programas de carácter internacional.</li>
                    <li>Ambas instituciones se comprometen a organizar seminarios, reuniones y paneles para el enriquecimiento mutuo a nivel académico.</li>
                    <li>Así también se trabajará conjuntamente para facilitar el intercambio de publicaciones como revistas y avances científicos.</li>
                </ul>
            </div>
        </div>

    </section>
@endsection
