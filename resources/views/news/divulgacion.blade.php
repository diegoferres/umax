@extends('layout.app')

@section('head')

@endsection

@section('content')
    <div class="header-top mb-0">
        <div class="container">
            <h2 data-aos="fade-up">Divulgación científica</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="news mt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-5">
                    @foreach ($news as $key => $new)
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%d", strtotime($new->datetime)) }}</h3>
                                <p class="text-uppercase">@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%b", strtotime($new->datetime)) }}</p>
                            </div>
                            <h3 class="card-text mt-5">{{ $new->title }}</h3>
                            <h5 class="card-text my-1">{{ $new->autor }}</h5>
                            <p>{{ substr(html_entity_decode(strip_tags(htmlspecialchars_decode($new->content))),0, 300) }}</p>
                            <a href="{{ route('noticias.detail', $new->id) }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                    @endforeach
                    {{--<div class="card" data-aos="fade-up">
                        <div class="card-body">
                            --}}{{--<div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>--}}{{--
                            <h3 class="card-text mt-5">Investigador Asociado de la UMAX publica artículo en prestigiosa
                                revista científica de la región</h3>
                            <h5 class="card-text my-1">Dr. Nicolás Ayala Servín. Responsable del Núcleo de Trasplantes y
                                Donación de Órganos.</h5>
                            <p>El Dr. José Nicolás Ayala Servín, Investigador Asociado de la Carrera de Medicina de la
                                Universidad María Auxiliadora (UMAX) en conjunto con otros investigadores, llevaron a
                                cabo un estudio a través del Núcleo de Investigación de Trasplante de la
                                institución.</p>
                            <a href="https://www.umax.edu.py/noticias/11/detail" class="">Continuar leyendo</a>
                        </div>
                    </div>
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            --}}{{--<div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>--}}{{--
                            <h3 class="card-text mt-5">Distinguen a Investigador asociado de la UMAX</h3>
                            <h5 class="card-text my-1">Dr. Nicolás Ayala Servín. Responsable del Núcleo de Tecnologías
                                de la Información y Comunicación.</h5>
                            <p>El Dr. Nicolás Ayala Servín obtuvo el primer lugar en la edición número 45º de los
                                Premios “Jóvenes Sobresalientes del Paraguay”, evento organizado por la Cámara Junior
                                Internacional y con apoyo del Congreso Nacional.</p>
                            <a href="https://www.umax.edu.py/noticias/6/detail" class="">Continuar leyendo</a>
                        </div>
                    </div>
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            --}}{{--<div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>--}}{{--
                            <h3 class="card-text mt-5">Distinguen a Investigador asociado de la UMAX</h3>
                            <h5 class="card-text my-1">Dr. Nicolás Ayala Servín. Responsable del Núcleo de Tecnologías
                                de la Información y Comunicación.</h5>
                            <p>El Dr. Nicolás Ayala Servín obtuvo el primer lugar en la edición número 45º de los
                                Premios “Jóvenes Sobresalientes del Paraguay”, evento organizado por la Cámara Junior
                                Internacional y con apoyo del Congreso Nacional.</p>
                            <a href="https://www.umax.edu.py/noticias/6/detail" class="">Continuar leyendo</a>
                        </div>
                    </div>
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            --}}{{--<div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>--}}{{--
                            <h3 class="card-text mt-5">Distinguen a Investigador asociado de la UMAX</h3>
                            <h5 class="card-text my-1">Dr. Nicolás Ayala Servín. Responsable del Núcleo de Tecnologías
                                de la Información y Comunicación.</h5>
                            <p>El Dr. Nicolás Ayala Servín obtuvo el primer lugar en la edición número 45º de los
                                Premios “Jóvenes Sobresalientes del Paraguay”, evento organizado por la Cámara Junior
                                Internacional y con apoyo del Congreso Nacional.</p>
                            <a href="https://www.umax.edu.py/noticias/6/detail" class="">Continuar leyendo</a>
                        </div>
                    </div>
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            --}}{{--<div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>--}}{{--
                            <h3 class="card-text mt-5">Distinguen a Investigador asociado de la UMAX</h3>
                            <h5 class="card-text my-1">Dr. Nicolás Ayala Servín. Responsable del Núcleo de Tecnologías
                                de la Información y Comunicación.</h5>
                            <p>El Dr. Nicolás Ayala Servín obtuvo el primer lugar en la edición número 45º de los
                                Premios “Jóvenes Sobresalientes del Paraguay”, evento organizado por la Cámara Junior
                                Internacional y con apoyo del Congreso Nacional.</p>
                            <a href="https://www.umax.edu.py/noticias/6/detail" class="">Continuar leyendo</a>
                        </div>
                    </div>
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            --}}{{--<div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>--}}{{--
                            <h3 class="card-text mt-5">Distinguen a Investigador asociado de la UMAX</h3>
                            <h5 class="card-text my-1">Dr. Nicolás Ayala Servín. Responsable del Núcleo de Tecnologías
                                de la Información y Comunicación.</h5>
                            <p>El Dr. Nicolás Ayala Servín obtuvo el primer lugar en la edición número 45º de los
                                Premios “Jóvenes Sobresalientes del Paraguay”, evento organizado por la Cámara Junior
                                Internacional y con apoyo del Congreso Nacional.</p>
                            <a href="https://www.umax.edu.py/noticias/6/detail" class="">Continuar leyendo</a>
                        </div>
                    </div>
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones</h3>
                            <a href="{{ route('news-detail-5') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
    </section>

@endsection
