@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top mb-0">
        <div class="container">
            <h2 data-aos="fade-up">Ante la difusión de supuestas informaciones</h2>
        </div>
        <img src="{{ asset('images/news/news-5.png') }}" alt="" class="w-100">
    </div>
    <section class="news mt-0 bg-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card" data-aos="fade-left">
                        <div class="">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>
                            <h3 class="card-text my-5">A LA OPINIÓN PÚBLICA</h3>
                        </div>
                        <div class="card-img-top">
                            <img src="{{ asset('images/news/news-5.png') }}" class="img-fluid" alt="...">
                        </div>
                    </div>
                    <p class="mt-5">Ante la difusión de supuestas informaciones referentes a estudiantes brasileños de la Universidad María Auxiliadora UMAX, mediante audios de mensajería instantánea, nos dirigimos a la comunidad educativa y al público en general con la finalidad de aclarar lo siguiente:

                        <br><br>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum assumenda at nulla qui necessitatibus repellendus odio quas fugit totam excepturi esse voluptate culpa quis dolor, omnis aspernatur repellat architecto nisi. Odit, officia accusamus quo sequi modi distinctio nesciunt suscipit at eaque, repellendus numquam, error molestiae ex dignissimos quasi! Blanditiis, dignissimos.
                    </p>

                    <div class="share mt-5">
                        <div class="d-flex">
                            <h6>Compartir</h6>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-facebook"></ion-icon></h4></a>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-linkedin"></ion-icon></h4></a>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-twitter"></ion-icon></h4></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection