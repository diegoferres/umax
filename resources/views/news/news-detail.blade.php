@extends('layout.app')

@section('head')
    <title>Noticias | UMAX</title>
    <meta name="description" content="{{ $new->title }}">
@endsection

@section('content')

    <div class="header-top mb-0">
        <div class="container">
            {{--<h2 data-aos="fade-up">{{ $new->title }}</h2>--}}
        </div>
        <img src="{{ asset($new->image_path.$new->image_name) }}" alt="" class="w-100">
    </div>
    <section class="news mt-0 bg-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card" data-aos="fade-left">
                        <div class="">
                            <div class="fecha">
                                <h3>@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%d", strtotime($new->datetime)) }}</h3>
                                <p class="text-uppercase">@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%b", strtotime($new->datetime)) }}</p>
                            </div>
                            <h3 class="card-text my-5">{{ $new->title }}</h3>
                        </div>
                        <div class="card-img-top">
                            <img src="{{ asset($new->image_path.$new->image_name) }}" class="img-fluid" alt="...">
                        </div>
                    </div>
                    <p class="mt-5">
                        {!! $new->content !!}
                    </p>
                    {{--<h6>Links</h6>
                    <a href="https://independiente.com.py/un-pulmon-verde-en-mariano-roque-alonso/">https://independiente.com.py/un-pulmon-verde-en-mariano-roque-alonso/</a>
                    <a href="https://twitter.com/noticiaspycom/status/1201663729517158400">https://twitter.com/noticiaspycom/status/1201663729517158400</a>
                    <a href="http://agendaparaguay.com/v2/2019/12/02/inauguran-remozada-plaza-en-roque-alonso/">http://agendaparaguay.com/v2/2019/12/02/inauguran-remozada-plaza-en-roque-alonso/</a>
                    <a href="https://twitter.com/ANIBALCORO/status/1201478392866070529">https://twitter.com/ANIBALCORO/status/1201478392866070529</a>
                    <a href="https://m.facebook.com/story.php?story_fbid=762967460837101&id=606563016477547">https://m.facebook.com/story.php?story_fbid=762967460837101&id=606563016477547</a>--}}

                    <div class="share mt-5">
                        <div class="d-flex">
                            <h6>Compartir</h6>
                            <a href="http://www.facebook.com/sharer/sharer.php?u={{ request()->fullUrl() }}&title={{ $new->title }}" target="_blank"><h4 class="mx-3"><ion-icon name="logo-facebook"></ion-icon></h4></a>
                            <a href="http://www.linkedin.com/shareArticle?mini=true&url={{ request()->fullUrl() }}&title={{ $new->title }}&source=https://www.umax.edu.py/" target="_blank"><h4 class="mx-3"><ion-icon name="logo-linkedin"></ion-icon></h4></a>
                            <a href="http://twitter.com/intent/tweet?original_referer={{ request()->fullUrl() }}&text={{ $new->title }}&url={{ request()->fullUrl() }}" target="_blank"><h4 class="mx-3"><ion-icon name="logo-twitter"></ion-icon></h4></a>
                        </div>
                        <div class="text-right">
                            <a href="{{ route('noticias') }}" class=" btn btn-primary mb-5 mb-md-0">Ir a noticias</a>
                        </div>
                        {{--<div class="d-flex justify-content-md-end mt-5 aos-init aos-animate" data-aos="fade-up">

                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
