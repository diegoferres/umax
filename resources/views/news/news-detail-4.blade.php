@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top mb-0">
        <div class="container">
            <h2 data-aos="fade-up">Misa del mes de Noviembre</h2>
        </div>
        <img src="{{ asset('images/news/news-4.png') }}" alt="" class="w-100">
    </div>
    <section class="news mt-0 bg-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card" data-aos="fade-left">
                        <div class="">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>
                            <h3 class="card-text my-5">Nos congregamos en familia para iniciar un nuevo mes</h3>
                        </div>
                        <div class="card-img-top">
                            <img src="{{ asset('images/news/news-4.png') }}" class="img-fluid" alt="...">
                        </div>
                    </div>
                    <p class="mt-5">Invitamos a la comunidad educativa a participar de la misa mensual correspondiente a noviembre. En la ocasión se celebrará la Devoción al Sagrado Corazón de Jesús. <br><br>
                        Como en cada inicio de mes nos congregamos en familia para dar gracias a Dios y nuestra madre María Auxiliadora. También pediremos por la salud de todo el pueblo paraguayo y de la humanidad, asediada por el COVID-19. <br><br>
                        Teniendo en cuenta el escenario actual, la misa será transmitida en Facebook Live de la Universidad y del Colegio María Auxiliadora.
                        <br><br>
                        <b>Les esperamos el viernes 6 de noviembre desde las 8:00 horas.</b>
                    </p>

                    <div class="share mt-5">
                        <div class="d-flex">
                            <h6>Compartir</h6>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-facebook"></ion-icon></h4></a>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-linkedin"></ion-icon></h4></a>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-twitter"></ion-icon></h4></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection