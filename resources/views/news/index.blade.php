@extends('layout.app')

@section('head')

@endsection

@section('content')
    <div class="header-top mb-0">
        <div class="container">
            <h2 data-aos="fade-up">Noticias</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="news mt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @foreach ($news as $key => $new)
                        @if ($key < ($news->count() / 2))

                            <div class="card" data-aos="fade-left">
                                <div class="card-img-top">
                                    <img src="{{ asset($new->image_path.$new->image_name) }}" class="img-fluid" alt="...">
                                </div>
                                <div class="card-body">
                                    <div class="fecha">
                                        <h3>@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%d", strtotime($new->datetime)) }}</h3>
                                        <p class="text-uppercase">@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%b", strtotime($new->datetime)) }}</p>
                                    </div>
                                    <h3 class="card-text my-5">{{ $new->title }}</h3>
                                    <a href="{{ route('noticias.detail', $new->id) }}" class="">Continuar leyendo</a>
                                </div>
                            </div>
                        {{--@elseif($key == 1)
                            <div class="card" data-aos="fade-left">
                                <div class="card-img-top">
                                    <img src="{{ asset($new->image_path.$new->image_name) }}" class="img-fluid" alt="...">
                                </div>
                                <div class="card-body">
                                    <div class="fecha">
                                        <h3>@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%d", strtotime($new->datetime)) }}</h3>
                                        <p class="text-uppercase">@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%b", strtotime($new->datetime)) }}</p>
                                    </div>
                                    <h3 class="card-text my-5">{{ $new->title }}</h3>
                                    <a href="{{ route('noticias.detail', $new->id) }}" class="">Continuar leyendo</a>
                                </div>
                            </div>--}}
                        @endif
                    @endforeach
                </div>
                <div class="col-md-4 mt-5">
                    @foreach ($news as $key => $new)
                        @if ($key > ($news->count() / 2))

                            <div class="card" data-aos="fade-up">
                                <div class="card-body">
                                    <div class="fecha">
                                        <h3>@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%d", strtotime($new->datetime)) }}</h3>
                                        <p class="text-uppercase">@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%b", strtotime($new->datetime)) }}</p>
                                    </div>
                                    <h3 class="card-text my-5">{{ $new->title }}</h3>
                                    <a href="{{ route('noticias.detail', $new->id) }}" class="">Continuar leyendo</a>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    {{--<div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>
                            <h3 class="card-text my-5">Participación de Expo Carreras Virtual</h3>
                            <a href="{{ route('news-detail-2') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones</h3>
                            <a href="{{ route('news-detail-5') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
    </section>

@endsection
