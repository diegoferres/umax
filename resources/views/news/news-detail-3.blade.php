@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top mb-0">
        <div class="container">
            <h2 data-aos="fade-up">Capacitación de Docentes – Patricia Nieto</h2>
        </div>
        <img src="{{ asset('images/news/news-3.png') }}" alt="" class="w-100">
    </div>
    <section class="news mt-0 bg-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card" data-aos="fade-left">
                        <div class="">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>
                            <h3 class="card-text my-5">La excelencia comienza por casa </h3>
                        </div>
                        <div class="card-img-top">
                            <img src="{{ asset('images/news/news-3.png') }}" class="img-fluid" alt="...">
                        </div>
                    </div>
                    <p class="mt-5">La formación constante es el camino sin escalas hacia la excelencia tanto en el estudio como en el trabajo. 
                        <br><br> 
                        Bajo esa premisa, la Universidad María Auxiliadora emprendió una serie de capacitaciones para su principal capital, sus recursos humanos. 
                        <br><br>
                        Funcionaros y docentes de la UMAX participaron en charlas virtuales sobre temas tales como la Razón del Trabajo y Manejo del Estrés, respectivamente. 
                        <br><br>
                        Buscando siempre dar respuestas satisfactorias a los requerimientos diarios, nos enfocamos a dotar a nuestros colaboradores de las herramientas esenciales para el desarrollo de labor en tiempo y forma.
                    </p>

                    <div class="share mt-5">
                        <div class="d-flex">
                            <h6>Compartir</h6>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-facebook"></ion-icon></h4></a>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-linkedin"></ion-icon></h4></a>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-twitter"></ion-icon></h4></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection