@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top mb-0">
        <div class="container">
            <h2 data-aos="fade-up">Participación de Expo Carreras Virtual</h2>
        </div>
        <img src="{{ asset('images/news/news-2.png') }}" alt="" class="w-100">
    </div>
    <section class="news mt-0 bg-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card" data-aos="fade-left">
                        <div class="">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>
                            <h3 class="card-text my-5">UMAX marca presencia en Expo Carrera</h3>
                        </div>
                        <div class="card-img-top">
                            <img src="{{ asset('images/news/news-2.png') }}" class="img-fluid" alt="...">
                        </div>
                    </div>
                    <p class="mt-5">La Universidad María Auxiliadora estará presente en la Expo Carrera Virtual, la feria destinada a las opciones en educación terciaria, organizada por la Asociación Paraguaya de Universidades Privadas. <br><br>
                        Adaptándose a los tiempos de distanciamiento social, se desarrollará en modalidad virtual del 3 al 10 de noviembre. <br><br>
                        Los que visiten el stand de la UMAX se encontrarán con contenido interactivo y actualizado sobre nuestra oferta académica, las ventajas y la proyección de nuestra universidad tanto en el plano nacional como internacional.
                    </p>

                    <div class="share mt-5">
                        <div class="d-flex">
                            <h6>Compartir</h6>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-facebook"></ion-icon></h4></a>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-linkedin"></ion-icon></h4></a>
                            <a href="" target="_blank"><h4 class="mx-3"><ion-icon name="logo-twitter"></ion-icon></h4></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection