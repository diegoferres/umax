@extends('layout.app')

@section('head')
    <style>
        .efecto-1 {
            box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.5);
            width: 30%;
        }
    </style>

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Bienestar Estudiantil</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="mt-5 text-center text-justify">

                {{--<h3 data-aos="fade-up">Justificación y Objetivos</h3>--}}
                <p class="mt-0" data-aos="fade-up" data-aos-duration="800" >
                    La Dirección de Bienestar Estudiantil diseña y ejecuta los planes tendientes a la orientación de
                    nuestros estudiantes en el siempre complejo y exigente proceso de formación en Educación Superior.
                    Su principio y su fin se centra en el estudiante, buscando contribuir en su calidad de vida, con
                    atención a todas las dimensiones de la persona, física, social, emocional, cognitiva y espiritual.
                </p>
            </div>

            <h4 class="mt-5 aos-init aos-animate" data-aos="fade-up">Ámbitos de acción</h4>
            <ol><b>Desarrollo Académico:</b> Contempla el apoyo integral a los estudiantes, especialmente en el proceso
                de
                integración y la adaptación a la comunidad universitaria, como bases de su aprendizaje y su
                posterior inserción al mundo profesional
            </ol>

            <ol><b>Desarrollo Integral-Personal:</b> Este ámbito se enfoca principalmente en las dimensiones del ser
                humano en
                el
                contexto de la vida institucional y su proyección a la sociedad, propiciando el crecimiento personal,
                promoviendo habilidades para la vida con enfoque en los valores humanos.
            </ol>

            <ol><b>Calidad de Vida:</b> La búsqueda permanente del bienestar se centra en la satisfacción de las
                necesidades,
                entendiendo a los estudiantes e impulsándolos a alcanzar sus múltiples potencialidades. La calidad de
                vida y
                por consiguiente el tener una vida digna representa el ideal máximo del bienestar.
            </ol>

            <h5 class="mt-5 aos-init aos-animate" data-aos="fade-up">Acciones</h5>
            {{--ol.abc {list-style-type: upper-latin;}--}}
            {{--<ol>
                <b>a) Programas de Becas:</b> <br>La Universidad María Auxiliadora, reconoce e impulsa el esfuerzo de
                quienes buscan la excelencia académica. A través del programa de becas, se otorgan diversos beneficios a
                quienes reúnen los méritos requeridos y cumplen una serie de condiciones para el efecto.
                <br>
                Entre ellas se destaca la participación activa de los beneficiados en actividades académicas,
                científicas, culturales, deportivas, religiosas y recreativas promovidas por la UMAX.
                <br>
                El estudiante no solo tiene el beneficio mencionado para alcanzar sus metas profesionales, en ese
                proceso complementa su formación prestando servicios comunitarios en poblaciones vulnerables.

                --}}{{--<div class="mt-2">
                    <img width="50%" src="" alt="">
                </div>--}}{{--
            </ol>--}}
            <ol>
                <b>a) Programa de Orientación y atención psicológica:</b> <br>Dicho programa brinda asesoramiento, orientación psicológica y educativa
                personalizada a todos los estudiantes, con el objetivo de promover el desarrollo
                integral de los mencionados, en respuesta a las demandas actuales. Se trabaja en la
                adaptación a la comunidad y en brindar las respuestas concretas y efectivas a las
                problemáticas que puedan suscitarse en el proceso de formación y que pueden afectar
                al desarrollo de la vida del estudiante, ya sea, en el plano académico, personal y social,
                en base a un enfoque preventivo y de promoción de buenos valores.

                {{--<div class="mt-2">
                    <center>
                        <img class="efecto-1" width="50%" src="/images/bienestar/bienestar_9.jpg" alt="">
                    </center>
                </div>--}}
            </ol>

            <ol>
                <b>b) Programa de formación integral al estudiante:</b> <br>El objetivo de este programa es el de
                propiciar espacios participativos de formación integral dirigido a los estudiantes, para el desarrollo
                de capacidades individuales, colectivas y profesionales, mediante acciones para fomentar la salud mental
                en la comunidad universitaria, la convivencia y el sentido de pertenencia.


                {{--<div class="mt-2">
                    <center>
                        <img class="efecto-1" width="30%" src="/images/bienestar/bienestar_2.jpg" alt="">

                        <img class="efecto-1" width="30%" src="/images/bienestar/bienestar_3.jpg" alt="">
                    </center>
                </div>--}}

            </ol>

            <ol>
                <b>c) Atención a la Salud:</b> <br>El cuidado de la salud permite llevar una vida positiva en todos los
                aspectos y es fundamental para el desarrollo pleno. Como institución formadora de profesionales de la
                salud, la UMAX articula y promueve iniciativas para la buena salud tanto de sus estudiantes como para
                los demás componentes de la comunidad educativa.
                <br>
                <br>

                <b>Enfermeria</b>
                <br>
                El área de Enfermería cuenta con personal altamente capacitado y equipos de última generación, para
                responder de manera efectiva en tiempo y forma, cuando se producen situaciones de urgencia en el día a
                día dentro de la institución. Como así también el monitoreo de indicadores de salud, controles
                preventivos, verificación de síntomas, entre otros.
                <br>
                <br>
                También tiene como función, la orientación sobre los diversos aspectos que hacen a la salud, de tal
                forma a contar con información veraz, con un enfoque preventivo integral, al igual que campañas de
                vacunación.
                {{--<div class="mt-2 mb-2">
                    <center>
                    <img class="efecto-1" width="50%" src="/images/bienestar/bienestar_9.jpg" alt="">
                    </center>
                </div>--}}
                <br>
                <br>
                <b>Cantina</b>
                <br>
                Para los momentos donde se necesita una pausa en la jornada de estudio, ponemos a disposición una
                confortable cantina con variado menú para todos los momentos del día.
                <br>
                <br>
                Las confortables instalaciones otorgan un ambiente ideal para el descanso y para disfrutar de los
                alimentos disponibles.
                <br>
                <br>
                El personal se destaca por su capacidad, y especialmente en su trato amable y cordial con los
                estudiantes y colaboradores en general, así se promueve una convivencia armoniosa, necesaria y siempre
                bienvenida dentro de la institución.

                {{--<div class="mt-2 mb-2">
                    <center>
                        <img class="efecto-1" width="50%" src="/images/bienestar/bienestar_4.jpg" alt="">
                    </center>
                </div>--}}


            </ol>

            <ol>
                <b>d) Programa de promoción cultural:</b> <br>Organiza diversas actividades que promueven el
                desarrollo de habilidades tales como: Poesía, teatro, canto, arte, danzas, artesanía,
                permitiendo la integración, el trabajo en equipo y la solidaridad entre compañeros lo
                que se evidenciará en sus presentaciones. Primarán en todas las actuaciones la
                revaloración de nuestra cultura, el amor al legado histórico y la identificación con el
                patrimonio local, regional y la interculturalidad.


                {{--<div class="mt-2">
                    <center>
                        <img class="efecto-1" width="50%" src="/images/bienestar/bienestar_6.jpg" alt="">
                    </center>
                </div>--}}
            </ol>

            <ol>
                <b>e) Programas deportivos:</b> <br>El deporte es una herramienta de integración que a su vez permite el
                cuidado de la salud, mediante el ejercicio. La UMAX comprende y respalda el rol del deporte en la
                sociedad y en virtud al mismo, organiza y acompaña diversas actividades deportivas en sus instalaciones.
                <br>
                <br>
                Los torneos internos de Fútbol, Vóleibol, Handball tienen amplia popularidad y aceptación entre los
                estudiantes, quienes cada año dan lo mejor de sí para representar de la mejor manera a sus cursos y
                lograr el título.
                <br>
                <br>
                Estas actividades a su vez permiten que los deportistas destacados, integren la selección universitaria
                que ha participado con éxito en diversas competiciones universitarias a nivel nacional.


                {{--<center>
                <div class="mt-2">
                    <img class="efecto-1" width="50%" src="/images/bienestar/bienestar_8.jpg" alt="">
                    <img class="efecto-1" width="50%" src="/images/bienestar/bienestar_7.jpg" alt="">
                </div>
                </center>--}}
            </ol>
            <br>
            <h5>Contacto</h5>
            <h6>bienestar_estudiantil@umax.edu.py</h6>
            <h6>+595 974 698 522</h6>
        </div>

        <div class="fotos">
            <div class="container">
                <h3 class="mt-5 py-5" data-aos="fade-up">Fotos</h3>
            </div>
            <!-- Glider-->
            <div class="glider-contain mb-5">
                <div class="glider">
                    <div class="ml-0" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_1.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_2.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_3.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_4.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_6.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_7.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_8.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_9.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_10.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_11.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_12.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/bienestar/bienestar_13.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-angle-left"></i>
                    </button>
                    <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
            <!-- End Glider-->
        </div>

    </section>

@section('scripts')
    <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script>
@endsection

@endsection
