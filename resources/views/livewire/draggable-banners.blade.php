<div>
    <div class="row">
        <form enctype="multipart/form-data" wire:submit.prevent="storeImage">

            <div class="form-group row">
                <div class="col-sm-4">
                    <label>Nombre:</label>
                    <input required wire:model="banner_name" class="form-control form-control-sm">
                </div>
                <div class="col-sm-7">
                    <label>Archivo:</label>
                    <input required wire:model="banner_image" type="file" class="form-control form-control-sm" accept="image/*">
                </div>
                <div class="col-sm-1">
                    <br>
                    <button type="submit" class="fa-pull-right btn btn-info btn-xs"><i class="fas fa-plus"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>

                <th>#</th>
                <th>Nombre</th>
                <th>Activo</th>
                <th>Vista previa</th>
                <th>Cargado el</th>
                <th>Arrastrar</th>
                <th>Acciones</th>
            </tr>

            </thead>
            <tbody wire:sortable="sortTable">
            @foreach ($banners->sortBy('order') as $banner)
                <tr wire:sortable.item="{{ $banner->id }}" wire:key="task-{{ $banner->id }}">
                    <td>{{ $banner->id }}</td>
                    <td>{{ $banner->name }}</td>
                    <td>
                        <input type="checkbox"
                               {{ $banner->status ? 'checked' : '' }}
                               wire:click="switchStatus({{ $banner->id }}, {{ $banner->status ? 1 : 2 }})">
                    </td>
                    <td><img width="30px" src="{{ asset('storage/'.$banner->image_path) }}" alt=""></td>
                    <td>{{ $banner->created_at }}</td>
                    <td style="cursor: grab" wire:sortable.handle><i class="fas fa-grip-vertical fa-2x"></i></td>
                    <td><button class="btn btn-default" wire:click="destroy({{ $banner->id }})"><i class="fas fa-trash"></i></button></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
