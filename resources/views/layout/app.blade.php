<!doctype html>
<html lang="es" xmlns:livewire="http://www.w3.org/1999/html">

<head>
    <title>UMAX</title>
    <!-- Required meta tags -->
    <meta name="description" content="Con 28 años de vida institucional, 13 de ellos en el plano de la Educación Superior, la Universidad María Auxiliadora se erige como referencia en la enseñanza integral de carreras de salud.">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="57x57" href="/images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicon-16x16.png">
    <link rel="manifest" href="/images/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{ asset('css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('css/aos.css')}}" rel="stylesheet">
    <style>
        .nav-link:hover + .dropdown-menu { display: block; }
    </style>
    <script src="{{asset('js/main.js')}}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-HPPRN72SSE"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-HPPRN72SSE');
    </script>

    @yield('head')
</head>

<body>
<!-- Code for Action: Umax_Conversion -->
<!-- Code for Pixel: Umax_Conversion -->
<!-- Begin DSP Conversion Action Tracking Code Version 9 -->
<script type='text/javascript'>
    (function() {
        var w = window, d = document;
        var s = d.createElement('script');
        s.setAttribute('async', 'true');
        s.setAttribute('type', 'text/javascript');
        s.setAttribute('src', '//c1.rfihub.net/js/tc.min.js');
        var f = d.getElementsByTagName('script')[0];
        f.parentNode.insertBefore(s, f);
        if (typeof w['_rfi'] !== 'function') {
            w['_rfi']=function() {
                w['_rfi'].commands = w['_rfi'].commands || [];
                w['_rfi'].commands.push(arguments);
            };
        }
        _rfi('setArgs', 'ver', '9');
        _rfi('setArgs', 'rb', '45540');
        _rfi('setArgs', 'ca', '20837597');
        _rfi('setArgs', '_o', '45540');
        _rfi('setArgs', '_t', '20837597');
        _rfi('track');
    })();
</script>
<noscript>
    <iframe src='//20837597p.rfihub.com/ca.html?rb=45540&ca=20837597&_o=45540&_t=20837597&ra={{ rand(1,1000) }}' style='display:none;padding:0;margin:0' width='0' height='0'>
    </iframe>
</noscript>
<!-- End DSP Conversion Action Tracking Code Version 9 -->
<!-- End DSP Conversion Action Tracking Code Version 9 -->
@include('partials.nav')

<section class="page-content">
    @yield('content')
</section>

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            @include('partials.form-inscripcion')
        </div>
    </div>
</div>

<a href='#' class="btn-float">
    <ion-icon name="chevron-up-outline" class="my-float"></ion-icon>
</a>

<a href="https://wa.me/595983746014" class="btn-float whatsapp"
   target="_blank">
    <ion-icon name="logo-whatsapp" class="my-float"></ion-icon>
</a>

@include('partials.footer')

<!-- JavaScript -->
<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
<script src="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.js"></script>

<script src="{{asset('js/aos.js')}}"></script>
<script>
    AOS.init();
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
@yield('scripts')

<!-- Code for Action: Umax_Aprendizaje -->
<!-- Code for Pixel: Umax_Aprendizaje -->
<!-- Begin DSP Conversion Action Tracking Code Version 9 -->
<script type='text/javascript'>
    (function() {
        var w = window, d = document;
        var s = d.createElement('script');
        s.setAttribute('async', 'true');
        s.setAttribute('type', 'text/javascript');
        s.setAttribute('src', '//c1.rfihub.net/js/tc.min.js');
        var f = d.getElementsByTagName('script')[0];
        f.parentNode.insertBefore(s, f);
        if (typeof w['_rfi'] !== 'function') {
            w['_rfi']=function() {
                w['_rfi'].commands = w['_rfi'].commands || [];
                w['_rfi'].commands.push(arguments);
            };
        }
        _rfi('setArgs', 'ver', '9');
        _rfi('setArgs', 'rb', '45540');
        _rfi('setArgs', 'ca', '20837596');
        _rfi('setArgs', '_o', '45540');
        _rfi('setArgs', '_t', '20837596');
        _rfi('track');
    })();
</script>
<noscript>
    <iframe src='//20837596p.rfihub.com/ca.html?rb=45540&ca=20837596&_o=45540&_t=20837596&ra={{ rand(1,1000) }}' style='display:none;padding:0;margin:0' width='0' height='0'>
    </iframe>
</noscript>
</body>

</html>
