<section class="form-carrera">
    <div class="form">
        <h3 class="mb-5" data-aos="fade-right"
            data-aos-duration="500">Elige tu futuro</h3>
        <form action="{{ route('mail.send.future') }}" method="get">
            <div class="form-group" data-aos="fade-up">
                <select class="form-control custom-select" name="career" id="selectCarrera" required>
                    <option disabled selected>Carrera</option>
                    <option>Medicina</option>
                    <option>Licenciatura en Enfermeria</option>
                    <option>Técnico Superior en Farmacia</option>
                    <option>Técnico Superior en Radiología</option>
                    <option>Técnico Superior en Laboratorio Clínico</option>
                    <option>Técnico Superior en Masaje Terapéutico</option>
                    <option>Técnico Superior en Enfermería</option>
                </select>
            </div>
            <div class="form-group" data-aos="fade-up">
                <input type="text" class="form-control" id="inputName" name="name" placeholder="Nombre y Apellido"
                       required>
            </div>
            <div class="form-group" data-aos="fade-up">
                <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Corrreo electrónico"
                       required>
            </div>
            <div class="form-group mb-5" data-aos="fade-up">
                <input type="text" class="form-control" id="inputCelular" name="cellphone" placeholder="Celular"
                       required>
            </div>
            <button class="btn btn-primary" type="submit" data-aos="fade-up">Quiero Inscribirme</button>
            {{--<a href="#" class="btn btn-primary" data-aos="fade-up">Quiero Inscribirme</a>--}}
        </form>
        <!-- Code for Action: Umax_Conversion -->
        <!-- Code for Pixel: Umax_Conversion -->
        <!-- Begin DSP Conversion Action Tracking Code Version 9 -->
        <script type='text/javascript'>
            (function() {
                var w = window, d = document;
                var s = d.createElement('script');
                s.setAttribute('async', 'true');
                s.setAttribute('type', 'text/javascript');
                s.setAttribute('src', '//c1.rfihub.net/js/tc.min.js');
                var f = d.getElementsByTagName('script')[0];
                f.parentNode.insertBefore(s, f);
                if (typeof w['_rfi'] !== 'function') {
                    w['_rfi']=function() {
                        w['_rfi'].commands = w['_rfi'].commands || [];
                        w['_rfi'].commands.push(arguments);
                    };
                }
                _rfi('setArgs', 'ver', '9');
                _rfi('setArgs', 'rb', '45540');
                _rfi('setArgs', 'ca', '20837597');
                _rfi('setArgs', '_o', '45540');
                _rfi('setArgs', '_t', '20837597');
                _rfi('track');
            })();
        </script>
        <noscript>
            <iframe src='//20837597p.rfihub.com/ca.html?rb=45540&ca=20837597&_o=45540&_t=20837597&ra={{ rand(1,1000) }}' style='display:none;padding:0;margin:0' width='0' height='0'>
            </iframe>
        </noscript>
        <!-- End DSP Conversion Action Tracking Code Version 9 -->
        <!-- End DSP Conversion Action Tracking Code Version 9 -->
    </div>
    <div class="banner">
        <img src="{{ asset('images/estudiante.png') }}" class="img-fluid" alt="Imagen de Estudiante"
             data-aos="fade-left"
             data-aos-duration="500">
    </div>
</section>
