<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container px-0">
        <div class="logo" data-aos="fade-right">
            <img class="img-fluid" src="{{asset('/images/logo.png')}}" alt="Logo UMAX">
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon" data-aos="fade-left"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNavDropdown">

            <ul class="navbar-nav ml-auto align-items-center" data-aos="fade-left">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('index') }}">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about') }}">Nosotros</a>
                </li>
                {{--<li class="nav-item dropdown">
                    <a class="nav-link" id="navbarDropdownCarreras" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Carreras</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownCarreras">
                        <a class="dropdown-item" href="#" id="navbarDropdownGrados" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Grado</a>
                        <div class="dropdown-menu dropdown-lvl" aria-labelledby="navbarDropdownGrados">
                            <a class="dropdown-item" href="{{ route('revista-cientifica') }}">Medicina</a>
                            <a class="dropdown-item" href="{{ route('equipo-editorial') }}">Lic. Enfermería</a>
                        </div>

                        <a class="dropdown-item" href="#" id="navbarDropdownPregrados" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pregrado</a>
                        <div class="dropdown-menu dropdown-lvl" aria-labelledby="navbarDropdownPregrados">
                            <a class="dropdown-item" href="{{ route('revista-cientifica') }}">Medicinas</a>
                            <a class="dropdown-item" href="{{ route('equipo-editorial') }}">Lic. Enfermerías</a>
                        </div>
                        --}}{{--<a class="dropdown-item" href="{{ route('investigacion') }}">Pregrado</a>--}}{{--

                    </div>
                </li>--}}
                <li class="nav-item dropdown">

                    <a class="nav-link" id="navbarDropdownCarreras" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Carreras</a>
                    <div class="dropdown-menu  " aria-labelledby="navbarDropdownCarreras">

                        <a class="dropdown-item sub-menu" href="#" id="navbarDropdownGrado" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Grado</a>
                        <div class="dropdown-menu dropdown-lvl" aria-labelledby="navbarDropdownGrado">
                            <a class="dropdown-item" href="{{ route('medicina') }} ">Medicina</a>
                            <a class="dropdown-item" href="{{ route('enfermeria') }}">Licenciatura en Enfermería</a>
                        </div>

                        <a class="dropdown-item sub-menu" href="#" id="navbarDropdownPregrado" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pregrado</a>
                        <div class="dropdown-menu dropdown-lvl" aria-labelledby="navbarDropdownPregrado">
                            <a class="dropdown-item" href="{{ route('pregrado-1') }}">Técnico Superior en Farmacia</a>
                            <a class="dropdown-item" href="{{ route('pregrado-2') }}">Técnico Superior en Radiología</a>
                            <a class="dropdown-item" href="{{ route('pregrado-3') }}">Técnico Superior en Laboratorio Clínico</a>
                            <a class="dropdown-item" href="{{ route('pregrado-4') }}">Técnico Superior en Masaje Terapéutico</a>
                            <a class="dropdown-item" href="{{ route('pregrado-5') }}">Técnico Superior en Enfermería</a>
                        </div>

                        <a class="dropdown-item" href="{{ route('postgrado') }}">Postgrado</a>

                        {{--<a class="dropdown-item sub-menu" href="#" id="navbarDropdownPostgrado" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Postgrado</a>
                        <div class="dropdown-menu dropdown-lvl" aria-labelledby="navbarDropdownPostgrado">
                            <a class="dropdown-item" href="#">Próximamente</a>
                        </div>--}}
                    </div>
                </li>
                {{--<li class="nav-item">
                    <a class="nav-link" href="{{ route('postgrado') }}">
                        Postgrados
                    </a>
                    --}}{{-- <div class="dropdown-menu" aria-labelledby="navbarDropdownPostgrado">
                        <a class="dropdown-item" href="{{ route('postgrado') }}">Información</a>
                        <a class="dropdown-item" href="{{ route('maestria') }}">Maestría</a>
                        <a class="dropdown-item" href="{{ route('especializacion') }}">Especialización</a>
                        <a class="dropdown-item" href="{{ route('diplomado') }}">Diplomado</a>
                    </div> --}}{{--
                </li>--}}
                <li class="nav-item dropdown">
                    <a class="nav-link" id="navbarDropdownInvestigacion" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Investigación</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownInvestigacion">
                        <a class="dropdown-item" href="{{ route('investigacion') }}">Presentación</a>
                        <a class="dropdown-item" href="#" id="navbarDropdownRevista" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Núcleos de Investigación</a>
                        <div class="dropdown-menu dropdown-lvl" aria-labelledby="navbarDropdownRevista">
                            <a class="dropdown-item" href="{{ route('salud-mental') }}">Psiquiatría y Salud Mental</a>
                            <a class="dropdown-item" href="{{ route('innovacion-medica') }} ">Innovación Médica</a>
                            <a class="dropdown-item" href="{{ route('bioestadistica-aplicada-salud') }}">Bioestadística aplicada a la salud</a>
                            <a class="dropdown-item" href="{{ route('nutricion-geriatria') }}">Nutrición y Geriatría</a>
                            <a class="dropdown-item" href="{{ route('donacion-transplante-organos') }}">Donación y Transplante de Órganos</a>
                            <a class="dropdown-item" href="{{ route('tecnologia-informacion-comunicacion-salud') }}">Tecnologías de la Información y Comunicación aplicada a la salud</a>
                            <a class="dropdown-item" href="{{ route('enfermedades-infecciosas') }}">Enfermedades Infecciosas</a>
                        </div>
                        <a class="dropdown-item" href="#" id="navbarDropdownRevista" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Revista Científica
                            UMAX</a>
                        <div class="dropdown-menu dropdown-lvl" aria-labelledby="navbarDropdownRevista">
                            <a class="dropdown-item" href="{{ route('revista-cientifica') }} ">La Revista</a>
                            <a class="dropdown-item" href="{{ route('equipo-editorial') }}">Equipo Editorial</a>
                        </div>

                        <a class="dropdown-item" href="{{ route('divulgacion') }}">Divulgación científica</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdownExtension" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Extensión
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownExtension">
                        <a class="dropdown-item" href="{{ route('convenios') }}">Convenios</a>
                        <a class="dropdown-item" href="{{ route('extension') }}">Extensión y Vinculación con el Medio</a>
                        {{--<a class="dropdown-item" href="{{ route('pastoral') }}">Pastoral</a>--}}
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('pastoral') }}">Pastoral</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('bienestar') }}">Bienestar Estudiantil</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdownVirtual" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Virtual
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownVirtual">
                        <a class="dropdown-item" href="https://portal.umax.edu.py">Portal de Información</a>
                        <a class="dropdown-item" href="https:\\login.microsoftonline.com\">Portal Microsoft Office 365</a>
                        <a class="dropdown-item" href="{{ route('virtual') }}">Aula Virtual</a>
                        <a class="dropdown-item" href="{{ route('biblioteca') }}">Biblioteca Virtual</a>

                    </div>
                </li>

                {{-- <li class="nav-item">
                     <a class="nav-link" href="https://alumnos.umax.edu.py/">Portal</a>
                 </li>--}}
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('egresados') }}">Egresados</a>
                </li>
                <li class="nav-item mr-lg-3">
                    <a class="nav-link" href="{{ route('contacto') }}">Contacto</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary my-auto" href="#" data-toggle="modal" data-target=".bd-example-modal-xl">Inscribirme</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
