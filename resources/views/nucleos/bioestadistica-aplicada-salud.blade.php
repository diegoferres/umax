@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Bioestadística aplicada a la salud</h2>
        </div>
        <img src="{{ asset('images/banner-medicina.png') }}" alt="" class="img-fluid">
    </div>

    <section class="university">

        <div class="malla">
            <div class="container">
                <div class="d-flex justify-content-md-start">
                    <img width="120px" class="mb-1" src="/images/nucleos/dario_alviso.jpg" alt="">
                    <h3 class="mb-1 ml-4 pt-5" data-aos="fade-left"><a href="https://www.researchgate.net/profile/Dario-Alviso">PhD. Darío López Alviso</a>
                    </h3>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-2018" role="tabpanel"
                         aria-labelledby="pills-2018-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab"
                                   href="#primer-2018"
                                   role="tab" aria-controls="primer-2018" aria-selected="true">Perfil</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo-2018"
                                   role="tab" aria-controls="segundo-2018" aria-selected="false">Miembros</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero-2018"
                                   role="tab" aria-controls="tercero-2018" aria-selected="false">Proyectos</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto-2018"
                                   role="tab"
                                   aria-controls="cuarto-2018" aria-selected="false">Artículos Científicos</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto-2018"
                                   role="tab"
                                   aria-controls="quinto-2018" aria-selected="false">Materiales de Divulgación</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                {{-- PERFIL --}}
                                <div class="tab-pane fade show active" id="primer-2018" role="tabpanel"
                                     aria-labelledby="primer-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Ingeniero electromecánico por la Facultad de Ingeniería de la Universidad
                                        Nacional de Asunción (FIUNA).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Master en Ciencias por la Ecole Centrale Paris, Francia.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        PhD con doble diploma FIUNA/Ecole Centrale Paris.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Posdoctorado en la Universidad de Buenos Aires.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Investigador Asistente del CONACYT, Argentina. Ingreso 2020, en donde trabaja en
                                        las áreas de
                                        combustión experimental y numérica y cuerdas vocales.
                                    </p>
                                </div>
                                {{-- COLABORADORES --}}
                                <div class="tab-pane fade" id="segundo-2018" role="tabpanel"
                                     aria-labelledby="segundo-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Dra. Olga Sosa AquinoLicenciada en Ciencias Matemáticas, Docente de Tiempo
                                        Completo.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Lic. Niselli Burguez, Licenciada en Enfermería, Licenciada en Obstetricia,
                                        Docente de Tiempo Parcial.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Lic. María Celeste Ramírez Barreto, Licenciada en Ciencias mención Matemática
                                        Pura, Docente de Tiempo
                                        Parcial.
                                    </p>
                                </div>

                                {{-- PROYECTOS --}}
                                <div class="tab-pane fade" id="tercero-2018" role="tabpanel"
                                     aria-labelledby="tercero-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">

                                    </p>
                                </div>

                                {{-- PAPERS --}}
                                <div class="tab-pane fade" id="cuarto-2018" role="tabpanel"
                                     aria-labelledby="cuarto-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Prediction of the refractive index and speed of sound of biodiesel from its
                                        composition and molecular structure.
                                    </p>
                                </div>

                                {{-- MATERIALES DE DIVULGACION --}}
                                <div class="tab-pane fade" id="quinto-2018" role="tabpanel"
                                     aria-labelledby="quinto-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>Editorial Elsevier publicará artículo de investigador de la UMAX.
                                        <br><ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>Investigador de la UMAX implementa Machine Learning entre sus líneas de investigación.
                                        <br><ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>Bioestadística: una herramienta fundamental en la investigación científica.
                                        <br><ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>Docentes de Tiempo Completo de la UMAX se capacitan en Bioestadística.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>

@section('scripts')
    {{--  <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script> --}}
@endsection

@endsection
