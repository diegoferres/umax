@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Enfermedades Infecciosas</h2>
        </div>
        <img src="{{ asset('images/banner-medicina.png') }}" alt="" class="img-fluid">
    </div>

    <section class="university">

        <div class="malla">
            <div class="container">
                <div class="d-flex justify-content-md-start">
                    <img width="120px" class="mb-1" src="/images/nucleos/dr_jaime_vester.jpg" alt="">
                    <h3 class="mb-1 ml-4 pt-5" data-aos="fade-left"><a href="https://www.researchgate.net/profile/Jaime-Vester">Lic. Jaime Vester</a>
                    </h3>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-2018" role="tabpanel"
                         aria-labelledby="pills-2018-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab"
                                   href="#primer-2018"
                                   role="tab" aria-controls="primer-2018" aria-selected="true">Perfil</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo-2018"
                                   role="tab" aria-controls="segundo-2018" aria-selected="false">Miembros</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero-2018"
                                   role="tab" aria-controls="tercero-2018" aria-selected="false">Proyectos</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto-2018"
                                   role="tab"
                                   aria-controls="cuarto-2018" aria-selected="false">Artículos Científicos</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto-2018"
                                   role="tab"
                                   aria-controls="quinto-2018" aria-selected="false">Materiales de Divulgación</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="primer-2018" role="tabpanel"
                                     aria-labelledby="primer-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Licenciado en Enfermería por la Universidad Nacional de Asunción (FENOB – UNA).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Especialista en Didáctica de la Educación Superior – Rectorado, UNA.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Investigador categorizado en el Programa Nacional de Incentivo a los
                                        <br>
                                        Investigadores (PRONII) del Consejo Nacional de Ciencia y Tecnología (CONACYT).
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Docente de la cátedra Investigación en Enfermería FENOB-UNA.
                                    </p>
                                </div>
                                <div class="tab-pane fade" id="segundo-2018" role="tabpanel"
                                     aria-labelledby="segundo-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Dr. Iván Fernando Allende Criscioni, Médico, Docente de Tiempo Parcial.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Dra. Ángela Gabriela Espínola Linares Médico, Docente de Tiempo Completo.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Dra. Gloria Celeste Samudio Domínguez, Médico, Docente de Tiempo Parcial.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Lic. Gustavo Adolfo Villalba Dure, Licenciado en Ciencias Mención Biología,
                                        Docente de Tiempo Completo
                                    </p>
                                </div>
                                <div class="tab-pane fade" id="tercero-2018" role="tabpanel"
                                     aria-labelledby="tercero-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">

                                    </p>
                                </div>
                                <div class="tab-pane fade" id="cuarto-2018" role="tabpanel"
                                     aria-labelledby="cuarto-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">

                                    </p>
                                </div>
                                <div class="tab-pane fade" id="quinto-2018" role="tabpanel"
                                     aria-labelledby="quinto-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Vacunación basada en la evidencia científica: aspectos para mejorar el sistema.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Vacuna contra la COVID-19: clave para la inmunización.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        La UMAX presenta Núcleo de Investigación sobre Enfermedades Infecciosas
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>

@section('scripts')
    {{--  <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script> --}}
@endsection

@endsection
