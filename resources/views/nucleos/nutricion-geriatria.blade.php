@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Nutrición y Geriatría</h2>
        </div>
        <img src="{{ asset('images/banner-medicina.png') }}" alt="" class="img-fluid">
    </div>

    <section class="university">

        <div class="malla">
            <div class="container">
                <div class="d-flex justify-content-md-start">
                    <img width="120px" class="mb-1" src="/images/nucleos/johana_meza.jpeg" alt="">
                    <h3 class="mb-1 ml-4 pt-5" data-aos="fade-left"><a href="https://www.researchgate.net/scientific-contributions/Johana-Vanessa-Meza-Paredes-2176922193">MSc. Johana Meza</a>
                    </h3>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-2018" role="tabpanel"
                         aria-labelledby="pills-2018-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab"
                                   href="#primer-2018"
                                   role="tab" aria-controls="primer-2018" aria-selected="true">Perfil</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo-2018"
                                   role="tab" aria-controls="segundo-2018" aria-selected="false">Miembros</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero-2018"
                                   role="tab" aria-controls="tercero-2018" aria-selected="false">Proyectos</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto-2018"
                                   role="tab"
                                   aria-controls="cuarto-2018" aria-selected="false">Artículos Científicos</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto-2018"
                                   role="tab"
                                   aria-controls="quinto-2018" aria-selected="false">Materiales de Divulgación</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                {{-- PERFIL --}}
                                <div class="tab-pane fade show active" id="primer-2018" role="tabpanel"
                                     aria-labelledby="primer-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Magíster en Nutrición Humana por la Facultad de Ciencias Médicas de la
                                        Universidad Nacional de Asunción
                                        (FCM – UNA).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Especialista en Dietética Clínica y Soporte Nutricional por la Facultad de
                                        Ciencias Químicas - UNA.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Didáctica Superior Universitaria - Facultad de ciencias económicas – UNA.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Diplomado en la Cátedra de Ciencia, Tecnología y Sociedad (CTS) del Consejo
                                        Nacional de Ciencia y
                                        Tecnología (CONACYT).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Doctorado en Educación con énfasis en la educación superior Facultad de
                                        Filosofía – UNA.
                                    </p>
                                </div>
                                {{-- COLABORADORES --}}
                                <div class="tab-pane fade" id="segundo-2018" role="tabpanel"
                                     aria-labelledby="segundo-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Lic. Lisy Rocío Olmedo Valdez, Licenciada en Nutrición, Docente de Tiempo
                                        Completo.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Lic. María Consuelo Vera Segovia, Licenciada en Nutrición, Docente de Tiempo
                                        Completo.
                                    </p>
                                </div>

                                {{-- PROYECTOS --}}
                                <div class="tab-pane fade" id="tercero-2018" role="tabpanel"
                                     aria-labelledby="tercero-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">

                                    </p>
                                </div>

                                {{-- PAPERS --}}
                                <div class="tab-pane fade" id="cuarto-2018" role="tabpanel"
                                     aria-labelledby="cuarto-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>Sedentarismo en adultos mayores residentes en hogares de ancianos de la zona periurbana de Asunción.
                                    </p>
                                </div>

                                {{-- MATERIALES DE DIVULGACION --}}
                                <div class="tab-pane fade" id="quinto-2018" role="tabpanel"
                                     aria-labelledby="quinto-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>Experta en nutrición forma parte del núcleo de investigación de la UMAX.
                                        <b></b><ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>Una nutrición equilibrada es fundamental para una vida sana.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>

@section('scripts')
    {{--  <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script> --}}
@endsection

@endsection
