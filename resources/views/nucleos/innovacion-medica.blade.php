@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Innovación Médica</h2>
        </div>
        <img src="{{ asset('images/banner-medicina.png') }}" alt="" class="img-fluid">
    </div>

    <section class="university">

        <div class="malla">
            <div class="container">
                <div class="d-flex justify-content-md-start">
                    <img width="120px" class="mb-1" src="/images/nucleos/lilian_alarcon.jpeg" alt="">
                    <h3 class="mb-1 ml-4 pt-5" data-aos="fade-left"><a href="https://www.researchgate.net/profile/Lilian-Alarcon-3">Dra. Lilian Celeste Alarcón Segovia</a>
                    </h3>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-2018" role="tabpanel"
                         aria-labelledby="pills-2018-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab"
                                   href="#primer-2018"
                                   role="tab" aria-controls="primer-2018" aria-selected="true">Perfil</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo-2018"
                                   role="tab" aria-controls="segundo-2018" aria-selected="false">Miembros</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero-2018"
                                   role="tab" aria-controls="tercero-2018" aria-selected="false">Proyectos</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto-2018"
                                   role="tab"
                                   aria-controls="cuarto-2018" aria-selected="false">Artículos Científicos</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto-2018"
                                   role="tab"
                                   aria-controls="quinto-2018" aria-selected="false">Materiales de Divulgación</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="primer-2018" role="tabpanel"
                                     aria-labelledby="primer-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Doctorado en Ciencias Biológicas por la Universidad Nacional del Litoral
                                        (Argentina).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Ingeniera Industrial y Especialista en Docencia en Educación Superior por la
                                        Universidad Católica Nuestra Señora de la Asunción (Paraguay).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Estancia de formación Científica en el Querrey Simpson Institute For
                                        Bioelectronics (Estados Unidos).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Capacitaciones en Gestión y Desarrollo de Productos e Innovación Tecnológica,
                                        Inteligencia Tecnológica en Actividades de Investigación y Desarrollo y en
                                        Nanomateriales en la Escuela de Nanomateriales aplicados a Energía y Salud por
                                        la Universidad de Buenos Aires (Argentina).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Reconocida por la Municipalidad de Asunción con el Premio Municipal de la
                                        Juventud.
                                    </p>
                                </div>
                                <div class="tab-pane fade" id="segundo-2018" role="tabpanel"
                                     aria-labelledby="segundo-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Dr. Diego Marcelo Muñoz Rodríguez, Médico, Docente de Tiempo Completo de la
                                        UMAX.
                                    </p>
                                </div>
                                <div class="tab-pane fade" id="tercero-2018" role="tabpanel"
                                     aria-labelledby="tercero-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">

                                    </p>
                                </div>
                                <div class="tab-pane fade" id="cuarto-2018" role="tabpanel"
                                     aria-labelledby="cuarto-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Development a Process of Technological Innovation for Industrial Paper
                                        Production.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Multifactorial Effects of Gelling Conditions on Mechanical Properties of
                                        Skin-Like Gelatin Membranes
                                        Intended for In Vitro Experimentation and Artificial Skin Models.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Catalytic effects of magnetic and conductive nanoparticles on immobilized
                                        glucose oxidase in skin sensors.

                                    </p>
                                </div>
                                <div class="tab-pane fade" id="quinto-2018" role="tabpanel"
                                     aria-labelledby="quinto-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>La UMAX cuenta con especialista en nanotecnología e innovación.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>Profesional de la UMAX desarrolla innovador proceso para la industria del papel.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>La Nanotecnología y sus aplicaciones a la investigación científica.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>Docente investigadora de la UMAX publica artículo sobre innovador proyecto en revista de impacto
                                        internacional.
                                    </p>
                                </div>
                                <div class="tab-pane fade" id="sexto-2018" role="tabpanel"
                                     aria-labelledby="sexto-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Doctorado en Ciencias Biológicas por la Universidad Nacional del Litoral
                                        (Argentina).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Ingeniera Industrial y Especialista en Docencia en Educación Superior por la
                                        Universidad Católica Nuestra Señora de la Asunción (Paraguay).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Estancia de formación Científica en el Querrey Simpson Institute For
                                        Bioelectronics (Estados Unidos).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Capacitaciones en Gestión y Desarrollo de Productos e Innovación Tecnológica,
                                        Inteligencia Tecnológica en Actividades de Investigación y Desarrollo y en
                                        Nanomateriales en la Escuela de Nanomateriales aplicados a Energía y Salud por
                                        la Universidad de Buenos Aires (Argentina).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Reconocida por la Municipalidad de Asunción con el Premio Municipal de la
                                        Juventud.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>

@section('scripts')
    {{--  <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script> --}}
@endsection

@endsection
