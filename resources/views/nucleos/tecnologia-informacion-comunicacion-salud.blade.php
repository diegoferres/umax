@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Tecnologías de la Información y Comunicación Aplicada a la Salud</h2>
        </div>
        <img src="{{ asset('images/banner-medicina.png') }}" alt="" class="img-fluid">
    </div>

    <section class="university">

        <div class="malla">
            <div class="container">
                <div class="d-flex justify-content-md-start">
                    <img width="120px" class="mb-1" src="/images/nucleos/dr_nicolas_ayala.jpg" alt="">
                    <h3 class="mb-1 ml-4 pt-5" data-aos="fade-left"><a href="https://www.researchgate.net/profile/Jose-Ayala-Servin">Dr. Nicolás Ayala</a>
                    </h3>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-2018" role="tabpanel"
                         aria-labelledby="pills-2018-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab"
                                   href="#primer-2018"
                                   role="tab" aria-controls="primer-2018" aria-selected="true">Perfil</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo-2018"
                                   role="tab" aria-controls="segundo-2018" aria-selected="false">Miembros</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero-2018"
                                   role="tab" aria-controls="tercero-2018" aria-selected="false">Proyectos</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto-2018"
                                   role="tab"
                                   aria-controls="cuarto-2018" aria-selected="false">Artículos Científicos</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto-2018"
                                   role="tab"
                                   aria-controls="quinto-2018" aria-selected="false">Materiales de Divulgación</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                {{-- PERFIL --}}
                                <div class="tab-pane fade show active" id="primer-2018" role="tabpanel"
                                     aria-labelledby="primer-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Doctor en Medicina y Cirugía por la Facultad de Ciencias Médicas de la
                                        Universidad Nacional de Asunción
                                        (FCM-UNA).
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Médico Residente- Especialidad en Psiquiatría Clínica- Universidad Nacional de
                                        Asunción.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Joven Investigador de la Universidad Nacional de Asunción -2017/2018.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Vice-Director de la Dirección de Relaciones Externas de la Sociedad Científica
                                        de Estudiantes de Medicina
                                        de la Universidad Nacional de Asunción (SOCIEM UNA)- 2014/2016, 2016/2017 y
                                        2017/2019.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Presidente de la Federación Latinoamericana de Sociedades Científicas de
                                        Estudiantes de Medicina
                                        (FELSOCEM)- 2017/2018.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Director del Comité de Prensa y Publicidad de la Revista Científica Discover
                                        Medicine- 2019/2020.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Miembro del Consejo Asesor de la Federación Latinoamericana de Sociedades
                                        Científicas de Estudiantes de
                                        Medicina (FELSOCEM)-2019/2020.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Presidente del Comité de Ética y Sanciones de la Federación Latinoamericana de
                                        Sociedades Científicas de
                                        Estudiantes de Medicina (FELSOCEM)-2020/2021.
                                    </p>
                                </div>
                                {{-- COLABORADORES --}}
                                <div class="tab-pane fade" id="segundo-2018" role="tabpanel"
                                     aria-labelledby="segundo-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Dr. Diego Marcelo Muñoz Rodríguez, Médico, Docente de Tiempo Completo.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Dr. Rodrigo Salvador Pederzoli Mareco, Médico, Docente de Tiempo Completo.
                                    </p>
                                </div>

                                {{-- PROYECTOS --}}
                                <div class="tab-pane fade" id="tercero-2018" role="tabpanel"
                                     aria-labelledby="tercero-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">

                                    </p>
                                </div>

                                {{-- PAPERS --}}
                                <div class="tab-pane fade" id="cuarto-2018" role="tabpanel"
                                     aria-labelledby="cuarto-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Utilización de las tecnologías de la información y comunicación (TIC) en
                                        estudiantes universitarios
                                        paraguayos.
                                    </p>
                                </div>

                                {{-- MATERIALES DE DIVULGACION --}}
                                <div class="tab-pane fade" id="quinto-2018" role="tabpanel"
                                     aria-labelledby="quinto-2018-tab">
                                    <p data-aos="fade-up" data-aos-duration="800">
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Docente de la UMAX publica trabajo sobre la utilización de las TIC en
                                        estudiantes universitarios.
                                        <br>
                                        <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon>
                                        Enfoque en investigación científica y ética fueron temas de capacitación a
                                        docentes de tiempo completo de
                                        la UMAX.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>

@section('scripts')
    {{--  <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script> --}}
@endsection

@endsection
