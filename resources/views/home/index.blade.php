@extends('layout.app')

@section('head')

@endsection

@section('content')

    @include('home.slider')

    <section class="about mb-5">
        <div class="container">
            <div class="row align-items-center mb-3">
                <div class="col-lg-5">
                    <h5 data-aos="fade-up">Sobre nosotros</h5>
                    <h2 data-aos="fade-up">La Universidad <br>María Auxiliadora</h2>
                </div>
                <div class="col-lg-7">
                    <p class="mb-0" data-aos="fade-up">Con 13 años de recorrido en el ámbito de la Educación Superior, la Universidad María
                        Auxiliadora se erige como una institución de referencia en la enseñanza de carreras de salud. <a href="{{ route('about') }}">Continuar
                            leyendo</a>
                        </p>
                </div>
            </div>
        </div>
        <!-- Glider-->
        <div class="glider-contain my-5">
            <div class="glider">
                <div class="ml-0 videos" data-aos="fade-left">
                    <div class="image-prev">
                        <h4>¡SENTÍ EL PULSO UMAX!</h4>
                        <a href="#" class="btn btn-play" data-toggle="modal" data-target="#ModalInstitucional">
                            <ion-icon name="play-sharp"></ion-icon>
                        </a>
                        <img src="{{ asset('images/s1.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
                <div class="ml-1 videos" data-aos="fade-left">
                    <div class="image-prev">
                        <h4>UMAX Laboratorio</h4>
                        <a href="#" class="btn btn-play" data-toggle="modal" data-target="#ModalLaboratorio">
                            <ion-icon name="play-sharp"></ion-icon>
                        </a>
                        <img src="{{ asset('images/s2.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
                <div class="ml-1 videos" data-aos="fade-left">
                    <div class="image-prev">
                        <h4>Biblioteca moderna y actualizada</h4>
                        <a href="#" class="btn btn-play" data-toggle="modal" data-target="#ModalBiblioteca">
                            <ion-icon name="play-sharp"></ion-icon>
                        </a>
                        <img src="{{ asset('images/s3.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
                <div class="ml-1 videos" data-aos="fade-left">
                    <div class="image-prev">
                        <h4>UMAX Sala de Enfermería</h4>
                        <a href="#" class="btn btn-play" data-toggle="modal" data-target="#ModalSala">
                            <ion-icon name="play-sharp"></ion-icon>
                        </a>
                        <img src="{{ asset('images/s4.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
                {{-- <div class="ml-1" data-aos="fade-left">
                    <img src="{{ asset('images/s5.png') }}" class="d-block w-100" alt="IMG">
                </div> --}}
                <div class="ml-1 videos" data-aos="fade-left">
                    <div class="image-prev">
                        <h4>UMAX Clases Prácticas</h4>
                        <a href="#" class="btn btn-play" data-toggle="modal" data-target="#ModalClases">
                            <ion-icon name="play-sharp"></ion-icon>
                        </a>
                        <img src="{{ asset('images/s6.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
                <div class="ml-1 videos" data-aos="fade-left">
                    <div class="image-prev">
                        <h4>UMAX Laboratorio de Simulación</h4>
                        <a href="#" class="btn btn-play" data-toggle="modal" data-target="#ModalSalaSi">
                            <ion-icon name="play-sharp"></ion-icon>
                        </a>
                        <img src="{{ asset('images/s7.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
                <div class="ml-1 videos" data-aos="fade-left">
                    <div class="image-prev">
                        <h4>UMAX Seguridad</h4>
                        <a href="#" class="btn btn-play" data-toggle="modal" data-target="#ModalSeguridad">
                            <ion-icon name="play-sharp"></ion-icon>
                        </a>
                        <img src="{{ asset('images/s8.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
                <div class="ml-1 videos" data-aos="fade-left">
                    <div class="image-prev">
                        <h4>Compromiso con la calidad académica</h4>
                        <a href="#" class="btn btn-play" data-toggle="modal" data-target="#ModalCompromiso">
                            <ion-icon name="play-sharp"></ion-icon>
                        </a>
                        <img src="{{ asset('images/s9.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-angle-left"></i>
                </button>
                <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-angle-right"></i></button>
            </div>
        </div>
        <!-- End Glider-->
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="mb-4" data-aos="fade-up">Por qué elegirnos</h3>
                    <div class="about-date d-flex">
                        <div class="item flex-fill" data-aos="flip-down">
                            <h3 class="mb-0">
                                <ion-icon name="ribbon-outline"></ion-icon>
                            </h3>
                            <h4 class="ml-2 mb-0">Educación Superior Certificada</h4>
                        </div>
                        <div class="item flex-fill" data-aos="flip-down">
                            <h3 class="mb-0">
                                <ion-icon name="star-outline"></ion-icon>
                            </h3>
                            <h4 class="ml-2 mb-0">13 años formando líderes</h4>
                        </div>
                        <div class="item flex-fill" data-aos="flip-down">
                            <h3 class="mb-0">
                                <ion-icon name="people-circle-outline"></ion-icon>
                            </h3>
                            <h4 class="ml-2 mb-0">Más de 1200 Egresados</h4>
                        </div>
                        <div class="item flex-fill" data-aos="flip-down">
                            <h3 class="mb-0">
                                <ion-icon name="business-outline"></ion-icon>
                            </h3>
                            <h4 class="ml-2 mb-0">Instalaciones modernas y de primer nivel</h4>
                        </div>
                        <div class="item flex-fill" data-aos="flip-down">
                            <h3 class="mb-0">
                                <ion-icon name="earth-outline"></ion-icon>
                            </h3>
                            <h4 class="ml-2 mb-0">Convenios Nacionales e Internacionales</h4>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    @include('home.slider-testimonios')

    @include('partials.form-inscripcion')

    <section class="news">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7" data-aos="fade-up">
                    <h5>Noticias</h5>
                    <h2>Conoce las noticias destacadas de la universidad</h2>
                </div>
                <div class="col-md-5" data-aos="fade-up">
                    <a href="{{ route('noticias') }}" class="btn btn-primary float-right">Ver más noticias</a>
                </div>
            </div>
            <div class="row">
                @foreach ($news as $key => $new)
                    @if ($loop->first)
                        <div class="col-md-6">
                            <div class="card" data-aos="fade-right">
                                <div class="card-img-top">
                                    <img src="{{ asset($new->image_path.$new->image_name) }}" class="img-fluid"
                                         alt="...">
                                </div>
                                <div class="card-body">
                                    <div class="fecha">
                                        <h3>@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%d", strtotime($new->datetime)) }}</h3>
                                        <p class="text-uppercase">@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%b", strtotime($new->datetime)) }}</p>
                                    </div>
                                    <h3 class="card-text my-5">{{ $new->title }}</h3>
                                    <a href="{{ route('noticias.detail', $new->id) }}" class="">Continuar leyendo</a>
                                </div>
                            </div>
                        </div>
                    @elseif ($key == 1)
                        <div class="col-md-6">
                            <div class="card" data-aos="fade-left">
                                <div class="card-img-top">
                                    <img src="{{ asset($new->image_path.$new->image_name) }}" class="img-fluid"
                                         alt="...">
                                </div>
                                <div class="card-body">
                                    <div class="fecha">
                                        <h3>@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%d", strtotime($new->datetime)) }}</h3>
                                        <p class="text-uppercase">@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%b", strtotime($new->datetime)) }}</p>
                                    </div>
                                    <h3 class="card-text my-5">{{ $new->title }}</h3>
                                    <a href="{{ route('noticias.detail', $new->id) }}" class="">Continuar leyendo</a>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-lg-4">
                            <div class="card" data-aos="fade-up">
                                <div class="card-body">
                                    <div class="fecha">
                                        <h3>@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%d", strtotime($new->datetime)) }}</h3>
                                        <p class="text-uppercase">@php(setlocale(LC_TIME,"es_PY.UTF-8")){{ strftime("%b", strtotime($new->datetime)) }}</p>
                                    </div>
                                    <h3 class="card-text my-5">{{ $new->title }}</h3>
                                    <a href="{{ route('noticias.detail', $new->id) }}" class="">Continuar leyendo</a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

                {{--<div class="col-lg-4">
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>
                            <h3 class="card-text my-5">Misa del mes de Noviembre</h3>
                            <a href="{{ route('news-detail-4') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">nov</p>
                            </div>
                            <h3 class="card-text my-5">Participación de Expo Carreras Virtual</h3>
                            <a href="{{ route('news-detail-2') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card" data-aos="fade-up">
                        <div class="card-body">
                            <div class="fecha">
                                <h3>5</h3>
                                <p class="text-uppercase">may</p>
                            </div>
                            <h3 class="card-text my-5">Ante la difusión de supuestas informaciones</h3>
                            <a href="{{ route('news-detail-5') }}" class="">Continuar leyendo</a>
                        </div>
                    </div>
                </div>--}}
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="ModalInstitucional" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/q40gxNoyHKE" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalBiblioteca" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/28I0s61X5BU" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalLaboratorio" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/C1jhoDeNwSc" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalClases" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/5neIK9qwD5U" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalSala" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/7egSzxO_s7A" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalSalaSi" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/WgsA4t2zUO4" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalSeguridad" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/Bh7V9Fz9WbM" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalCompromiso" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <video width="100%" height="700" src="/videos/compromiso.mp4" controls></video>
            </div>
        </div>
    </div>

    <style>

    </style>
    <div class="modal fade" id="ModalAnnouncement" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <img class="img-fluid" onclick="closeFormModal()" data-toggle="modal" data-target=".bd-example-modal-xl" src="/images/announcements/POP-UP_UMAX-01.gif" alt="">
                {{--<iframe width="100%" height="700" src="https://www.youtube.com/embed/28I0s61X5BU" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>--}}
            </div>
        </div>
    </div>

    {{--<a class="btn btn-primary my-auto" href="#" data-toggle="modal" data-target=".bd-example-modal-xl">Inscribirme</a>--}}


    {{-- <div class="modal fade" id="ModalInstitucional" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">¡SENTÍ EL PULSO UMAX!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">

            </div>
        </div>
        </div>
    </div> --}}

@endsection

@section('scripts')
    <script>
        /*$(document).ready(function (){
            $('#ModalAnnouncement').modal('show')
        })

        function closeFormModal() {
            $('#ModalAnnouncement').modal('hide')
        }*/
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });

        $("#ModalCompromiso").on('hidden.bs.modal', function (e) {
            $("#ModalCompromiso video").attr("src", $("#ModalCompromiso video").attr("src"));
        });
    </script>
@endsection

