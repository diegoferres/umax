<div class="slider-home">
    <div id="carouselHome" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach ($banners->sortBy('order') as $key => $banner)
                <li data-target="#carouselHome" data-slide-to="{{ $key }}" class="{{ $loop->first ? 'active' : ''}}"></li>
            @endforeach
                {{--<li data-target="#carouselHome" data-slide-to="1"></li>
                <li data-target="#carouselHome" data-slide-to="2"></li>--}}
        </ol>
        <div class="carousel-inner">
            @foreach ($banners->sortBy('order') as $banner)
                <div class="carousel-item {{ $loop->first ? 'active' : ''}}">
                    <img src="{{asset('storage/'.$banner->image_path)}}" class="d-block w-100" alt="...">
                    {{-- <div class="carousel-caption">
                        <h5 data-aos="fade-up" data-aos-duration="200">Conoce la nueva</h5>
                        <h1 data-aos="fade-up" data-aos-duration="300">Sala moderna de Biblioteca</h1>
                        <p class="my-4" data-aos="fade-up" data-aos-duration="400">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa</p>
                        <a href="#" class="btn btn-primary mt-4" data-aos="fade-up" data-aos-duration="500">Más información</a>
                    </div> --}}
                </div>
            @endforeach
            {{--<div class="carousel-item">
                <img src="{{asset('/images/1.jpg')}}" class="d-block w-100" alt="...">
                --}}{{-- <div class="carousel-caption">
                    <h5 data-aos="fade-up" data-aos-duration="200">Conoce la nueva</h5>
                    <h1 data-aos="fade-up" data-aos-duration="300">Sala moderna de Biblioteca</h1>
                    <p class="my-4" data-aos="fade-up" data-aos-duration="400">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa</p>
                    <a href="#" class="btn btn-primary mt-4" data-aos="fade-up" data-aos-duration="500">Más información</a>
                </div> --}}{{--
            </div>
            <div class="carousel-item">
                <img src="{{asset('/images/SLIDES_PAGINA-02.jpg')}}" class="d-block w-100" alt="...">
                --}}{{-- <div class="carousel-caption">
                    <h5 data-aos="fade-up" data-aos-duration="200">Conoce la nueva</h5>
                    <h1 data-aos="fade-up" data-aos-duration="300">Sala moderna de Biblioteca</h1>
                    <p class="my-4" data-aos="fade-up" data-aos-duration="400">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa</p>
                    <a href="#" class="btn btn-primary mt-4" data-aos="fade-up" data-aos-duration="500">Más información</a>
                </div> --}}{{--
            </div>--}}
            {{--<div class="carousel-item">
                <img src="{{asset('/images/3.jpg')}}" class="d-block w-100" alt="...">
                --}}{{-- <div class="carousel-caption">
                    <h5 data-aos="fade-up" data-aos-duration="200">Conoce la nueva</h5>
                    <h1 data-aos="fade-up" data-aos-duration="300">Sala moderna de Biblioteca</h1>
                    <p class="my-4" data-aos="fade-up" data-aos-duration="400">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa</p>
                    <a href="#" class="btn btn-primary mt-4" data-aos="fade-up" data-aos-duration="500">Más información</a>
                </div> --}}{{--
            </div>--}}
        </div>
        <a class="carousel-control-prev ml-3" href="#carouselHome" role="button" data-slide="prev">
            <ion-icon name="chevron-back-outline"></ion-icon>
        </a>
        <a class="carousel-control-next" href="#carouselHome" role="button" data-slide="next">
            <ion-icon name="chevron-forward-outline"></ion-icon>
        </a>
    </div>
</div>
