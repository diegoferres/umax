<div class="testimonios my-5">
    <div class="container">
        <div id="carouselTestimonios" class="carousel slide" data-ride="carousel">
            <h3 data-aos="fade-right">Testimonios <br>de nuestros alumnos</h3>
            <a class="carousel-control-prev" href="#carouselTestimonios" role="button" data-slide="prev" data-aos="fade-left">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
            <a class="carousel-control-next" href="#carouselTestimonios" role="button" data-slide="next" data-aos="fade-left">
                <ion-icon name="chevron-forward-outline"></ion-icon>
            </a>
            <ol class="carousel-indicators">
                <li data-target="#carouselTestimonios" data-slide-to="0" class="active"></li>
                <li data-target="#carouselTestimonios" data-slide-to="1"></li>
                <li data-target="#carouselTestimonios" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" data-aos="fade-up">
                <div class="carousel-item active">
                    <div class="profile">
                        <div class="d-flex align-items-center">
                            <div class="picture">
                                <img src="{{asset('/images/est-ruben.png')}}" alt="" class="img-fluid">
                            </div>
                            <div class="date">
                                <h4 class="mb-0"><b>Rubens Rodríguez</b></h4>
                                <small>Tercer Semestre Chile</small>
                            </div>
                        </div>
                        <div class="caption">
                            <p>Estoy contento y agradecido por el calor humano de todos los que componen el equipo UMAX, desde el personal administrativo, doctores, docentes y autoridades.
                                <br>
                                A mis 38 años nunca pensé estar lejos de mi país y puedo decir que nunca es tarde para aprender. 
                                <br>
                                Estoy orgulloso de representar a la Universidad en el ámbito estudiantil en una hermosa carrera que la de servir a los demás.
                                Destaco el hecho de que se hace mucho trabajo en la comunidad y eso engrandece tanto a la persona y a la Universidad como tal. </p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="profile">
                        <div class="d-flex align-items-center">
                            <div class="picture">
                                <img src="{{asset('/images/est-aline.png')}}" alt="" class="img-fluid">
                            </div>
                            <div class="date">
                                <h4 class="mb-0"><b>Alice Fogaça</b></h4>
                                <small>6to Semestre Grupo A Brasil</small>
                            </div>
                        </div>
                        <div class="caption">
                            <p>Soy muy agradecida por poder estudiar en la Universidad María Auxiliadora, en ella deposito mis sueños de pequeña de ser médica. 
                                La carrera es larga, hay que tener dedicación tanto de mi parte como del conjunto UMAX. También hay obstáculos, pero de todos ellos se puede aprender a ser mejores. <br>
                                Confío además que en estos momentos de tanta dificultad con una pandemia, de que juntos saldremos adelante y seremos más fuertes</p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="profile">
                        <div class="d-flex align-items-center">
                            <div class="picture">
                                <img src="{{asset('/images/est-chelsea.png')}}" alt="" class="img-fluid">
                            </div>
                            <div class="date">
                                <h4 class="mb-0"><b>Chelsea Servin</b></h4>
                                <small>Estudiante de Medicina</small>
                            </div>
                        </div>
                        <div class="caption">
                            <p>Soy Chelsea Servín, alumna de la carrera de Medicina. <br> 
                                En la Universidad María Auxiliadora encontré mi determinación y voluntad en la medicina, dando pasos pequeños pero que son considerados por mi como enormes. <br> 
                                Me han y siguen proporcionando los materiales precisos para llevar a cabo el trayecto y lograr ser profesional.
                                Desde el principio y en todo momento he disfrutado y aprovechado al máximo la capacidad de obtener conocimientos imprescindibles que me llevarán a alcanzar la finalidad y satisfacción de ser médico en un futuro cercano, y aún más por las elecciones que me llevaron a estar en el lugar indicado. <br>
                                Considero que nuestra faceta como estudiantes y futuros profesionales es muy importante, debemos abrazar la carrera médica con mucha responsabilidad y vocación, por ende digo que la faceta humana de la medicina es para mí la motivación número uno. <br>
                                <b>“La medicina es para aquellos que no se pueden imaginar haciendo otra cosa.” Dra. Luanda Gazzete.</b>
                            </p>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>