<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>#SOMOSUMAX</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('assets/icon/favicon.png') }}" rel="icon">
    <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

    <!-- WhatsApp Float Icon -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <a href="https://wa.me/595983746014" class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>

    <style>
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:70px;
            right:8px;
            background-color:#25d366;
            color:#FFF;
            border-radius:50px;
            text-align:center;
            font-size:30px;
            z-index:100;
        }

        .my-float{
            margin-top:16px;
        }
    </style>
    <!-- End WhatsApp Float Icon -->

    <!-- =======================================================
    * Template Name: Dewi - v4.7.0
    * Template URL: https://bootstrapmade.com/dewi-free-multi-purpose-html-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
    <div style="height: 65px;" class="container d-flex align-items-center justify-content-end">


        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        <nav id="navbar" class="navbar">
            <ul>
                <li style="border-right: 1px solid;"><a class="nav-link scrollto active" href="#">INÍCIO</a></li>
                <li style="border-right: 1px solid;"><a class="nav-link scrollto" href="#vantagens">VANTAGEM</a></li>
                <li ><a class="nav-link scrollto" href="#footer">CONTATO</a></li>
                <li style="width: 161px;"><a class="nav-link scrollto " style="    border: 1px solid;
            padding: 10px;
            border-radius: 25px;
            height: 28px;
            left:18px" href="{{ route('ventajas.medicina.index') }}">TRADUZIR</a> <span><img style="    position: absolute;
    bottom: -11px;
    width: 64px;
    right: -34px;" src="{{ asset('assets/icon/icon-paraguay.svg') }}" alt=""></span></li>

            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->


    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero">
    <div class="hero-container" data-aos="fade-up" data-aos-delay="150">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div>
                    <img src="{{ asset('assets/img/umax_logo.png') }}" class="img-fluid" alt="">
                </div>
                <br>
                <h1>A PRIMEIRA<br>E A ÚNICA</h1>
                <h2>Universidade privada do
                    Paraguai com<br>curso de Medicina
                    acreditada<br>para MERCOSUL</h2>
                <div class="hashtag">
                    <h2>#<span>SOMOS</span>UMAX</h2>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                <img src="{{ asset('assets/img/umax_certificado_pt.png') }}" class="img-fluid" alt="">
            </div>
        </div>

    </div>
</section><!-- End Hero -->

<main id="main">
<div style="max-width: 100%;">
    <div class="row stripe">
        <div class="col col-one" data-aos="fade-up" data-aos-delay="100">
            <div style="align-self: center;">

                CURSO DE<br>MEDICINA UMAX
            </div>
        </div>
        <div class="col col-two" data-aos="fade-up" data-aos-delay="150">
            <div class="row">
                <div class="col-8" style="align-self: center;">

                    ACREDITAÇÃO<br>INTERNACIONAL
                </div>
                <div class="col-4">
                    <img src="{{ asset('assets/icon/icon-certificado.svg') }}" alt="" class="img-fluid" width="50px">
                </div>
            </div>


        </div>
        <div class="col col-three" data-aos="fade-up" data-aos-delay="200">
            <div class="row">
                <div class="col-8" style="align-self: center;">
                    ACREDITAÇÃO<br>NACIONAL
                </div>
                <div class="col-4">
                    <img src="{{ asset('assets/icon/icon-certificado.svg') }}" alt="" class="img-fluid" width="50px">
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- ======= About Boxes Section ======= -->
    <section id="vantagens" class="about-boxes">
        <div class="container" data-aos="fade-up">

            <h2>Vantagens exclusivas do curso de Medicina<br>
                com acreditação do Mercosul</h2>

            <div class="row" style="padding-top: 50px;">
                <div class="col-sm-5">
                    <ul>
                        <li>Facilita o acesso ao Prontuário Médico (CRM), sem exame.</li>
                        <li>Permite revalidação simplificada em Universidades Públicas Brasileiras.</li>
                        <li>Facilita o acesso à bolsa universitária para brasileiros.</li>

                    </ul>
                </div>
                <div class="col-sm-2"></div>
                <div class="col-sm-5">
                    <ul>
                        <li>Possibilita mobilidade estudantil entre Universidades brasileiras e demais países do Mercosul.</li>
                        <li>Permite inscrições para Pós-Graduação e Residência no Brasil e demais países do Mercosul.</li>
                        <li>Facilita a inscrição para pós-graduação e residências na América e na Europa.</li>
                        <li>Reconhecimento internacional de qualidade acadêmica.</li>

                    </ul>
                </div>
            </div>
        </div>


    </section><!-- End About Boxes Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
        <div class="container" data-aos="zoom-in">
            <h2>Quais países compõem?</h2>
            <div class="row" style="justify-content: center;">

                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                    <img src="{{ asset('assets/icon/icon-brasil.svg') }}" class="img-fluid" alt="">
                </div>

                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                    <img src="{{ asset('assets/icon/icon-argentina.svg') }}" class="img-fluid" alt="">
                </div>

                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                    <img src="{{ asset('assets/icon/icon-uruguay.svg') }}" class="img-fluid" alt="">
                </div>

                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                    <img src="{{ asset('assets/icon/icon-bolivia.svg') }}" class="img-fluid" alt="">
                </div>

                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                    <img src="{{ asset('assets/icon/icon-chile.svg') }}" class="img-fluid" alt="">
                </div>

            </div>

        </div>
    </section><!-- End Clients Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about" style="background: #000;">

        <div class="container" data-aos="fade-up">


            <div class="row">

                <div class="col-lg-8 video-box align-self-baseline" style="margin: auto;" data-aos="zoom-in" data-aos-delay="100">
                    <img src="{{ asset('assets/img/umax_bg_1.jpeg') }}" class="img-fluid" alt="">
                    <a href="https://youtu.be/2UDrw9eMqVQ" class="glightbox play-btn mb-4"></a>
                </div>



            </div>

        </div>
    </section><!-- End About Section -->


</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
        <div class="container" style="padding-top: 35px;">
            <div class="row">

                <div class="col-lg-4 col-md-6">
                    <div class="footer-info" style="border-right: 3px solid #952020;">
                        <h2>Contatos</h2>
                        <p>+595 21 296 900</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="footer-info" style="border-right: 3px solid #952020;">
                        <h2>Saiba mais sobre nós</h2>
                        <p><a href="https://www.umax.edu.py/" target="_blank">www.umax.com.py</a></p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 ">
                    <div class="footer-info">
                        <div class="row">
                            <div class="col-sm-4">
                                <h2 style="font-size: 27px;">Siga-nos</h2>

                            </div>
                            <div class="col-sm-8">
                                <p style="font-size:12px">Universidad María<br>
                                    Auxiliadora - UMAX</p>

                            </div>
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="https://www.instagram.com/umaxparaguay/" target="_blank"><img src="{{ asset('assets/icon/icon-insta.svg') }}" class="img-social-insta" alt=""></a></li>
                                <li><a href="https://www.facebook.com/UMAXPAR/"><img src="{{ asset('assets/icon/icon-face.svg') }}" class="img-social-face" alt=""></a></li>
                                <li><a href="https://twitter.com/UMAXPY"><img src="{{ asset('assets/icon/icon-twit.svg') }}" class="img-social-twit" alt=""></a></li>
                                <li><a href="https://www.linkedin.com/company/37796802/admin/"><img src="{{ asset('assets/icon/icon-in.svg') }}" class="img-social-in" alt=""></a></li>
                                <li><a href="https://www.youtube.com/channel/UCet3SULPyR8rw178xoY5v0g"><img src="{{ asset('assets/icon/icon-you.svg') }}" class="img-social-you" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>


</footer><!-- End Footer -->

<div id="preloader"></div>
<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="{{ asset('assets/vendor/purecounter/purecounter.js') }}"></script>
<script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
<script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('assets/js/main.js') }}"></script>





</body>

</html>
