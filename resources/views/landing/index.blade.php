<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>#SOMOSUMAX</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="/assets/icon/favicon.png" rel="icon">
    <link href="/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="/assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="/assets/css/style.css" rel="stylesheet">

    <!-- WhatsApp Float Icon -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <a href="https://wa.me/595983746014" class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>

    <style>
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:70px;
            right:8px;
            background-color:#25d366;
            color:#FFF;
            border-radius:50px;
            text-align:center;
            font-size:30px;
            z-index:100;
        }

        .my-float{
            margin-top:16px;
        }
    </style>
    <!-- End WhatsApp Float Icon -->

    <!-- =======================================================
    * Template Name: Dewi - v4.7.0
    * Template URL: https://bootstrapmade.com/dewi-free-multi-purpose-html-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>
<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
    <div style="    height: 65px;" class="container d-flex align-items-center justify-content-end">


        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        <nav id="navbar" class="navbar">
            <ul>
                <li style="border-right: 1px solid;"><a class="nav-link scrollto active" href="#">INICIO</a></li>
                <li style="border-right: 1px solid;"><a class="nav-link scrollto" href="#ventajas">VENTAJAS</a></li>
                <li ><a class="nav-link scrollto" href="#footer">CONTACTOS</a></li>
                <li style="width: 161px;"><a class="nav-link scrollto " style="    border: 1px solid;
            padding: 10px;
            border-radius: 25px;
            height: 28px;
            left:18px" href="{{ route('ventajas.medicina.portugues') }}">TRADUCIR</a> <span><img style="    position: absolute;
    bottom: -11px;
    width: 64px;
    right: -34px;" src="/assets/icon/icon-brasil.svg" alt=""></span></li>

            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->


    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero">
    <div class="hero-container" data-aos="fade-up" data-aos-delay="150">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div>
                    <img src="/assets/img/umax_logo.png" class="img-fluid" alt="">
                </div>
                <br>
                <h1>LA PRIMERA<br>Y LA ÚNICA</h1>
                <h2>Universidad Privada del Paraguay con<br>
                    carrera de Medicina acreditada para<br>
                    el MERCOSUR.</h2>
                <div class="hashtag">
                    <h2>#<span>SOMOS</span>UMAX</h2>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                <img src="/assets/img/umax_certificado.png"class="img-fluid" alt="">
            </div>
        </div>

    </div>
</section><!-- End Hero -->

<main id="main">
    <div style="max-width: 100%;">
    <div class="row stripe">
        <div class="col col-one" data-aos="fade-up" data-aos-delay="100">
            <div style="align-self: center;">

                CARRERA DE<br>MEDICINA UMAX
            </div>
        </div>
        <div class="col col-two" data-aos="fade-up" data-aos-delay="150">
            <div class="row">
                <div class="col-8" style="align-self: center;">

                    ACREDITACIÓN<br>INTERNACIONAL
                </div>
                <div class="col-4">
                    <img src="/assets/icon/icon-certificado.svg" alt="" class="img-fluid" width="50px">
                </div>
            </div>


        </div>
        <div class="col col-three" data-aos="fade-up" data-aos-delay="200">
            <div class="row">
                <div class="col-8" style="align-self: center;">
                    ACREDITACIÓN<br>NACIONAL
                </div>
                <div class="col-4">
                    <img src="/assets/icon/icon-certificado.svg" alt="" class="img-fluid" width="50px">
                </div>
            </div>
        </div>
    </div>
    </div>

    <!-- ======= About Boxes Section ======= -->
    <section id="ventajas" class="about-boxes">
        <div class="container" data-aos="fade-up">

            <h2>Ventajas exclusivas de la carrera de Medicina<br>
                con acreditación del MERCOSUR</h2>

            <div class="row" style="padding-top: 50px;">
                <div class="col-sm-5">
                    <ul>
                        <li>Posibilita movilidad estudiantil entre países del MERCOSUR.</li>
                        <li>Permite la inscripción a Posgrados y Residencias en los países del MERCOSUR.</li>

                    </ul>
                </div>
                <div class="col-sm-2"></div>
                <div class="col-sm-5">
                    <ul>
                        <li>Facilita la inscripción a Posgrados y Residencias en América y Europa.</li>
                        <li>Reconocimiento internacional de calidad académica.</li>
                        <li>Amplia oferta laboral en todos los países del MERCOSUR.</li>

                    </ul>
                </div>
            </div>
        </div>


    </section><!-- End About Boxes Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
        <div class="container" data-aos="zoom-in">
            <h2>¿Qué países integran?</h2>
            <div class="row" style="justify-content: center;">

                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                    <img src="/assets/icon/icon-brasil.svg" class="img-fluid" alt="">
                </div>

                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                    <img src="/assets/icon/icon-argentina.svg" class="img-fluid" alt="">
                </div>

                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                    <img src="/assets/icon/icon-uruguay.svg" class="img-fluid" alt="">
                </div>

                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                    <img src="/assets/icon/icon-bolivia.svg" class="img-fluid" alt="">
                </div>

                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                    <img src="/assets/icon/icon-chile.svg" class="img-fluid" alt="">
                </div>

            </div>

        </div>
    </section><!-- End Clients Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about" style="background: #000;">

        <div class="container" data-aos="fade-up">


            <div class="row">

                <div class="col-lg-8 video-box align-self-baseline" style="margin: auto;" data-aos="zoom-in" data-aos-delay="100">
                    <img src="{{ asset('assets/img/umax_bg_1.jpeg') }}" class="img-fluid" alt="">
                    <a href="https://youtu.be/2k-dpVbI5zo" class="glightbox play-btn mb-4"></a>
                </div>



            </div>

        </div>
    </section><!-- End About Section -->


</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
        <div class="container" style="padding-top: 35px;">
            <div class="row">

                <div class="col-lg-4 col-md-6">
                    <div class="footer-info" style="border-right: 3px solid #952020;">
                        <h2>Contactos</h2>
                        <p>(021) 296 900</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="footer-info" style="border-right: 3px solid #952020;">
                        <h2>Conocenos más</h2>
                        <p><a href="https://www.umax.edu.py/" target="_blank">www.umax.com.py</a></p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 ">
                    <div class="footer-info">
                        <div class="row">
                            <div class="col-sm-4">
                                <h2>Seguinos</h2>

                            </div>
                            <div class="col-sm-8">
                                <p style="font-size:12px">Universidad María<br>
                                    Auxiliadora - UMAX</p>

                            </div>
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="https://www.instagram.com/umaxparaguay/" target="_blank"><img src="/assets/icon/icon-insta.svg" class="img-social-insta" alt=""></a></li>
                                <li><a href="https://www.facebook.com/UMAXPAR/"><img src="/assets/icon/icon-face.svg" class="img-social-face" alt=""></a></li>
                                <li><a href="https://twitter.com/UMAXPY"><img src="/assets/icon/icon-twit.svg" class="img-social-twit" alt=""></a></li>
                                <li><a href="https://www.linkedin.com/company/37796802/admin/"><img src="/assets/icon/icon-in.svg" class="img-social-in" alt=""></a></li>
                                <li><a href="https://www.youtube.com/channel/UCet3SULPyR8rw178xoY5v0g"><img src="/assets/icon/icon-you.svg" class="img-social-you" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>


</footer><!-- End Footer -->

<div id="preloader"></div>
<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="/assets/vendor/purecounter/purecounter.js"></script>
<script src="/assets/vendor/aos/aos.js"></script>
<script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="/assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="/assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="/assets/js/main.js"></script>

<!-- Code for Action: Umax_Aprendizaje -->
<!-- Code for Pixel: Umax_Aprendizaje -->
<!-- Begin DSP Conversion Action Tracking Code Version 9 -->
<script type='text/javascript'>
    (function() {
        var w = window, d = document;
        var s = d.createElement('script');
        s.setAttribute('async', 'true');
        s.setAttribute('type', 'text/javascript');
        s.setAttribute('src', '//c1.rfihub.net/js/tc.min.js');
        var f = d.getElementsByTagName('script')[0];
        f.parentNode.insertBefore(s, f);
        if (typeof w['_rfi'] !== 'function') {
            w['_rfi']=function() {
                w['_rfi'].commands = w['_rfi'].commands || [];
                w['_rfi'].commands.push(arguments);
            };
        }
        _rfi('setArgs', 'ver', '9');
        _rfi('setArgs', 'rb', '45540');
        _rfi('setArgs', 'ca', '20837596');
        _rfi('setArgs', '_o', '45540');
        _rfi('setArgs', '_t', '20837596');
        _rfi('track');
    })();
</script>
<noscript>
    <iframe src='//20837596p.rfihub.com/ca.html?rb=45540&ca=20837596&_o=45540&_t=20837596&ra={{ rand(1,1000) }}' style='display:none;padding:0;margin:0' width='0' height='0'>
    </iframe>
</noscript>



</body>

</html>
