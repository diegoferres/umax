@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Revista Científica UMAX</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Sobre la Revista</h3>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                    La <b>Revista Científica UMAX</b> es el órgano oficial de la Universidad María Auxiliadora para la
                    difusión de la producción científica, disponible para la comunidad académica, cientifica y
                    profesional; tanto nacional como internacional.
                    <br>
                    <br>
                    A traves de la <b>Revista Científica UMAX</b>, publicación semestral; se pretende proporcionar un
                    espacio a los cientìficos, docentes y jovenes investigadores y profesionales de publicar su
                    producción en alguna de las modalidades disponibles en nuestra Revista; desde Artículos
                    Originales, Artículos Originales Breves; Artículos de Revisión, Reporte de Casos Clínicos,
                    Cartas la Editor y un espacio destinado a los Informes de Innovación y Transferencia
                    Tecnológica, de manera que todos los científicos, investigadores e innovadores encuentren en
                    la Revista el espacio que buscaban; de calidad y de impacto con alto rigor científico-ético
                    <br>
                    <br>
                    La revista se caracteriza por ser multidisciplinaria con un fuerte énfasis en las Ciencias de la
                    Salud; Ciencias Sociales y Humanidades y otras de interés para la revista y la comunidad
                    académica, profesional según el avance de la misma ciencia.
                </p>
            </div>
            <h3 data-aos="fade-up" class="mt-5">Número Actual</h3>
            <h4 data-aos="fade-up">Vol. 1. Número 1 (2021). Revista Científica UMAX</h4>
            <div class="row my-5">
                <div class="col-12 col-lg-5 pr-lg-0" data-aos="fade-right">
                    <img src="{{ asset('images/portada-revista.jpg') }}" alt="" class="img-fluid">
                </div>
                <div class="col-12 col-lg-7 px-lg-0" data-aos="fade-left" style="background-color: #F6F6FA;">
                    <div class="text my-auto">
                        <h4 class="mb-4">PUBLICADO: 18-03-2021</h4>
                        <h4>Editorial</h4>
                        <p>Prof. Dr. Hernando Javier Quiñonez Sarabia</p>
                        <a href="{{ asset('records/Editorial-Revista-UMAX.pdf')}}" class="btn btn-primary" target="_blank"><ion-icon name="document-text-outline" class="mr-2"></ion-icon> PDF</a>
                    </div>
                </div>
            </div>
            <h3 data-aos="fade-up" class="mt-5 mb-4">Artículos Científicos</h3>
            <div class="d-flex flex-column">
                <p><b>Artículos Originales</b></p>
                <a style="color: black" href="{{ asset('records/articulos-originales/Caracterización clínica de mujeres con síndrome hipertensivo gestacional.pdf')}}" class="mb-3" target="_blank">Caracterización clínica de mujeres con síndrome hipertensivo gestacional del Servicio de Ginecoobstetricia del Hospital Materno Infantil de Loma Pyta de diciembre</a>
                <a style="color: black" href="{{ asset('records/articulos-originales/Fibrosis quística en niños menores de cinco años. Hospital Materno Infantil de Loma Pyta, 2012 a 2017..pdf')}}" class="mb-3" target="_blank">Fibrosis quística en niños menores de cinco años. Hospital Materno Infantil de Loma Pyta, 2012 a 2017.</a>
                <a style="color: black" href="{{ asset('records/articulos-originales/Frecuencia de enfermedades de transmisión sexual en jóvenes.pdf')}}" class="mb-3" target="_blank">Frecuencia de enfermedades de transmisión sexual en jóvenes que consultaron en el Hospital Distrital de San Ignacio, Misiones en el periodo de  2014 a 2018.</a>
                <a style="color: black" href="{{ asset('records/articulos-originales/Hormonas tiroideas en el embarazo de adolescente. Hospital Regional de Caacupé del año 2017.pdf')}}" class="mb-3" target="_blank">Hormonas tiroideas en el embarazo de adolescente. Hospital Regional de Caacupé del año 2017</a>
                <a style="color: black" href="{{ asset('records/articulos-originales/Sedentarismo en adultos mayores residentes en hogares de ancianos de la zona periurbana de asunción.pdf')}}" class="mb-3" target="_blank">Sedentarismo en adultos mayores residentes en hogares de ancianos de la zona periurbana de asunción</a>
                <p><b>Artículos Breves</b></p>
                <a style="color: black" href="{{ asset('records/articulos-breves/Caracterización de pacientes con síndrome hipertensivo que concurrieron al Hospital Regional de Caacupé durante el período de enero a marzo de 2019..pdf')}}" class="mb-3" target="_blank">Caracterización de pacientes con síndrome hipertensivo que concurrieron al Hospital Regional de Caacupé durante el período de enero a marzo de 2019.</a>
                <a style="color: black" href="{{ asset('records/articulos-breves/Conocimientos, percepciones y creencias sobre las hepatitis a, b y c.pdf')}}" class="mb-3" target="_blank">Conocimientos, percepciones y creencias sobre las hepatitis a, b y c en los estudiantes de medicina de la Universidad Nacional de Asunción, Paraguay, 2019</a>
                <p><b>Carta al Editor</b></p>
                <a style="color: black" href="{{ asset('records/carta-editor/Caracterización de la investigación científica en facultades de medicina del Paraguay.pdf')}}" class="mb-3" target="_blank">Caracterización de la investigación científica en facultades de medicina del Paraguay</a>
                <a style="color: black" href="{{ asset('records/carta-editor/Fisioterapia en salud mental abordaje integral.pdf')}}" class="mb-3" target="_blank">Fisioterapia en salud mental abordaje integral</a>
            </div>
            <a href="{{ route('envios') }}" class="btn btn-primary mt-4">Enviar un artículo</a>

            <div class="mt-5">
                <p>Revista Científica UMAX</p>
                <div class="d-flex align-items-center mb-3">
                    <img src="{{ asset('images/cc.png') }}" alt="CC" width="50">
                    <p class="ml-2 mb-0">Todo contenido de esta revista está bajo Licencia.</p>
                </div>
                {{--<p>ISSN (impreso) 111111</p>--}}
                <p>ISSN (digital) 2710-4869</p>
            </div>
        </div>
    </section>

@endsection
