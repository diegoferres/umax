@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Extensión Universitaria</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Justificación y Objetivos</h3>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                    La labor de la Extensión Universitaria como la función que, articulada a la docencia e investigación, promueve la interacción permanente con su propia comunidad universitaria y con los demás actores de la sociedad. 
                    <br><br>
                    Aportando desde su quehacer con el conocimiento y desde los principios y valores que inspiran su labor educativa, propuestas y solución a las necesidades y problemas que enfrenta la sociedad.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Justificación</h4>
                <p data-aos="fade-up" data-aos-duration="800">La extensión universitaria, forma parte integral de su tarea educativa, y que es fundamento de su sentido y propósito. La institución procura que profesores, alumnos y demás miembros del claustro universitario se comprometan libremente, en unidad de vida, con coherencia de pensamiento, palabra y acción, a buscar, descubrir, comunicar y conservar la verdad, en todos los campos del conocimiento, con fundamento en una concepción cristiana del hombre y del mundo, como contribución al progreso de la sociedad.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Objetivos</h4>
                <p data-aos="fade-up" data-aos-duration="800"><ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Promover la participación de los estudiantes universitarios a fin de inculcar la responsabilidad social. 
                    <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Fortalecer y promocionar las actividades universitarias en los sectores sociales y productivos con el fin de lograr un mayor impacto, así como también recibir información que retroalimente los proyectos y programas de vinculación con la finalidad de adecuarlos a las necesidades de los sectores sociales y de desarrollo universitario. 
                    <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Fomentar la formación de grupos multidisciplinarios para la ejecución del proyecto de extensión. Delinear objetivos y metas que produzcan impactos sociales destinados a satisfacer las necesidades de la comunidad y a mejorar sus condiciones de vida. 
                    <br><br>
                    <ion-icon name="chevron-forward-outline" class="text-primary"></ion-icon> Organizar e impulsar el desarrollo de las actividades culturales, artísticas, y deportivas, así como de obtener recursos para las actividades de extensión.
                </p>
            </div>
        </div>
        <div class="videos">
            <div class="container">
                <h3 class="mb-4" data-aos="fade-up">Videos</h3>
                <div class="row">
                    <div class="col-md-4">
                        <div class="image-prev" data-aos="fade-left">
                            <h4>Extensión Universitaria UMAX</h4>
                            <a href="#" class="btn btn-play" data-toggle="modal" data-target="#ModalExtension"><ion-icon name="play-sharp"></ion-icon></a>
                            <img src="{{ asset('images/e-prev-3.png') }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="image-prev" data-aos="fade-left">
                            <h4>El rostro humano de la Medicina</h4>
                            <a href="#" class="btn btn-play" data-toggle="modal" data-target="#ModalMedicina"><ion-icon name="play-sharp"></ion-icon></a>
                            <img src="{{ asset('images/e-prev-2.png') }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="image-prev" data-aos="fade-left">
                            <h4>Proyecto Paraguay</h4>
                            <a href="#" class="btn btn-play" data-toggle="modal" data-target="#ModalProyecto"><ion-icon name="play-sharp"></ion-icon></a>
                            <img src="{{ asset('images/e-prev-1.png') }}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fotos pt-5">
            <div class="container">
                <h3 class="mt-5 py-5" data-aos="fade-up">Fotos</h3>
            </div>
            <!-- Glider-->
            <div class="glider-contain mb-5">
                <div class="glider">
                    <div class="ml-0" data-aos="fade-left">
                        <img src="{{ asset('images/extension-1.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/extension-2.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/extension-3.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/extension-4.png') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/extension-5.png') }}" class="d-block w-100" alt="IMG">
                    </div>{{-- 
                    <div class="ml-1">
                        <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1">
                        <img src="{{ asset('images/biblioteca.jpg') }}" class="d-block w-100" alt="IMG">
                    </div> --}}
                </div>
                <div class="d-flex justify-content-center">
                    <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-angle-left"></i></button>
                    <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-angle-right"></i></button>
                </div>
            </div>
            <!-- End Glider-->
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="ModalExtension" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/wXR6NTgyeNk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalMedicina" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/M711BrIu5xQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalProyecto" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <iframe width="100%" height="700" src="https://www.youtube.com/embed/tNGs9vgjTtc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    @section('scripts')
        <script>
            new Glider(document.querySelector('.glider'), {
                slidesToShow: 3.5,
                dots: '#scrollLockDelay',
                draggable: true,
                arrows: {
                    next: '.glider-next',
                    prev: '.glider-prev'
                }
            });
        </script>
    @endsection

@endsection