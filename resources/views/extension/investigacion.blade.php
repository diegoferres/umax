@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Investigación</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Importancia de la Investigación</h3>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                    La Universidad María Auxiliadora entiende que la investigación es trascendental en las actividades
                    académicas. Pues la importancia de la investigación radica en la capacidad que tienen las ciencias
                    de contribuir en la construcción de la sociedad y en la dignificación humana por medio de su labor
                    investigativa y de sus constantes descubrimientos. La investigación, por tanto, tiene su lugar
                    privilegiado dentro de la comunidad educativa de la Universidad María Auxiliadora, pues por medio de
                    esta comunidad educativa busca constantemente el descubrimiento de algo nuevo en el ámbito
                    científico y, por ende, la construcción de una sociedad pensante, crítica y proactiva.
                    <br><br>
                </p>
                <div class="autoridades mt-5">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="card" data-aos="flip-left">
                                <div class="avatar">
                                    <img src="{{ asset('images/dra-isabel.png') }}" alt="" class="img-fluid">
                                </div>
                                <div class="caption">
                                    <h4>María Isabel Rodriguez Riveros.</h4>
                                    <h6>Directora de Investigación</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-9 mt-5">
                            <h4 class="mb-4">Formación Académica - Títulos</h4>
                            <p>Doctora en Educación, (Facultad de Filosofía UNA, becaria del Concejo Nacional de
                                Ciencia y Tecnología (CONACYT) 2016-2019
                                <br><br>
                                Mgter. en Ciencias de la Educación con Especialidad en Investigación Socio Educativa
                                (Facultad de Filosofía UNA) 2009
                                <br><br>
                                Especialista en Metodología de la Investigación (Instituto de Investigaciones en
                                Ciencias de
                                la Salud – UNA) 2013
                                <br><br>
                                Post Grado en Didáctica Universitaria (Rectorado - UNA) 1996
                                <br><br>
                                Licenciada en Enfermería – Facultad de Enfermería y Obstetricia UNA, 1994
                                Docente Investigadora Categorizada en el Programa Nacional de Incentivos a
                                Investigadores (PRONII) Nivel I desde el 2014 hasta la fecha
                                <br><br>
                                Autora y Co-autora de Artículos Originales publicados en Revistas Arbitradas indexadas a
                                nivel nacional e Internacional
                                <br><br>
                                Participación activa en Congresos, Jornadas y capacitaciones Científicas nacionales e
                                internacionales
                                <br><br>
                                Evaluadora de Artículos Originales de Revistas Científicas Nacionales Indexadas
                                <br><br>
                                Actualmente Directora de la Dirección de Investigación e Innovación de la UMAX</p>
                        </div>
                    </div>
                </div>
                <h4 class="mt-5" data-aos="fade-up">Misión</h4>
                <p data-aos="fade-up" data-aos-duration="800">“Generar, promover, difundir y preservar el desarrollo de
                    investigaciones de calidad, formando docentes y estudiantes investigadores y generando proyectos que
                    respondan a los avances de la ciencia y tecnología y en respuesta a las necesidades sociales”.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Visión</h4>
                <p data-aos="fade-up" data-aos-duration="800">“Ser una dirección líder en investigación, que promueva
                    enfoques integrales que puedan incidir en la construcción de una sociedad nacional e internacional
                    eficiente, justa y sustentable basada en valores éticos”.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Objetivos</h4>
                <p data-aos="fade-up" data-aos-duration="800">Fomentar y fortalecer el área con el desarrollo de
                    investigaciones de calidad que generen conocimientos que impacten en la formación integral de los
                    estudiantes, egresados, docentes e investigadores involucrados y retribuyan a la sociedad el aporte
                    dado.
                </p>
                <h4 class="mt-5" data-aos="fade-up">Lineas de Investigación</h4>
                <h5>ANÁLISIS DE SITUACIÓN DE SALUD</h5>

                <li>Caracterización sociodemográfica, situación sociohistórica y cultural de la población</li>
                <li>Identificación de los riesgos a nivel comunitario, familiar e individual.</li>
                <li>Situación de salud de la comunidad</li>
                <li>Descripción de los servicios de salud existentes y análisis de las acciones de salud
                    realizadas.
                </li>
                <li>Descripción y análisis de los daños y problemas de salud de la población.</li>
                <li>Análisis de la participación de la población en la identificación y solución de los problemas de
                    salud.
                </li>
                <li>Análisis de la intersectorialidad en la gestión de salud en la comunidad.</li>
                <li>Vigilancia y control de enfermedades transmisibles y no transmisibles</li>
                <li>Sistema de georreferencia: Análisis georreferenciado de la distribución de enfermedades</li>
                <br>

                <h5>SALUD DE LA INFANCIA Y ADOLESCENCIA</h5>
                <li>Mortalidad y la morbilidad infantiles (0 a 19 años)</li>
                <li>Crecimiento y desarrollo integral de niños, niñas y adolescentes</li>
                <li>Cobertura de control neonatal y primera infancia</li>
                <li>Patologías Pediátricas</li>
                <li>Prevención, diagnóstico y tratamiento las ITS</li>
                <li>Planificación familiar</li>
                <br>

                <h5>SALUD SEXUAL Y REPRODUCTIVA</h5>
                <li>Educación sexual integral</li>
                <li>Planificación familiar</li>
                <li>Atención prenatal y el parto sin riesgo, la atención posnatal,</li>
                <li>Infecciones de transmisión sexual (VIH, Virus de Papiloma Humano, otros)</li>
                <li>Diagnóstico y tratamiento de enfermedades que afectan a la salud reproductiva (incluidos el cáncer
                    de mama y el cáncer cervical).
                </li>
                <li>Tumores</li>
                <li>Lactancia materna</li>
                <br>

                <h5>SALUD DEL ADULTO MAYOR</h5>
                <li>Calidad y estilo de vida, aspectos socioeconómicos y derechos humanos</li>
                <li>Factores de riesgo y factores protectores</li>
                <li>Percepción sobre calidad de vida y derechos humanos de las personas adultas mayores</li>
                <li>Proceso Salud/Enfermedad</li>
                <li>Políticas y Modelos de Atención</li>
                <li>Tanatología</li>
                <li>Estado nutricional</li>
                <li>Aspectos psicoemocionales del adulto mayor</li>
                <br>

                <h5>GESTIÓN EN SISTEMAS Y SERVICIOS DE SALUD</h5>
                <li>Políticas y Modelos de Atención</li>
                <li>Educación en Salud (Profesionales y usuarios)</li>
                <li>Interculturalidad socioeconómica y salud</li>
                <li>Liderazgo en salud</li>
                <li>Sistema de Información en Salud</li>
                <li>Promoción de la Salud</li>
                <h5>SALUD MENTAL</h5>
                <li>Factores de la etiología, patogénesis, el diagnóstico, tratamiento y prevención de enfermedades y/o transtornos mentales</li>
                <li>Epidemilogía de los transtornos mentales</li>
                <li>Psicoterapia: factores del proceso psicoterapéutico</li>
                <li>Las drogas y la conducta de la fatiga, el sueño y la tensión (“stress”)</li>
                <li>Trastornos psiquiátricos del adulto</li>
                <li>Violencia Familiar</li>
                <li>Estilos de vida</li>
                <h5>TRASPLANTE DE ÓRGANOS</h5>
                <li>Donación y trasplante de órganos</li>
                <li>Ablación</li>
                <li>Trasplante de tejidos anatómicos humanos</li>
                <h5>TECNOLOGÍA DE LA INFORMACIÓN Y COMUNICACIÓN EN SALUD (TICS).</h5>
                <li>Utilización de tecnologías de la información y comunicación</li>
                <li>Tics en cuidados de la salud</li>
                <li>Aplicación de las TIC a programas de salud y desarrollo humano</li>
                <li>Sistema de georreferencia</li>
                <h5>BIOESTADÍSTICA APLICADA AL CAMPO DE LA SALUD</h5>
                <li>Modelado utilizando Machine Learning.</li>
                <li>Epidemiología</li>
                <h5>INNOVACIÓN EN CIENCIAS DE LA SALUD</h5>
                <li>Nano-materiales aplicados al campo de la Salud</li>
                <li>Desarrollos tecnológicos con aplicación médica</li>
                <li>Evaluación de incorporación de nuevas tecnologías en salud</li>
            </div>
        </div>
    </section>

@endsection
