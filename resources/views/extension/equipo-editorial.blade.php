@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Equipo Editorial</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h4 data-aos="fade-up">Editora</h4>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">Prof. Dra. María Isabel Rodríguez-Riveros UMAX</p>
                <h4 data-aos="fade-up" class="pt-4">Co-Editores</h4>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">Prof. MSc. Arsenio Amaral, UMAX</p>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">Prof. Dra. Rosa Zavala, UMAX</p>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">Prof. Lic. José E. García, UMAX</p>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">Dr. Nicolás Ayala-Servín; Investigador Asociado, UMAX</p>
                <h4 data-aos="fade-up" class="pt-4">Asistente de Idiomas</h4>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">Prof. Lic. Eustaquio Funes (Español)</p>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">Prof. Dra. Ramona Bolívar (Inglés)</p>
                <h4 data-aos="fade-up" class="pt-4">Asistencia técnica de Producción Editorial y Diagramación</h4>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">Ing. Cinthia Maiz</p>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">Lic. Jessica Izzi</p>
            </div>
        </div>
    </section>

@endsection