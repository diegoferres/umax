@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Pastoral</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <center>
                <div class="card-img-top">
                    <img width="250px" src="/images/Logo-Pastoral.png" class="img-fluid" alt="...">
                </div>
            </center>
            <div class="mt-5 text-center">

                {{--<h3 data-aos="fade-up">Justificación y Objetivos</h3>--}}
                <p class="mt-0" data-aos="fade-up" data-aos-duration="800">
                    Nuestra Pastoral Universitaria, está abocada al acompañamiento de la vida y el caminar de todos los
                    miembros de la comunidad educativa, promoviendo un encuentro personal y comprometido con Jesucristo
                    y la Santísima Virgen María, en innumerables iniciativas misioneras como solidarias.
                </p>
            </div>
            <h4 class="mt-5 aos-init aos-animate" data-aos="fade-up">Pilares de la pastoral</h4>
            {{--<p data-aos="fade-up" data-aos-duration="800" class="aos-init aos-animate text-justify">La extensión
                universitaria, forma parte integral de su tarea educativa, y que es fundamento de su sentido y
                propósito. La institución procura que profesores, alumnos y demás miembros del claustro universitario se
                comprometan libremente, en unidad de vida, con coherencia de pensamiento, palabra y acción, a buscar,
                descubrir, comunicar y conservar la verdad, en todos los campos del conocimiento, con fundamento en una
                concepción cristiana del hombre y del mundo, como contribución al progreso de la sociedad.
            </p>--}}
            <ol><b>1. La Evangelización:</b> La evangelización responde a la misión que todos tenemos: la de anunciar la
                alegría de la Buena Nueva del Reino de Dios.
            </ol>
            <ol><b>2. La devoción a la Virgen María Auxiliadora:</b> La devoción a la Virgen María, puesto que la
                universidad lleva el nombre de la Madre de Jesús, en su advocación a “María Auxiliadora”.
            </ol>
            <ol><b>3. La Caridad:</b> Sin obras de Caridad, no podrá llevarse a cabo la evangelización. La Institución
                desde sus orígenes realiza innumerables obras de caridad, a través de proyectos de ayuda social.
            </ol>

            <p data-aos="fade-up" data-aos-duration="800" class="aos-init aos-animate text-justify">
                Desde el año 2016 hasta la actualidad, la Pastoral Universitaria brinda asistencia médica a hogares,
                fundaciones y comunidades vulnerables en las especialidades de Clínica Médica, Psiquiatría, Ginecología,
                Pediatría, Medicina familiar, como parte de la Extensión Universitaria y Vinculación con el Medio.
                <br>
                <br>
                Nuestra Universidad en cuya misión se menciona la promoción de los valores humanos y cristianos, es una
                institución con orientación católica y ecuménica que apunta formar profesionales íntegros e idóneos, por
                lo que implementa la acción pastoral universitaria en la búsqueda de fortalecer la formación personal y
                espiritual de la comunidad educativa, como gestión prioritaria de la misma.
                <br>
                <br>
                Las actividades del ámbito académico de la Pastoral se planifican, ejecutan y evalúan en forma conjunta
                con el Vicerrectorado, responsable del área misional de la universidad; con los Directores del área
                pedagógica, de investigación y de extensión y vinculación con el medio, así como con los Decanos de las
                Facultades, y los Directores de las carreras de pregrado, grado y posgrado.
            </p>

            <h4 class="mt-5 aos-init aos-animate" data-aos="fade-up">Ámbitos de Desarrollo</h4>
            <h5 class="mt-5 aos-init aos-animate" data-aos="fade-up">Acción social y servicios solidarios</h5>
            <p data-aos="fade-up" data-aos-duration="800" class="aos-init aos-animate text-justify">La Dirección de la
                Pastoral, conjuntamente con la Dirección de Extensión y Vinculación con el Medio, propicia la creación
                sistemática de acciones pertinentes para el desarrollo de los servicios sociales comunitarios basados en
                la caridad, apuntando, sobre todo, a las comunidades más vulnerables.
            </p>
            <h5 class="mt-5 aos-init aos-animate" data-aos="fade-up">Acción litúrgica</h5>
            <p data-aos="fade-up" data-aos-duration="800" class="aos-init aos-animate text-justify">En el ámbito de la
                acción litúrgica, la Dirección de Pastoral busca constantemente la actualización espiritual de la
                Comunidad Universitaria: directivos, docentes, estudiantes y personal administrativo, mediante
                actividades sacramentales de su competencia, retiros, jornadas y reuniones espirituales, acordes a la
                línea del pensamiento doctrinal de la Iglesia Católica.
                <br>
                Asimismo, dentro de este ámbito se desarrollan las actividades religiosas cotidianas: Celebraciones
                Eucarísticas, Bautismos, Primera Comunión, Confirmación y fiestas a las advocaciones a la Virgen María y
                al Sagrado Corazón de Jesús.
            </p>
            <h5 class="mt-5 aos-init aos-animate" data-aos="fade-up">Relaciones interinstitucionales</h5>
            <p data-aos="fade-up" data-aos-duration="800" class="aos-init aos-animate text-justify">La Universidad María
                Auxiliadora, mediante la Dirección de Pastoral en cooperación con la Dirección de Extensión y
                Vinculación, promueve el vínculo con instituciones públicas y privadas, con la comunidad y la sociedad
                toda, a través de acciones concretas, articulando la acción pastoral y la responsabilidad social.
                <br>
                <br>
                En su afán de dar cumplimiento a este indicador promueve las alianzas estratégicas con otras
                instituciones de educación superior e instituciones religiosas, proponiendo la suscripción de convenios
                de colaboración mutua y de beneficios para estudiantes, docentes, personal administrativo/de apoyo y
                directivos y las comunidades con las cuales establece sus convenios.
            </p>
            <h5 class="mt-5 aos-init aos-animate" data-aos="fade-up">Principios</h5>
            <p data-aos="fade-up" data-aos-duration="800" class="aos-init aos-animate text-justify">En concordancia con
                sus principios cristianos, teniendo en cuenta la Espiritualidad cristiana como principio institucional
                y, basado en el Modelo Educativo Institucional, la Universidad María Auxiliadora establece los
                siguientes principios:
                <br>
            <ol><b>a. La formación de personas con valores y sentido de servicio:</b> acompañar la formación de personas con
                valores y con pleno sentido de servicio a la sociedad.
            </ol>
            <ol><b>b. Compromiso y responsabilidad:</b> con el afán de contribuir con sus principios, está comprometida con la
                comunidad educativa y la sociedad en general con el sentido de responsabilidad pastoral y, desde tal,
                con los aspectos de la vinculación con el medio en base a los pilares de la Evangelización, la Caridad y
                la Devoción a la Virgen María Auxiliadora.
            </ol>
            <ol><b>c. Formación integral de las personas:</b> para la implementación de los procesos de la Acción pastoral
                universitaria, en la Universidad María Auxiliadora, se concibe la educación como la totalidad de los
                procesos de formación y crecimiento humano continuo y evolutivo que se proyectan en la humanización y la
                formación integral del hombre como hijo de Dios, y como persona, en su contexto socioambiental para
                contribuir al desarrollo sociocultural de la comunidad.
            </ol>
            <ol><b>d. Ecumenismo:</b> dentro de la Pastoral Universitaria, además de respetar la libertad de la diversidad
                cultural religiosa y la libertad de pensamiento, en la práctica de la acción pastoral se tiene absoluto
                respeto a la manifestación de todas las creencias que no se opongan a la ética, la moral y a las buenas
                costumbres o al orden público.
            </ol>
            <ol>
                <b>e. Fraternidad:</b> asimismo, desde los principios pastorales, se promueve la convivencia comunitaria,
                dentro y fuera de la comunidad universitaria con un sentido fraterno y solidario, apuntando a la armonía
                con enfoque cristiano de orientación católica.
            </ol>
        </div>


        <div class="fotos">
            <div class="container">
                <h3 class="mt-5 py-5" data-aos="fade-up">Fotos</h3>
            </div>
            <!-- Glider-->
            <div class="glider-contain mb-5">
                <div class="glider">
                    <div class="ml-0" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/1.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/2.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/3.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/4.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/5.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/6.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/7.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/8.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/9.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/10.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/11.jpg') }}" class="d-block w-100" alt="IMG">
                    </div>
                    <div class="ml-1" data-aos="fade-left">
                        <img src="{{ asset('images/pastoral/12.jpeg') }}" class="d-block w-100" alt="IMG">
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <button role="button" aria-label="Previous" class="glider-prev"><i class="fas fa-angle-left"></i>
                    </button>
                    <button role="button" aria-label="Next" class="glider-next"><i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
            <!-- End Glider-->
        </div>
    </section>

@section('scripts')
    <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script>
@endsection

@endsection
