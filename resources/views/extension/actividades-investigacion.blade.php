@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Actividades de Investigación</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Actividades de Investigación</h3>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                   Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aspernatur veniam iste asperiores beatae tempore. Tempore odit quia dolor dicta voluptatibus provident maiores cupiditate est ut?
                   Sunt officia quam consequuntur rem nisi fuga corrupti earum, cumque veniam mollitia eius voluptas temporibus sequi atque, similique, ex nihil iure quos. Dolor, maiores non?
                   Vero vel ad obcaecati maxime sit aut in error rem, optio magni eligendi dolorem numquam iure officiis voluptas reiciendis perspiciatis eveniet? Saepe rem unde quis.
                </p>
            </div>
        </div>
    </section>

@endsection