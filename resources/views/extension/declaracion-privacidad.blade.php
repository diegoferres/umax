@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Declaración Privacidad</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros" data-aos="fade-up" data-aos-duration="800">
                <h4>Los nombres y las direcciones de correo electrónico introducidos en esta revista se usarán exclusivamente para los fines establecidos en ella y no se proporcionarán a terceros o para su uso con otros fines.</h4>
                <div class="mt-5" data-aos="fade-up" data-aos-duration="800">
                    <p>Revista Científica UMAX</p>
                    <div class="d-flex align-items-center mb-3">
                        <img src="{{ asset('images/cc.png') }}" alt="CC" width="50">
                        <p class="ml-2 mb-0">Todo contenido de esta revista está bajo Licencia.</p>
                    </div>
                    {{--<p>ISSN (impreso) 111111</p>--}}
                    <p>ISSN (digital) En proceso</p>
                </div>
            </div>
        </div>
    </section>

@endsection
