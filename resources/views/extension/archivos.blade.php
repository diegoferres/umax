@extends('layout.app')

@section('head')

@endsection

@section('content')
    
    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Archivos</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
            <div class="row">
                <div class="col-12 col-lg-4" data-aos="fade-right">
                    <img src="{{ asset('images/portada-revista.jpg') }}" alt="" class="img-fluid">
                    <div class="text px-5" style="background-color: #F6F6FA;">
                        <a href="{{ route('revista-cientifica')}} "> <h4 data-aos="fade-up">Vol. 1. Número 1 (2020). Revista Científica UMAX</h4></a>
                        <h5 class="mb-4">PUBLICADO: 2020-12-22</h5>
                        <h4>Editorial</h4>
                        <p>Prof. Dr. Nombre y Apellido</p>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>

@endsection