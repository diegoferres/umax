<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>ADMIN - UMAX</title>
<!-- plugins:css -->
<link rel="stylesheet" href="/administration/vendors/iconfonts/font-awesome/css/all.min.css">
<link rel="stylesheet" href="/administration/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet" href="/administration/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="/administration/css/style.css">
<!-- endinject -->
<link rel="shortcut icon" href="http://www.urbanui.com/"/>
@yield('styles')
