<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="profile-image">
                    <img src="https://www.umax.edu.py/images/logo.png" alt="image"/>
                </div>
                <div class="profile-name">
                    <p class="name">
                        {{ Auth::user()->name }}
                    </p>
                    <p class="designation">
                        {{ Auth::user()->email }}
                    </p>
                </div>
            </div>
        </li>
        {{--<li class="nav-item">
            <a class="nav-link" href="{{ route('admin.dashboard') }}">
                <i class="fa fa-home menu-icon"></i>
                <span class="menu-title">Tablero</span>
            </a>
        </li>--}}
        <li class="nav-item {{ Request::route()->getName() == 'admin.banners.index' ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.banners.index') }}">
                <i class="fa fa-images menu-icon"></i>
                <span class="menu-title">Banners</span>
            </a>
        </li>
        <li class="nav-item {{ Request::route()->getName() == 'admin.news.index'
                               || Request::route()->getName() == 'admin.news.create'
                               ||  Request::route()->getName() == 'admin.news.edit'? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.news.index') }}">
                <i class="fa fa-newspaper menu-icon"></i>
                <span class="menu-title">Noticias</span>
            </a>
        </li>

        <li class="nav-item {{ Request::is('admin/covenants*') }}">
            <a class="nav-link" href="{{ route('admin.covenants.index') }}">
                <i class="fa fa-building menu-icon"></i>
                <span class="menu-title">Convenios</span>
            </a>
        </li>
    </ul>
</nav>
<!-- partial -->
