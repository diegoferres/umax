@extends('administration.layout.app')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Tablero
            </h3>
        </div>

        <div class="row">
            <div class="col-md-8 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <i class="fas fa-newspaper"></i>
                            Noticias
                            <a href="{{ route('admin.news.create') }}" class="fa-pull-right btn btn-info btn-xs"><i class="fas fa-plus"></i></a>
                        </h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Título</th>
                                    <th>Autor</th>
                                    <th>Fecha Noticia</th>
                                    <th>Categorias</th>
                                    <th>Estado</th>
                                    <th>Creado el</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($news as $new)
                                    <tr>
                                        <td class="font-weight-bold">
                                            {{ $new->id }}
                                        </td>
                                        <td class="text-muted">
                                            {{ $new->title }}
                                        </td>
                                        <td class="text-muted">
                                            {{ $new->autor }}
                                        </td>
                                        <td>
                                            {{ $new->datetime }}
                                        </td>
                                        <td>
                                            @foreach ($new->categories as $category)
                                                <h5><span class="badge badge-pill badge-primary ">{{ $category->name }}</span></h5>
                                            @endforeach
                                        </td>
                                        <td>
                                            <label class="badge badge-{{ $new->published ? 'success' : 'default' }} badge-pill badge-">{{ $new->published ? 'Publicado' : 'Archivado' }}</label>
                                        </td>
                                        <td>
                                            {{ $new->created_at }}
                                        </td>
                                        <td>
                                            {!! Form::open(['route' => ['admin.news.destroy', $new->id], 'method' => 'DELETE']) !!}
                                                <button type="submit" class="btn btn-outline-danger btn-xs"><i class="fas fa-trash"></i></button>
                                            {!! Form::close() !!}
                                            <a href="{{ route('admin.news.edit', $new->id) }}" class="btn btn-outline-info btn-xs"><i class="fas fa-pencil-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $news->links() }}
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection
@section('scripts')
<!-- Custom js for this page-->
<script src="/administration/js/dashboard.js"></script>
<!-- End custom js for this page-->
@endsection

