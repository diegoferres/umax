@extends('administration.layout.app')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Tablero
            </h3>
        </div>

        <div class="row">
            <div class="col-md-8 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <i class="fas fa-newspaper"></i>
                            Convenios
                            <a href="{{ route('admin.covenants.create') }}" class="fa-pull-right btn btn-info btn-xs"><i class="fas fa-plus"></i></a>
                        </h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Título</th>
                                    <th>Imagen</th>
                                    <th>Imagen Interna</th>
                                    <th>Link Externo</th>
                                    <th>Creado el</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($covenants as $covenant)
                                    <tr>
                                        <td class="font-weight-bold">
                                            {{ $covenant->id }}
                                        </td>
                                        <td class="text-muted">
                                            {{ $covenant->title }}
                                        </td>
                                        <td class="text-muted">
                                            @if ($covenant->image_path)
                                                <a href="{{ asset('storage/'.$covenant->image_path) }}" target="_blank"><img height="50px" src="{{ asset('storage/'.$covenant->image_path) }}" alt=""></a>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($covenant->internal_image_path)
                                                <a href="{{ asset('storage/'.$covenant->internal_image_path) }}" target="_blank"><img height="50px" src="{{ asset('storage/'.$covenant->internal_image_path) }}" alt=""></a>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $covenant->created_at }}
                                        </td>
                                        <td>
                                            {!! Form::open(['route' => ['admin.covenants.destroy', $covenant->id], 'method' => 'DELETE']) !!}
                                                <button type="submit" class="btn btn-outline-danger btn-xs"><i class="fas fa-trash"></i></button>
                                            {!! Form::close() !!}
                                            <a href="{{ route('admin.covenants.edit', $covenant->id) }}" class="btn btn-outline-info btn-xs"><i class="fas fa-pencil-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $covenants->links() }}
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection
@section('scripts')
<!-- Custom js for this page-->
<script src="/administration/js/dashboard.js"></script>
<!-- End custom js for this page-->
@endsection

