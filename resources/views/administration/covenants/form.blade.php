@extends('administration.layout.app')
@section('styles')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Convenios
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Admin</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Convenios</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        @isset ($covenants)
                            <h4 class="card-title">Editar convenio</h4>
                        @else
                            <h4 class="card-title">Crear convenio</h4>
                        @endisset
                        <p class="card-description">
                            Carga la información del convenio
                        </p>
                        @isset ($covenants)
                            {!! Form::model($covenants, ['route' => ['admin.covenants.update', $covenants->id], 'method' => 'PATCH','files' => true]) !!}
                        @else
                            {!! Form::open(['route' => 'admin.covenants.store', 'method' => 'post' ,'files' => true]) !!}
                        @endisset

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Título:</label>
                                {!! Form::text('title', old('title'), ['class' => 'form-control', 'required']) !!}
                            </div>

                            <div class="col-sm-3">
                                <label>Imagen convenio:</label>
                                <input name="image_path" class="form-control" type="file"/>
                            </div>

                            <div class="col-sm-3">
                                <label>Imagen Interna:</label>
                                <input name="internal_image_path" class="form-control" type="file"/>
                            </div>

                            <div class="col-sm-6">
                                <label>URL Externo:</label>
                                {!! Form::text('external_link', old('external_link'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">

                            <label for="exampleTextarea1">Textarea</label>
                            {!! Form::textarea('content_text', old('content_text') ?? isset($covenants) ? $covenants->content : '', ['class' => 'form-control', 'id' => 'tinyEditor']) !!}
                            {{--<textarea name="content_text" id="summernote"></textarea>--}}
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Guardar</button>
                        <a href="{{ route('admin.covenants.index') }}" class="btn btn-light">Cancelar</a>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script src="https://cdn.tiny.cloud/1/le0xnl72caevg2xl8a4noobxfd3cuv10352omp6pa0ieawfi/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>

    <script>
        $(document).ready(function () {
            $('#categories').select2()
        })


        tinymce.init({
            menubar: false,
            height: 500,
            selector: 'textarea',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | bold italic underline strikethrough ' +
                '| fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | ' +
                'outdent indent |  numlist bullist | forecolor backcolor removeformat | emoticons | fullscreen  preview',
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function () {

            $('input[name=datetime]').inputmask("datetime", {
                mask: "1-2-y h:s",
                placeholder: "dd-mm-yyyy hh:mm",
                //leapday: "-02-29",
                separator: "-",
                alias: "dd-mm-yyyy"
            });

            /*$('#summernote').summernote({
                placeholder: 'Escribe aquí tu noticia',
                tabsize: 2,
                height: 400,
                lang: "es-ES",
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link']],
                ],
            });*/
        });
    </script>

@endsection

