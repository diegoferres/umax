@extends('administration.layout.app')
@section('styles')

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Noticias
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Admin</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Noticias</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        @isset ($new)
                            <h4 class="card-title">Editar noticia noticia</h4>
                        @else
                            <h4 class="card-title">Crear noticia</h4>
                        @endisset
                        <p class="card-description">
                            Carga la información de las últimas noticias
                        </p>
                        @isset ($new)
                            {!! Form::model($new, ['route' => ['admin.news.update', $new->id], 'method' => 'PATCH','files' => true]) !!}
                        @else
                            {!! Form::open(['route' => 'admin.news.store', 'method' => 'post' ,'files' => true]) !!}
                        @endisset

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Título:</label>
                                {!! Form::text('title', old('title'), ['class' => 'form-control', 'required']) !!}
                                {{--<input name="title" class="form-control"/>--}}
                            </div>
                            <div class="col-sm-3">
                                <label>Fecha:</label>
                                {!! Form::text('datetime', old('datetime') ?: (isset($new) ? date('d-m-Y H:i', strtotime($new->datetime)) : date('d-m-Y H:i')) , ['class' => 'form-control', 'data-inputmask' => "'alias': 'datetime'", 'required']) !!}
                                {{--<input name="datetime" value="{{ date('d-m-Y H:s') }}" class="form-control" data-inputmask="'alias': 'datetime'" />--}}
                            </div>
                            <div class="col-sm-3">
                                <label>Imagen:</label>
                                <input name="image" class="form-control" type="file"
                                       @if (!isset($new)) required @endif />
                            </div>
                        </div>

                        <div class="form-group">

                            <label for="exampleTextarea1">Textarea</label>
                            {!! Form::textarea('content_text', old('content_text') ?? isset($new) ? $new->content : '', ['class' => 'form-control', 'id' => 'tinyEditor']) !!}
                            {{--<textarea name="content_text" id="summernote"></textarea>--}}
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Guardar</button>
                        <a href="{{ route('admin.news.index') }}" class="btn btn-light">Cancelar</a>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script src="https://cdn.tiny.cloud/1/le0xnl72caevg2xl8a4noobxfd3cuv10352omp6pa0ieawfi/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            menubar: false,
            height: 500,
            selector: 'textarea',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | bold italic underline strikethrough ' +
                '| fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | ' +
                'outdent indent |  numlist bullist | forecolor backcolor removeformat | emoticons | fullscreen  preview',
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function () {

            $('input[name=datetime]').inputmask("datetime", {
                mask: "1-2-y h:s",
                placeholder: "dd-mm-yyyy hh:mm",
                //leapday: "-02-29",
                separator: "-",
                alias: "dd-mm-yyyy"
            });

            /*$('#summernote').summernote({
                placeholder: 'Escribe aquí tu noticia',
                tabsize: 2,
                height: 400,
                lang: "es-ES",
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link']],
                ],
            });*/
        });
    </script>

@endsection

