@component('mail::message')
Contacto desde Contacto

<table>
    <tr align="left">
        <th>Nombre y Apellido</th>
        <td>{{ $data['name'] }}</td>
    </tr>
    <tr align="left">
        <th>Correo</th>
        <td>{{ $data['email'] }}</td>
    </tr>
    <tr align="left">
        <th>Celular</th>
        <td>{{ $data['cellphone'] }}</td>
    </tr>
    <tr align="left">
        <th>Mensaje</th>
        <td>{{ $data['message'] }}</td>
    </tr>
</table>
<br>
<br>

{{--@component('mail::button', ['url' => ''])
Button Text
@endcomponent--}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
