@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Técnico Superior en Farmacia</h2>
        </div>
        <img src="{{ asset('images/banner-medicina.png') }}" alt="" class="img-fluid">
    </div>

    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h4 class="mt-5" data-aos="fade-up">PERFIL PROFESIONAL DEL TÉCNICO SUPERIOR EN FARMACIA</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    Al concluir sus estudios el Técnico Superior en Farmacia será un profesional con formación
                    científica, técnica y humanística capaz de:
                    <br>
                <ul>
                    <li>Desarrollar su actividad dentro de la Farmacia asistencial, hospitalaria, industria farmacéutica
                        y/o de preparados magistrales, bajo la supervisión del profesional farmacéutico y/o del químico
                        farmacéutico conforme al código sanitario y cumplimiento adecuado de las instrucciones
                        establecidos en los protocolos de cada área de trabajo, descriptivos como normas y
                        procedimientos de actuación.
                    </li>
                    <li>Realizar la dispensación, control e información sobre medicamentos para la prevención,
                        tratamiento, recuperación y rehabilitación de la salud, así como en la preparación, y otras
                        actividades orientadas, a fin de conseguir una utilización apropiada, segura y costo-efectiva de
                        los medicamentos y productos sanitarios, en beneficio de los usuarios.
                    </li>
                    <li>Participar en campañas y actividades de educación sanitaria de manera permanente.</li>
                    <li>Desarrollar su práctica con responsabilidad en el ejercicio de su profesión con actitud ética,
                        reflexiva y crítica, tendiente a mejorar la calidad de vida de las personas, las familias y la
                        comunidad.
                    </li>
                    <li>Utilizar la tecnología vigente y el equipamiento acorde, en el ejercicio de su profesión,
                        empleando las Nuevas Tecnologías de la información y la comunicación (NTIC).
                    </li>
                    <li>Demostrar destreza y habilidad comunicacional en el ejercicio de la profesión.</li>
                    <li>Valorar el trabajo en equipo interdisciplinario para brindar una atención integral y de calidad
                        a la persona, la familia y la comunidad.
                    </li>
                    <li>Participar en la planificación y organización de los procesos internos de la unidad en la que
                        presta servicios, a fin de mejorar la calidad de la atención, promover los cambios y fortalecer
                        la gestión.
                    </li>
                </ul>
                </p>

                <h4 class="mt-5" data-aos="fade-up">ROL DEL TÉCNICO SUPERIOR EN FARMACIA</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    Desempeñar los servicios generales, complementarios y de apoyo, bajo la supervisión del profesional
                    Químico Farmacéutico y/o Farmacéutico, en los servicios de farmacia y servicios sanitarios,
                    desarrollando las siguientes actividades:
                    <br>
                <ol>
                    <li>Recepción, almacenamiento, dispensación, reposición de medicamentos, control de stock, insumos varios utilizados en los servicios de farmacia y servicios sanitarios, garantizando su correcta conservación y organización según los criterios establecidos.</li>
                    <li>Colaboración, bajo la supervisión del profesional químico farmacéutico y/o farmacéutico, en la disposición de medicamentos e insumos hospitalarios en las diferentes modalidades, así como realizar la preparación y disposición de las solicitudes de medicamentos e insumos hospitalarios, de acuerdo con las normas de actuación establecidas.</li>
                    <li>Almacenamiento de datos, control y archivo de los impresos y registros utilizados en la recepción, conservación, custodia, dispensación y distribución de las solicitudes de medicamentos y productos sanitarios, de acuerdo con las normas de actuación establecida.</li>
                    <li>Manejo, limpieza, conservación, mantenimiento del buen funcionamiento y acondicionamiento de los equipos e instrumentales, materiales y demás medios utilizados para las preparaciones magistrales e industriales.</li>
                    <li>Control de los dispositivos de dosificación y envasado de medicamentos, así como el stock de los mismos a objeto de contar con disponibilidad en cantidad, calidad y estado operativo en el momento de ser requeridos.</li>
                    <li>Colaboración, bajo la supervisión del personal farmacéutico, en la elaboración y control de calidad de las preparaciones farmacéuticas, según los procedimientos establecidos de acuerdo a las normas de correcta elaboración y control de calidad.</li>
                    <li>Colaboración en el cumplimiento de los principios del sistema de gestión de la calidad establecido en el servicio de farmacia.</li>
                    <li>Utilización de las Nuevas Tecnologías de la Información y la Comunicación (TICs) y los programas informáticos para la disposición.</li>
                </ol>
                </p>
            </div>
        </div>
        <div class="malla">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <h3 class="mb-5" data-aos="fade-left">Malla Curricular</h3>
                    <ul class="nav nav-pills nav-date date mb-3" id="pills-tab" role="tablist">
                        {{--<li class="nav-item">
                            <a class="nav-link active" id="pills-2015-tab" data-toggle="pill" href="#pills-2015" role="tab"
                               aria-controls="pills-2015" aria-selected="true">2015</a>
                        </li>--}}
                        {{--<li class="nav-item">
                            <a class="nav-link active" id="pills-2018-tab" data-toggle="pill" href="#pills-2018"
                               role="tab"
                               aria-controls="pills-2018" aria-selected="false">2018</a>
                        </li>--}}
                    </ul>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    {{--<div class="tab-pane fade show active" id="pills-2015" role="tabpanel" aria-labelledby="pills-2015-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#primer"
                                   role="tab" aria-controls="primer" aria-selected="true">PRIMER AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo" role="tab"
                                   aria-controls="segundo" aria-selected="false">SEGUNDO AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero" role="tab"
                                   aria-controls="tercero" aria-selected="false">TERCER AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto" role="tab"
                                   aria-controls="cuarto" aria-selected="false">CUARTO AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto" role="tab"
                                   aria-controls="quinto" aria-selected="false">QUINTO AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#sexto" role="tab"
                                   aria-controls="sexto" aria-selected="false">SEXTO AÑO</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="primer" role="tabpanel" aria-labelledby="primer-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>1º SEMESTRE</h5>
                                            <p>Anatomía Humana I</p>
                                            <p>Histología Humana I</p>
                                            <p>Matemática Aplicada a la Medicina</p>
                                            <p>Medicina de la Comunidad</p>
                                            <p>Comunicación Oral y Escrita</p>
                                            <p>Biología Celular</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>2º SEMESTRE</h5>
                                            <p>Anatomía Humana II</p>
                                            <p>Histología Humana II</p>
                                            <p>Embriología y Genética</p>
                                            <p>Epidemiología y Ecología</p>
                                            <p>Biofísica Médica</p>
                                            <p>Bioestadística y Demografía</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="segundo" role="tabpanel" aria-labelledby="segundo-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>3º SEMESTRE</h5>
                                            <p>Fisiología Humana I</p>
                                            <p>Microbiología, Parasitología e Inmunología I</p>
                                            <p>Bioquímica Médica I</p>
                                            <p>Metodología de la Investigación</p>
                                            <p>Guaraní</p>
                                            <p>Inglés</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>4º SEMESTRE</h5>
                                            <p>Fisiología Humana II</p>
                                            <p>Microbiología, Parasitología e Inmunología II</p>
                                            <p>Bioquímica Médica II</p>
                                            <p>Psicología Médica</p>
                                            <p>Socio-antropología</p>
                                            <p>Primeros Auxilios</p>
                                            <p>Principios y Valores Humanos y Espirituales</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tercero" role="tabpanel" aria-labelledby="tercero-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>5º SEMESTRE</h5>
                                            <p>Fisiopatología I</p>
                                            <p>Anatomía Patológica I</p>
                                            <p>Medicina Preventiva y Salud Pública</p>
                                            <p>Medicina Familiar</p>
                                            <p>Informática Aplicada a la Medicina</p>
                                            <p>Patología Médica I</p>
                                            <p>Patología Quirúrgica I</p>
                                            <p>Optativa I</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>6º SEMESTRE</h5>
                                            <p>Fisiopatología II</p>
                                            <p>Anatomía Patológica II</p>
                                            <p>Economía</p>
                                            <p>Ética y Bioética Médica</p>
                                            <p>Medicina Legal</p>
                                            <p>Patología Médica II</p>
                                            <p>Patología Quirúrgica II</p>
                                            <p>Psicopatología</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="cuarto" role="tabpanel" aria-labelledby="cuarto-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>7º SEMESTRE</h5>
                                            <p>Semiología Médica I</p>
                                            <p>Semiología Quirúrgica I</p>
                                            <p>Farmacología I</p>
                                            <p>Otorrinolaringología</p>
                                            <p>Nutrición Clínica</p>
                                            <p>Medicina de Imágenes</p>
                                            <p>Psiquiatría</p>
                                            <p>Administración de Servicios de Salud</p>
                                            <p>Optativa II</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>8º SEMESTRE</h5>
                                            <p>Semiología Médica II</p>
                                            <p>Semiología Quirúrgica II</p>
                                            <p>Farmacología II</p>
                                            <p>Medicina Tropical</p>
                                            <p>Dermatología</p>
                                            <p>Oftalmología</p>
                                            <p>Urología</p>
                                            <p>Anestesiología</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="quinto" role="tabpanel" aria-labelledby="quinto-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>9º SEMESTRE</h5>
                                            <p>Cirugía I</p>
                                            <p>Ginecología y Obstetricia I</p>
                                            <p>Medicina Interna I</p>
                                            <p>Pediatría I</p>
                                            <p>Toxicología</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>10º SEMESTRE</h5>
                                            <p>Cirugía II</p>
                                            <p>Ginecología y Obstetricia II</p>
                                            <p>Medicina Interna II</p>
                                            <p>Pediatría II</p>
                                            <p>Traumatología, Ortopedia y Rehabilitación</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="sexto" role="tabpanel" aria-labelledby="sexto-tab">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>11º y 12º SEMESTRE</h5>
                                            <p>Internado en Medicina Interna</p>
                                            <p>Internado en Cirugía</p>
                                            <p>Internado en Pediatría</p>
                                            <p>Internado en Ginecología y Obstetricia</p>
                                            <p>Internado en Emergentología</p>
                                            <p>Atención Primaria en Salud en la comunidad</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mt-5 justify-content-md-between flex-column flex-md-row">
                            <div data-aos="fade-right" class="mb-3 mb-md-0">
                                <h4>Título a obtener: MEDICO</h4>
                                <h4>TOTAL HORAS: 8.340 horas reloj</h4>
                                <h4>EXTENSION UNIVERSITARIA: 60 horas reloj</h4>
                            </div>
                            <a href="{{ asset('records/brochure-medicina.pdf') }}" class="btn btn-primary" data-aos="fade-left"
                               target="_blank ">DESCARGAR BROCHURE</a>
                        </div>
                    </div>--}}
                    <div class="tab-pane fade show active" id="pills-2018" role="tabpanel"
                         aria-labelledby="pills-2018-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab"
                                   href="#primer-2018"
                                   role="tab" aria-controls="primer-2018" aria-selected="true">PRIMER AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo-2018"
                                   role="tab" aria-controls="segundo-2018" aria-selected="false">SEGUNDO AÑO</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="primer-2018" role="tabpanel"
                                     aria-labelledby="primer-2018-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>1º SEMESTRE</h5>
                                            <p>Lengua Castellana I</p>
                                            <p>Informática Aplicada I</p>
                                            <p>Contabilidad básica</p>
                                            <p>Proyecto y Metodología de trabajo intelectual I</p>
                                            <p>Química Inorgánica</p>
                                            <p>Anatomía y Fisiología I</p>
                                            <p>Primeros Auxilios</p>
                                            <p>Farmacología I</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>2º SEMESTRE</h5>
                                            <p>Lengua Castellana II</p>
                                            <p>Química Orgánica</p>
                                            <p>Anatomía y Fisiología II</p>
                                            <p>Patología General</p>
                                            <p>Farmacotecnia I</p>
                                            <p>Farmacología II</p>
                                            <p>Técnicas de Atención al Cliente</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="segundo-2018" role="tabpanel"
                                     aria-labelledby="segundo-2018-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>3º SEMESTRE</h5>
                                            <p>Informática Aplicada II</p>
                                            <p>Ética Profesional</p>
                                            <p>Microbiología y Parasitología</p>
                                            <p>Calidad en Salud</p>
                                            <p>Farmacotecnia II</p>
                                            <p>Farmacología III</p>
                                            <p>Botánica Farmacéutica</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>4º SEMESTRE</h5>
                                            <p>Lengua Guarani</p>
                                            <p>Lengua Extranjera - Inglés</p>
                                            <p>Proyecto y Metodología de trabajo intelectual II</p>
                                            <p>Salud Pública</p>
                                            <p>Administración Farmacéutica</p>
                                            <p>Marketing Farmacéutico</p>
                                            <p>Legislación Farmacéutica</p>
                                            <p>Cosmetología básica</p>
                                            <p>Proyecto de Graduación</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mt-5 justify-content-md-between flex-column flex-md-row">
                            <div data-aos="fade-right" class="mb-3 mb-md-0">
                                <h4>TOTAL HORAS TEÓRICAS Y PRÁCTICAS: 1.600 horas</h4>
                                <h4>PASANTÍAS HORAS RELOJ: 500 horas</h4>
                                <h4>TOTAL CARGA HORARIA DE LA CARRERA: 2.100 horas</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@section('scripts')
    {{--  <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script> --}}
@endsection

@endsection
