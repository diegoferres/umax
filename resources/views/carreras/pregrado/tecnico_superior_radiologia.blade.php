@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Técnico Superior en Radiología</h2>
        </div>
        <img src="{{ asset('images/banner-medicina.png') }}" alt="" class="img-fluid">
    </div>

    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h4 class="mt-5" data-aos="fade-up">PERFIL PROFESIONAL DEL TÉCNICO SUPERIOR EN RADIOLOGÍA</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    Al concluir los estudios, el Técnico Superior en Radiología será capaz de desarrollar
                    eficientemente las siguientes funciones:
                    <br>
                <ul>
                    <li>Aplicar las técnicas y procedimientos radiológicos más idóneos para lograr la mejor imagen
                        diagnóstica de las enfermedades.
                    </li>
                    <li>Realizar estudios radiológicos simples, contrastados y especializados.</li>
                    <li>Manejar equipos de alta tecnología para Tomografía Axial Computarizada.</li>
                    <li>Operar equipos para estudios de diagnóstico y tratamiento de Hemodinamia.</li>
                    <li>Manejar equipos de última generación para Resonancia Magnética.</li>
                    <li>Operar equipos para la aplicación de tratamientos de Radioterapia.</li>
                    <li>Identificar signos de imagen de la normalidad, entender la patología y encauzar los estudios de
                        imagenología.
                    </li>
                </ul>
                </p>
                <h4 class="mt-5" data-aos="fade-up">CAMPO OCUPACIONAL</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    El Técnico Superior en Radiología saldrá capacitado para poder desenvolverse como radiólogo en las
                    áreas de atención a los pacientes, investigativas y educativas, en cualquier sitio donde se le
                    requiera, bien sea institucional o en consultorio particular.
                    <br>
                    <br>
                    Específicamente, podría ocupar cargo de radiólogo general en cualquier hospital, clínica o IPS que
                    lo requiera, siendo capaz de resolver los problemas más comunes, y con el criterio para remitir y
                    consultar los casos más complejos. Además, quedará con la preparación adecuada para ingresar a
                    cualquier programa de subespecialidad que desee. También podría optar por dedicarse a la
                    investigación clínica, experimental o epidemiológica.
                </p>
            </div>
        </div>
        <div class="malla">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <h3 class="mb-5" data-aos="fade-left">Malla Curricular</h3>
                    <ul class="nav nav-pills nav-date date mb-3" id="pills-tab" role="tablist">
                        {{--<li class="nav-item">
                            <a class="nav-link active" id="pills-2015-tab" data-toggle="pill" href="#pills-2015" role="tab"
                               aria-controls="pills-2015" aria-selected="true">2015</a>
                        </li>--}}
                        {{--<li class="nav-item">
                            <a class="nav-link active" id="pills-2018-tab" data-toggle="pill" href="#pills-2018"
                               role="tab"
                               aria-controls="pills-2018" aria-selected="false">2018</a>
                        </li>--}}
                    </ul>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    {{--<div class="tab-pane fade show active" id="pills-2015" role="tabpanel" aria-labelledby="pills-2015-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#primer"
                                   role="tab" aria-controls="primer" aria-selected="true">PRIMER AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo" role="tab"
                                   aria-controls="segundo" aria-selected="false">SEGUNDO AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero" role="tab"
                                   aria-controls="tercero" aria-selected="false">TERCER AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto" role="tab"
                                   aria-controls="cuarto" aria-selected="false">CUARTO AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto" role="tab"
                                   aria-controls="quinto" aria-selected="false">QUINTO AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#sexto" role="tab"
                                   aria-controls="sexto" aria-selected="false">SEXTO AÑO</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="primer" role="tabpanel" aria-labelledby="primer-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>1º SEMESTRE</h5>
                                            <p>Anatomía Humana I</p>
                                            <p>Histología Humana I</p>
                                            <p>Matemática Aplicada a la Medicina</p>
                                            <p>Medicina de la Comunidad</p>
                                            <p>Comunicación Oral y Escrita</p>
                                            <p>Biología Celular</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>2º SEMESTRE</h5>
                                            <p>Anatomía Humana II</p>
                                            <p>Histología Humana II</p>
                                            <p>Embriología y Genética</p>
                                            <p>Epidemiología y Ecología</p>
                                            <p>Biofísica Médica</p>
                                            <p>Bioestadística y Demografía</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="segundo" role="tabpanel" aria-labelledby="segundo-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>3º SEMESTRE</h5>
                                            <p>Fisiología Humana I</p>
                                            <p>Microbiología, Parasitología e Inmunología I</p>
                                            <p>Bioquímica Médica I</p>
                                            <p>Metodología de la Investigación</p>
                                            <p>Guaraní</p>
                                            <p>Inglés</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>4º SEMESTRE</h5>
                                            <p>Fisiología Humana II</p>
                                            <p>Microbiología, Parasitología e Inmunología II</p>
                                            <p>Bioquímica Médica II</p>
                                            <p>Psicología Médica</p>
                                            <p>Socio-antropología</p>
                                            <p>Primeros Auxilios</p>
                                            <p>Principios y Valores Humanos y Espirituales</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tercero" role="tabpanel" aria-labelledby="tercero-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>5º SEMESTRE</h5>
                                            <p>Fisiopatología I</p>
                                            <p>Anatomía Patológica I</p>
                                            <p>Medicina Preventiva y Salud Pública</p>
                                            <p>Medicina Familiar</p>
                                            <p>Informática Aplicada a la Medicina</p>
                                            <p>Patología Médica I</p>
                                            <p>Patología Quirúrgica I</p>
                                            <p>Optativa I</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>6º SEMESTRE</h5>
                                            <p>Fisiopatología II</p>
                                            <p>Anatomía Patológica II</p>
                                            <p>Economía</p>
                                            <p>Ética y Bioética Médica</p>
                                            <p>Medicina Legal</p>
                                            <p>Patología Médica II</p>
                                            <p>Patología Quirúrgica II</p>
                                            <p>Psicopatología</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="cuarto" role="tabpanel" aria-labelledby="cuarto-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>7º SEMESTRE</h5>
                                            <p>Semiología Médica I</p>
                                            <p>Semiología Quirúrgica I</p>
                                            <p>Farmacología I</p>
                                            <p>Otorrinolaringología</p>
                                            <p>Nutrición Clínica</p>
                                            <p>Medicina de Imágenes</p>
                                            <p>Psiquiatría</p>
                                            <p>Administración de Servicios de Salud</p>
                                            <p>Optativa II</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>8º SEMESTRE</h5>
                                            <p>Semiología Médica II</p>
                                            <p>Semiología Quirúrgica II</p>
                                            <p>Farmacología II</p>
                                            <p>Medicina Tropical</p>
                                            <p>Dermatología</p>
                                            <p>Oftalmología</p>
                                            <p>Urología</p>
                                            <p>Anestesiología</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="quinto" role="tabpanel" aria-labelledby="quinto-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>9º SEMESTRE</h5>
                                            <p>Cirugía I</p>
                                            <p>Ginecología y Obstetricia I</p>
                                            <p>Medicina Interna I</p>
                                            <p>Pediatría I</p>
                                            <p>Toxicología</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>10º SEMESTRE</h5>
                                            <p>Cirugía II</p>
                                            <p>Ginecología y Obstetricia II</p>
                                            <p>Medicina Interna II</p>
                                            <p>Pediatría II</p>
                                            <p>Traumatología, Ortopedia y Rehabilitación</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="sexto" role="tabpanel" aria-labelledby="sexto-tab">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>11º y 12º SEMESTRE</h5>
                                            <p>Internado en Medicina Interna</p>
                                            <p>Internado en Cirugía</p>
                                            <p>Internado en Pediatría</p>
                                            <p>Internado en Ginecología y Obstetricia</p>
                                            <p>Internado en Emergentología</p>
                                            <p>Atención Primaria en Salud en la comunidad</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mt-5 justify-content-md-between flex-column flex-md-row">
                            <div data-aos="fade-right" class="mb-3 mb-md-0">
                                <h4>Título a obtener: MEDICO</h4>
                                <h4>TOTAL HORAS: 8.340 horas reloj</h4>
                                <h4>EXTENSION UNIVERSITARIA: 60 horas reloj</h4>
                            </div>
                            <a href="{{ asset('records/brochure-medicina.pdf') }}" class="btn btn-primary" data-aos="fade-left"
                               target="_blank ">DESCARGAR BROCHURE</a>
                        </div>
                    </div>--}}
                    <div class="tab-pane fade show active" id="pills-2018" role="tabpanel"
                         aria-labelledby="pills-2018-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab"
                                   href="#primer-2018"
                                   role="tab" aria-controls="primer-2018" aria-selected="true">PRIMER AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo-2018"
                                   role="tab" aria-controls="segundo-2018" aria-selected="false">SEGUNDO AÑO</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="primer-2018" role="tabpanel"
                                     aria-labelledby="primer-2018-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>1º SEMESTRE</h5>
                                            <p>Técnica Radiológica I</p>
                                            <p>Física Radiológica I</p>
                                            <p>Antatomía y Fisiología I</p>
                                            <p>Metodología de la Investigación</p>
                                            <p>Enfermería Básica</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>2º SEMESTRE</h5>
                                            <p>Técnica Radiológica II</p>
                                            <p>Física Radiológica II</p>
                                            <p>Antatomía y Fisiología II</p>
                                            <p>Matemática</p>
                                            <p>Embriología Básica</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="segundo-2018" role="tabpanel"
                                     aria-labelledby="segundo-2018-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>3º SEMESTRE</h5>
                                            <p>Técnica Radiológica III</p>
                                            <p>Química y Farmacología</p>
                                            <p>Patología Básica</p>
                                            <p>Psicología General</p>
                                            <p>Comunicación</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>4º SEMESTRE</h5>
                                            <p>Técnica Radiológica IV</p>
                                            <p>Ética y Deontología Profesional</p>
                                            <p>Informática</p>
                                            <p>Administración Hospitalaria</p>
                                            <p>Plan Optativo</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mt-5 justify-content-md-between flex-column flex-md-row">
                            <div data-aos="fade-right" class="mb-3 mb-md-0">
                                <h4>TOTAL HORAS TEÓRICAS Y PRÁCTICAS: 1.600 horas</h4>
                                <h4>PASANTÍAS HORAS RELOJ: 1.150 horas</h4>
                                <h4>TOTAL CARGA HORARIA DE LA CARRERA: 2.750 horas</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
