@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Técnico Superior en Enfermería</h2>
        </div>
        <img src="{{ asset('images/banner-medicina.png') }}" alt="" class="img-fluid">
    </div>

    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h4 class="mt-5" data-aos="fade-up">PERFIL PROFESIONAL DEL TÉCNICO SUPERIOR EN ENFERMERÍA</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    Al concluir sus estudios el Técnico/a Superior en Enfermería será un/a profesional con
                    formación científica, técnica y humanística capaz de:
                    <br>
                <ul>
                    <li>Reconocer a la Salud como un derecho humano y social fundamental, y la Atención Primaria en
                        Salud como una estrategia cuyo propósito es elevar el nivel y la calidad de vida de la
                        población.
                    </li>
                    <li>Desempeñar su rol en el equipo de salud, verificando las necesidades de la persona y su entorno,
                        brindando una atención integral a lo largo del ciclo vital, ejecutando procedimientos básicos y
                        técnicos en Enfermería bajo indicación del profesional de salud (Licenciado/a en Enfermería /
                        Médico/a tratante).
                    </li>
                    <li>Realizar Intervenciones en la promoción, prevención, protección, tratamiento y rehabilitación
                        dirigidas a las personas, las familias y el entorno, en base al perfil epidemiológico del país y
                        atendiendo a las normativas de salud vigentes.
                    </li>
                    <li>Aplicar los procedimientos y las técnicas del Proceso de Atención de Enfermería (PAE).</li>
                    <li>Valorar la educación permanente en salud y el trabajo en equipo, inter y multidisciplinario para
                        brindar una atención integral y de calidad a la persona, la familia y la comunidad.
                    </li>
                    <li>Asumir una actitud participativa, reflexiva, crítica, ética, de responsabilidad legal y de
                        compromiso profesional para el fortalecimiento del colectivo de Enfermería.
                    </li>
                    <li>Interpretar y ejecutar las indicaciones y/o instrucciones del Jefe / Licenciado/a en Enfermería
                        o Médico/a tratante.
                    </li>
                    <li>Colaborar en procesos de investigaciones epidemiológicas y en aquellas relacionadas con la salud
                        comunitaria.  Participar en la planificación y organización de los procesos internos de la
                        unidad en la que presta servicios, a fin de mejorar la calidad de la atención de la salud,
                        promover los cambios necesarios y fortalecer la gestión.
                    </li>
                    <li>Registrar los procedimientos de enfermería realizados, empleando terminologías de la taxonomía
                        establecida por la NANDA (NOR AMERICAN NURSING DIAGNOSIS ASSOCIATION).
                    </li>
                    <li>Emplear el lenguaje acorde al contexto multicultural según la población atendida, respetando la
                        diversidad.
                    </li>
                    <li>Utilizar la tecnología actual y el equipamiento acorde, en el ejercicio de su profesión.</li>
                </ul>
                </p>
                <h4 class="mt-5" data-aos="fade-up">ROL DEL TÉCNICO SUPERIOR EN ENFERMERÍA</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    El Técnico Superior en Enfermería se encuentra capacitado para realizar diversas labores dentro de
                    las cuales se contemplan las áreas de educación, prevención, promoción, recuperación y mantención de
                    la salud. Si bien, el Técnico Superior en Enfermería se encuentra bajo la supervisión de un/a
                    Enfermera/o, éste puede realizar procedimientos de manera independiente, y desempeñar funciones de
                    apoyo y colaboración con los diversos profesionales de la salud. Algunas de sus funciones son:
                    <br>
                <ul>
                    <li>Aseo y confort del paciente.</li>
                    <li>Administración de medicamentos vía oral*</li>
                    <li>Administración de medicamentos vía intramuscular*</li>
                    <li>Administración de medicamentos vía endovenoso*</li>
                    <li>Orientación al paciente en todo lo relacionado a normas de seguridad y a su enfermedad.</li>
                    <li>Colaboración con el equipo médico/salud en procedimientos y/o exámenes específicos.</li>
                    <li>Diagnóstico de signos vitales (presión, temperatura, pulso, otros).</li>
                    <li>Brindar primeros auxilios.</li>
                    <li>Toma de exámenes y/o muestras de laboratorio.</li>
                    <li>Aplicar normas de asepsia y antisepsia.</li>
                    <li>Otorgar cuidados básicos de Enfermería según indicación médica.</li>
                    <li>Promover el autocuidado en salud.</li>
                    <li>Participar en proyectos comunitarios en el área de salud.</li>
                    <li>Bajo Orden médica y de acuerdo a la normativa del Centro de Salud, solo en caso de no contarse
                        con profesional médico o licenciado el Técnico Superior asume la responsabilidad, para no caer
                        en omisión de auxilio.
                    </li>
                    <li>Bajo Orden médica y de acuerdo a la normativa del Centro de Salud, solo en caso de no contarse
                        con profesional médico o licenciado el Técnico Superior asume la responsabilidad, para no caer
                        en omisión de auxilio.
                    </li>
                </ul>
                </p>
            </div>
        </div>
        <div class="malla">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <h3 class="mb-5" data-aos="fade-left">Malla Curricular</h3>
                    <ul class="nav nav-pills nav-date date mb-3" id="pills-tab" role="tablist">
                        {{--<li class="nav-item">
                            <a class="nav-link active" id="pills-2015-tab" data-toggle="pill" href="#pills-2015" role="tab"
                               aria-controls="pills-2015" aria-selected="true">2015</a>
                        </li>--}}
                        {{--<li class="nav-item">
                            <a class="nav-link active" id="pills-2018-tab" data-toggle="pill" href="#pills-2018"
                               role="tab"
                               aria-controls="pills-2018" aria-selected="false">2018</a>
                        </li>--}}
                    </ul>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-2018" role="tabpanel"
                         aria-labelledby="pills-2018-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab"
                                   href="#primer-2018"
                                   role="tab" aria-controls="primer-2018" aria-selected="true">PRIMER AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo-2018"
                                   role="tab" aria-controls="segundo-2018" aria-selected="false">SEGUNDO AÑO</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="primer-2018" role="tabpanel"
                                     aria-labelledby="primer-2018-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>1º SEMESTRE</h5>
                                            <p>Lengua Castellana I</p>
                                            <p>Informática Aplicada a la Enfermería I</p>
                                            <p>Deontología y Etica Profesional I</p>
                                            <p>Salud Pública I</p>
                                            <p>Introducción a la Enfermería</p>
                                            <p>Anatomía y Fisiología I</p>
                                            <p>Tecnología en Enfermería I</p>
                                            <p>Nutrición y Dietoterapia</p>
                                            <p>Prácticas Laboratoriales en Enfermería I</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>2º SEMESTRE</h5>
                                            <p>Lengua Castellana II</p>
                                            <p>Informática Aplicada a la Enfermería II</p>
                                            <p>Deontología y Etica Profesional II</p>
                                            <p>Salud Pública II</p>
                                            <p>Microbiología y Parasitología I</p>
                                            <p>Anatomía y Fisiología II</p>
                                            <p>Tecnología en Enfermería II</p>
                                            <p>Enfermería Materno Infantil I</p>
                                            <p>Prácticas Laboratoriales en Enfermería II</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="segundo-2018" role="tabpanel"
                                     aria-labelledby="segundo-2018-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>3º SEMESTRE</h5>
                                            <p>Lengua Guarani</p>
                                            <p>Bioestadística I</p>
                                            <p>Psicología General</p>
                                            <p>Microbiología y Parasitología II</p>
                                            <p>Tecnología en Enfermería III</p>
                                            <p>Farmacología I</p>
                                            <p>Enfermería Materno Infantil II</p>
                                            <p>Enfermería en Salud del Adulto I</p>
                                            <p>Prácticas Laboratoriales en Enfermería III</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>4º SEMESTRE</h5>
                                            <p>Proyecto</p>
                                            <p>Bioestadística II</p>
                                            <p>Farmacología II</p>
                                            <p>Enfermería Materno Infantil III</p>
                                            <p>Enfermería en Salud del Adulto II</p>
                                            <p>Enfermería en Salud Mental y Psiquiatría</p>
                                            <p>Epidemiología</p>
                                            <p>Plan Optativo I y II</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mt-5 justify-content-md-between flex-column flex-md-row">
                            <div data-aos="fade-right" class="mb-3 mb-md-0">
                                <h4>TOTAL HORAS TEÓRICAS Y PRÁCTICAS: 1.600 horas</h4>
                                <h4>PASANTÍAS HORAS RELOJ: 1.250 horas</h4>
                                <h4>TOTAL CARGA HORARIA DE LA CARRERA: 2.850 horas</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
