@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Técnico Superior en Laboratorio Clínico</h2>
        </div>
        <img src="{{ asset('images/banner-medicina.png') }}" alt="" class="img-fluid">
    </div>

    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h4 class="mt-5" data-aos="fade-up">PERFIL PROFESIONAL DEL TÉCNICO SUPERIOR EN LABORATORIO CLÍNICO</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    Al concluir los estudios, el Técnico Superior en Laboratorio será capaz de desarrollar
                    eficientemente las siguientes funciones:
                    <br>
                <ul>
                    <li>Realizar la atención personal al usuario orientándolo hacia la correcta preparación de los
                        mismos antes de la extracción de materiales.
                    </li>
                    <li>Proceder a la toma de muestras de los diferentes especímenes de líquidos y secreciones
                        humanas.
                    </li>
                    <li>Efectuar la correcta separación y clasificación de las muestras.</li>
                    <li>Efectuar procedimientos básicos de laboratorio previamente establecidos.</li>
                    <li>Efectuar, según norma especificada por escrito, distintas determinaciones habituales o de rutina
                        en el laboratorio de análisis clínicos y químicos, tanto en sangre (hematocrito,
                        eritrosedimentación, recuento de glóbulos, uremia, glucemia, uricemia, determinaciones
                        enzimáticas, etc.) orina (constituyentes, urea, ácido úrico, electrolitos, elementos comunes del
                        sedimento) y tejidos (frotis, cortes histológicos, preparación de reactivos y colorantes);
                        manejo de distintos aparatos como fotocolorímetro, fotómetro de llama, espectrofotómetro,
                        balanzas analíticas, osciloscopio y cromatógrafo.
                    </li>
                    <li>Realizar tareas de esterilización.</li>
                    <li>Realizar pruebas para la detección de enfermedades transmisibles (microbianas, parasitarias) y
                        serodiagnóstico.
                    </li>
                </ul>
                </p>

                <h4 class="mt-5" data-aos="fade-up">ROL DEL TÉCNICO SUPERIOR EN LABORATORIO CLÍNICO</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    Las funciones del Técnico Superior en Laboratorio Clínico, corresponden a las acciones de
                    prevención, diagnóstico temprano y evaluación de las enfermedades mediante la utilización de
                    equipos, materiales y reactivos aplicando los procedimientos tecnológicos para el análisis de
                    especímenes y la aplicación de las medidas de control de calidad pertinentes que garanticen la
                    eficiencia y eficacia de dichos procedimientos.
                    <br>
                    Para cumplir estas funciones realizarán las siguientes actividades:
                    <br>
                <ul>
                    <li>Realizar las pruebas de laboratorio incorporando las nuevas técnicas aprendidas.</li>
                    <li>Educar al paciente en la correcta obtención y traslado de la muestra con la finalidad de
                        prevenir y controlar las enfermedades y para garantizar el éxito de los resultados.
                    </li>
                    <li>Proteger a los especímenes de contaminación y evitar que éstos a su vez se transformen en
                        agentes contaminantes.
                    </li>
                    <li>Realizar en forma periódica pruebas de control de calidad que garanticen la confiabilidad de los
                        resultados.
                    </li>
                    <li>Realizar el mantenimiento preventivo de los equipos y reportar inmediatamente cualquier
                        desperfecto detectado.
                    </li>
                    <li>Cooperar para una buena gestión administrativa de los servicios.</li>
                    <li>Participar en programas de educación continuada tendientes al mejoramiento profesional.</li>
                </ul>
                </p>
                <h4 class="mt-5" data-aos="fade-up">CAMPO OCUPACIONAL</h4>
                <p data-aos="fade-up" data-aos-duration="800">
                    El egresado del Técnico Superior en Laboratorio Clínico del Instituto Técnico Superior Maria
                    Auxiliadora, podrá desempeñarse vinculándose a determinadas instituciones o como profesional
                    independiente, en los siguientes campos:
                    <br>
                <ul>
                    <li>Laboratorios de Análisis Clínicos dependientes de servicios estatales: Hospitales
                        Universitarios, Hospitales del M.S.P. y B.S, Centro Regionales.
                    </li>
                    <li>Laboratorios de Análisis Clínicos de Hospitales y Clínicas Privadas.</li>
                    <li>Laboratorios de Análisis Clínicos privados.</li>
                </ul>
                </p>
            </div>
        </div>
        <div class="malla">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <h3 class="mb-5" data-aos="fade-left">Malla Curricular</h3>
                    <ul class="nav nav-pills nav-date date mb-3" id="pills-tab" role="tablist">
                        {{--<li class="nav-item">
                            <a class="nav-link active" id="pills-2015-tab" data-toggle="pill" href="#pills-2015" role="tab"
                               aria-controls="pills-2015" aria-selected="true">2015</a>
                        </li>--}}
                        {{--<li class="nav-item">
                            <a class="nav-link active" id="pills-2018-tab" data-toggle="pill" href="#pills-2018"
                               role="tab"
                               aria-controls="pills-2018" aria-selected="false">2018</a>
                        </li>--}}
                    </ul>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    {{--<div class="tab-pane fade show active" id="pills-2015" role="tabpanel" aria-labelledby="pills-2015-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#primer"
                                   role="tab" aria-controls="primer" aria-selected="true">PRIMER AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo" role="tab"
                                   aria-controls="segundo" aria-selected="false">SEGUNDO AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#tercero" role="tab"
                                   aria-controls="tercero" aria-selected="false">TERCER AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#cuarto" role="tab"
                                   aria-controls="cuarto" aria-selected="false">CUARTO AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#quinto" role="tab"
                                   aria-controls="quinto" aria-selected="false">QUINTO AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#sexto" role="tab"
                                   aria-controls="sexto" aria-selected="false">SEXTO AÑO</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="primer" role="tabpanel" aria-labelledby="primer-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>1º SEMESTRE</h5>
                                            <p>Anatomía Humana I</p>
                                            <p>Histología Humana I</p>
                                            <p>Matemática Aplicada a la Medicina</p>
                                            <p>Medicina de la Comunidad</p>
                                            <p>Comunicación Oral y Escrita</p>
                                            <p>Biología Celular</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>2º SEMESTRE</h5>
                                            <p>Anatomía Humana II</p>
                                            <p>Histología Humana II</p>
                                            <p>Embriología y Genética</p>
                                            <p>Epidemiología y Ecología</p>
                                            <p>Biofísica Médica</p>
                                            <p>Bioestadística y Demografía</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="segundo" role="tabpanel" aria-labelledby="segundo-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>3º SEMESTRE</h5>
                                            <p>Fisiología Humana I</p>
                                            <p>Microbiología, Parasitología e Inmunología I</p>
                                            <p>Bioquímica Médica I</p>
                                            <p>Metodología de la Investigación</p>
                                            <p>Guaraní</p>
                                            <p>Inglés</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>4º SEMESTRE</h5>
                                            <p>Fisiología Humana II</p>
                                            <p>Microbiología, Parasitología e Inmunología II</p>
                                            <p>Bioquímica Médica II</p>
                                            <p>Psicología Médica</p>
                                            <p>Socio-antropología</p>
                                            <p>Primeros Auxilios</p>
                                            <p>Principios y Valores Humanos y Espirituales</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tercero" role="tabpanel" aria-labelledby="tercero-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>5º SEMESTRE</h5>
                                            <p>Fisiopatología I</p>
                                            <p>Anatomía Patológica I</p>
                                            <p>Medicina Preventiva y Salud Pública</p>
                                            <p>Medicina Familiar</p>
                                            <p>Informática Aplicada a la Medicina</p>
                                            <p>Patología Médica I</p>
                                            <p>Patología Quirúrgica I</p>
                                            <p>Optativa I</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>6º SEMESTRE</h5>
                                            <p>Fisiopatología II</p>
                                            <p>Anatomía Patológica II</p>
                                            <p>Economía</p>
                                            <p>Ética y Bioética Médica</p>
                                            <p>Medicina Legal</p>
                                            <p>Patología Médica II</p>
                                            <p>Patología Quirúrgica II</p>
                                            <p>Psicopatología</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="cuarto" role="tabpanel" aria-labelledby="cuarto-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>7º SEMESTRE</h5>
                                            <p>Semiología Médica I</p>
                                            <p>Semiología Quirúrgica I</p>
                                            <p>Farmacología I</p>
                                            <p>Otorrinolaringología</p>
                                            <p>Nutrición Clínica</p>
                                            <p>Medicina de Imágenes</p>
                                            <p>Psiquiatría</p>
                                            <p>Administración de Servicios de Salud</p>
                                            <p>Optativa II</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>8º SEMESTRE</h5>
                                            <p>Semiología Médica II</p>
                                            <p>Semiología Quirúrgica II</p>
                                            <p>Farmacología II</p>
                                            <p>Medicina Tropical</p>
                                            <p>Dermatología</p>
                                            <p>Oftalmología</p>
                                            <p>Urología</p>
                                            <p>Anestesiología</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="quinto" role="tabpanel" aria-labelledby="quinto-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>9º SEMESTRE</h5>
                                            <p>Cirugía I</p>
                                            <p>Ginecología y Obstetricia I</p>
                                            <p>Medicina Interna I</p>
                                            <p>Pediatría I</p>
                                            <p>Toxicología</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>10º SEMESTRE</h5>
                                            <p>Cirugía II</p>
                                            <p>Ginecología y Obstetricia II</p>
                                            <p>Medicina Interna II</p>
                                            <p>Pediatría II</p>
                                            <p>Traumatología, Ortopedia y Rehabilitación</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="sexto" role="tabpanel" aria-labelledby="sexto-tab">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>11º y 12º SEMESTRE</h5>
                                            <p>Internado en Medicina Interna</p>
                                            <p>Internado en Cirugía</p>
                                            <p>Internado en Pediatría</p>
                                            <p>Internado en Ginecología y Obstetricia</p>
                                            <p>Internado en Emergentología</p>
                                            <p>Atención Primaria en Salud en la comunidad</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mt-5 justify-content-md-between flex-column flex-md-row">
                            <div data-aos="fade-right" class="mb-3 mb-md-0">
                                <h4>Título a obtener: MEDICO</h4>
                                <h4>TOTAL HORAS: 8.340 horas reloj</h4>
                                <h4>EXTENSION UNIVERSITARIA: 60 horas reloj</h4>
                            </div>
                            <a href="{{ asset('records/brochure-medicina.pdf') }}" class="btn btn-primary" data-aos="fade-left"
                               target="_blank ">DESCARGAR BROCHURE</a>
                        </div>
                    </div>--}}
                    <div class="tab-pane fade show active" id="pills-2018" role="tabpanel"
                         aria-labelledby="pills-2018-tab">
                        <div class="malla-tab" data-aos="fade-up">
                            <nav class="nav nav-pills flex-column flex-sm-row">
                                <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab"
                                   href="#primer-2018"
                                   role="tab" aria-controls="primer-2018" aria-selected="true">PRIMER AÑO</a>
                                <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#segundo-2018"
                                   role="tab" aria-controls="segundo-2018" aria-selected="false">SEGUNDO AÑO</a>
                            </nav>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="primer-2018" role="tabpanel"
                                     aria-labelledby="primer-2018-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>1º SEMESTRE</h5>
                                            <p>Anatomía y Fisiología</p>
                                            <p>Química Clínica</p>
                                            <p>Materiales, Instrumentación y Equipamientos</p>
                                            <p>Bioseguridad</p>
                                            <p>Primeros Auxilios</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>2º SEMESTRE</h5>
                                            <p>Etica Profesional</p>
                                            <p>Matemática</p>
                                            <p>Hematología</p>
                                            <p>Parasitología</p>
                                            <p>Reactivos y Medios de Cultivos</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="segundo-2018" role="tabpanel"
                                     aria-labelledby="segundo-2018-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>3º SEMESTRE</h5>
                                            <p>Informática</p>
                                            <p>Metodología de la Investigación</p>
                                            <p>Patología General</p>
                                            <p>Microbiología</p>
                                            <p>Inmunología</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>4º SEMESTRE</h5>
                                            <p>Bioestadística</p>
                                            <p>Comunicación</p>
                                            <p>Salud Pública</p>
                                            <p>Calidad en Salud</p>
                                            <p>Marketing y Orientación Laboral</p>
                                            <p>Plan Optativo</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mt-5 justify-content-md-between flex-column flex-md-row">
                            <div data-aos="fade-right" class="mb-3 mb-md-0">
                                <h4>TOTAL HORAS TEÓRICAS Y PRÁCTICAS: 1.600 horas</h4>
                                <h4>PASANTÍAS HORAS RELOJ: 1.150 horas</h4>
                                <h4>TOTAL CARGA HORARIA DE LA CARRERA: 2.750 horas</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@section('scripts')
    {{--  <script>
        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3.5,
            dots: '#scrollLockDelay',
            draggable: true,
            arrows: {
                next: '.glider-next',
                prev: '.glider-prev'
            }
        });
    </script> --}}
@endsection

@endsection
