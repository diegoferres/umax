@extends('layout.app')

@section('head')

@endsection

@section('content')

    <div class="header-top">
        <div class="container">
            <h2 data-aos="fade-up">Postgrado</h2>
        </div>
        <img src="{{ asset('images/fachada.jpg') }}" alt="" class="img-fluid">
    </div>
    <section class="university">
        <div class="container">
            <div class="text-nosotros">
                <h3 data-aos="fade-up">Programas de Posgrado</h3>
                <p class="mt-4" data-aos="fade-up" data-aos-duration="800">
                    La Universidad María Auxiliadora promueve un modelo de enseñanza integral que contempla todos los
                    niveles de la Educación Superior y en materia de Programas de Posgrado, se han elaborado los
                    proyectos enfocados en el área de Humanidades, como así también en el área de Salud que se dividen
                    de la siguiente manera:
                    <br><br>
                </p>
                <div class="autoridades mt-0">
                    <div class="row">
                        <div class="col-12 col-md-9 mt-0">
                            <h4 class="mb-4">Formación Académica - Títulos</h4>
                            <p>
                            <ul>
                                <li>Especialización en Didáctica Superior Universitaria.</li>
                                <li>Maestría en Educación Superior.</li>
                                <li>Maestría en Educación Médica.</li>
                                <li>Maestría en Educación con Énfasis en Gestión Institucional (en proceso de
                                    habilitación
                                    por el CONES).
                                </li>
                                <li>Maestría en Salud Pública (en proceso de habilitación por el CONES).</li>
                                <li>Maestría en Administración Hospitalaria (en proceso de habilitación por el CONES).
                                </li>
                            </ul>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
